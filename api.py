# -*- coding: utf-8 -*-
"""Example Google style docstrings.

feature -> similarity -> scene_feature -> scene_classifier

"""
import json
import redis
from flask import Flask, request
from pprintpp import pprint as pp
import logging
logger = logging.getLogger('merge.api')
logging.basicConfig(filename='/tmp/logs', level=logging.DEBUG)

import argparse
parser = argparse.ArgumentParser(description='')
parser.add_argument('--project ', action="store", dest="project", required=True)
parser.add_argument('--conf', action="store", dest="conf", required=False)
args = vars(parser.parse_args())
pp(args)
project_path = "projects/%s" % args['project']
import sys
sys.path.append(project_path)

from entity import Entity
from merge.entity_merge import EntityMerge
from merge.loader import MergeLoader


APP = Flask(__name__)

REDIS_HOST = "localhost"
REDIS_PORT = 6479
REDIS_DB = 0

POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
POOL_CLUSTER = redis.ConnectionPool(host=REDIS_HOST, port=6479, db=1)
CLUSTER_FULL = redis.ConnectionPool(host=REDIS_HOST, port=6579, db=1)
R_TEMPID = redis.StrictRedis(connection_pool=POOL, charset="utf-8", decode_responses=True)
R_GROUP = redis.StrictRedis(connection_pool=CLUSTER_FULL, charset="utf-8", decode_responses=True)
R_CLUSTER =  redis.StrictRedis(connection_pool=POOL_CLUSTER, charset="utf-8", decode_responses=True)

@APP.route('/eid', methods=['GET', 'POST'])
def eid():
    """
    get entity by entity id
    example: 6f1e37bc700bc975737f1f4dd449995c
    """
    entity_id = request.values.get('id', 'None')
    #req = request.get_json()
    #entity_id = req['id']
    print('eid:%s' % entity_id)
    #print(req)
    json_str = R_TEMPID.get(entity_id)
    if json_str is None:
        return json.dumps({'error': 'not existing'})
    entity = Entity(json_str=json_str.decode('utf-8'))
    return entity.to_json(indent=True)


@APP.route('/gid', methods=['GET', 'POST'])
def gid():
    """
    get group data from gid
    example: "b8dcd98a3f484a4daacdd946a7bb9f4f"
    """
    req = request.get_json()
    print(req)
    pp(req)
    group_id = req['id']
    json_str = R_GROUP.get(group_id)
    if json_str is None:
        return json.dumps({'error': 'not existing'})
    json_str = json_str.decode('utf-8')
    return json.dumps(json.loads(json_str), indent=4)


@APP.route('/similarity', methods=['GET', 'POST'])
def similarity():
    """
    get similarity b/t two entity by given two entity_id or group_id and type of id (entity or
    group)

    """
    query_type = request.values.get('type', 'None')
    id_1 = request.values.get('id_1', 'None')
    id_2 = request.values.get('id_2', 'None')
    if query_type not in ['eid', 'gid', 'eid_gid']:
        return json.dumps({'error': 'query type not existing'})
    if query_type == 'eid':
        json_str_1, json_str_2 = R_TEMPID.get(id_1), R_TEMPID.get(id_2)
    elif query_type == 'gid':
        json_str_1, json_str_2 = R_GROUP.get(id_1), R_GROUP.get(id_2)
    else:
        json_str_1, json_str_2 = R_GROUP.get(id_1), R_TEMPID.get(id_2)

    #R_CLUSTER
    if None in [json_str_1, json_str_2]:
        return json.dumps({'error': 'not existing'})
    result = ''
    if query_type == 'eid':
        entity_1 = Entity(json_str=json_str_1.decode('utf-8'))
        entity_2 = Entity(json_str=json_str_2.decode('utf-8'))
        result = fs.get_entity_similarity(entity_1, entity_2)
        # id_1=a3dd9fee84fce2e0abb954829b4b11c0  id_2=42d92198d32d7aa6ca4764349484b271
    elif query_type == 'gid':
        # deprecated section of code 
        group_1 = Group(from_redis=json_str_1.decode('utf-8'))
        group_2 = Group(from_redis=json_str_1.decode('utf-8'))
    else:
        group = Group(json_str=json_str_1.strip().decode('utf-8'))
        entity = Entity(json_str=json_str_2.decode('utf-8'))
        result = fs.get_group_entity_similarity(group=group, entity=entity)
    return json.dumps(result, ensure_ascii=False, indent=4)


@APP.route('/distance', methods=['GET', 'POST'])
def distance():
    """
    get similarity b/t two entity by given two entity_id or group_id and type of id (entity or
    group)

    """
    similarity_str = request.values.get('similarity', None)
    similarity = {}
    if similarity_str is not None:
        similarity = json.loads(similarity_str)
    if type(similarity) is not list:
        similarity = [similarity]
    distance_matrix, distance_matrix_hit = fs.get_distance(similarity)
    result = {
        'distance_matrix': distance_matrix,
        'distance_matrix_hit': distance_matrix_hit
    }
    return json.dumps(result, ensure_ascii=False, indent=4)

@APP.route('/feature', methods=['GET', 'POST'])
def feature():
    '''
    get similarity b/t two entity by given two entity_id or group_id and type of id (entity or
    group)
    '''

    distance_str = request.values.get('distance_matrix', None)
    distance_matrix = {}
    if distance_str is not None:
        distance_matrix = json.loads(distance_str)
    screne_feature = fs.get_screne_feature(distance_matrix)
    return json.dumps(screne_feature, ensure_ascii=False, indent=4)


@APP.route('/merge', methods=['GET', 'POST'])
def merge():
    '''
    indicate whether group and entity should be merged or not
    '''
    req = request.get_json()
    query_type, id_1, id_2 = req['type'], req['id_1'], req['id_2']
    #if query_type not in ['eid', 'gid', 'eid_gid']:
    #    return json.dumps({'error': 'query type not existing'})
    json_str_1, json_str_2 = R_GROUP.get(id_1), R_TEMPID.get(id_2)
    if None in [json_str_1, json_str_2]:
        return json.dumps({'error': 'not existing'})

    group = Group(json_str=json_str_1.strip().decode('utf-8'))
    entity = Entity(json_str=json_str_2.decode('utf-8'))
    result = fs.should_be_merge(group=group, entity=entity)
    return json.dumps(result)


@APP.route('/screne_classify', methods=['GET', 'POST'])
def screne_classify():
    '''
    indicate whether group and entity should be merged or not
    '''
    feature_str = request.values.get('screne_feature', None)
    feature = {}
    if feature_str is not None:
        feature = json.loads(feature_str)
    classify_result = fs.screne_classify(feature)
    return json.dumps(classify_result, ensure_ascii=False, indent=4)


@APP.route('/merge_tempids', methods=['GET', 'POST'])
def merge_tempids():
    '''
    given tempid list and then merge, return result.
    Input:
        tempids : [
            #tempid_1,
            #tempid_2,
            ...
        ]
    Output:
        [
            {#gid_1: #tempid_1},
            {#gid_2: #tempid_2},
            ...
        ]
    '''
    from merge.loader import MergeLoader as ML
    from batch import get_args
    global args
    pp(args)
    args = get_args(args)

    pp(args)
    args['CLUSTER']['LOAD'] = ''
    merge_loader = ML(args)

    pp(merge_loader.cluster)

    scene_merge = EntityMerge(args=args)

    tempid_str = request.values.get('tempids', '')
    tempids = json.loads(tempid_str)
    inputs = {}
    tmp = {}
    print('received %s tempids ' % (len(tempids)))
    for idx, tempid in enumerate(tempids):
        json_str = R_TEMPID.get(tempid)
        if json_str is None:
            return json.dumps({'error': 'not existing'})
        entity = Entity(json_str=json_str.decode('utf-8'))
        inputs[entity.eid] = entity
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    scene_merge.merge(inputs=inputs,
                      cluster=merge_loader.cluster,
                      idx_mgr=merge_loader.idx)

    cluster = merge_loader.cluster
    cluster_diff = scene_merge.cluster_diff
    from export import dump
    dump(scene_merge.cluster, scene_merge.cluster_diff, args)
    print('*'*100)
    result = {}
    for _, group in cluster.map.items():
        for eid, entity in group.map.items():
            result[eid] = group.id
    #pp(result)
    #print('*'*100)
    #pp(merge_loader.cluster.map)
    #if query_type not in ['eid', 'gid', 'eid_gid']:
    #    return json.dumps({'error': 'query type not existing'})
    #json_str_1, json_str_2 = R_GROUP.get(id_1), R_TEMPID.get(id_2)
    #if None in [json_str_1, json_str_2]:
    #    return json.dumps({'error': 'not existing'})
    #group = Group(json_str=json_str_1.strip().decode('utf-8'))
    #entity = Entity(json_str=json_str_2.decode('utf-8'))
    #result = FEATURE_SET.should_be_merge(group=group, entity=entity)
    return json.dumps(result)



@APP.route('/get_similarity_with_gid', methods=['GET', 'POST'])
def get_similarity_with_gid():
    req = request.get_json()
    #gid = req['gid']
    eids = list(r_group.smembers(gid))
    result = []
    for feature in ['name', 'tel']:
        e1 = Entity(json_str = r_tempid.get(eids[i]).decode('utf-8'))
        e2 = Entity(json_str = r_tempid.get(eids[j]).decode('utf-8'))
        scene_feature = feature_set.get_entity_similarity(e1=e1, e2=e2)
        result.append(scene_feature)
    """
    for feature in ['name', 'tel']:
        tmp = "%s[%s]:%s[%s]:%s" % (e1['eid'][:5], e1[feature], e2['eid'][:5], e2[feature], scene_feature[feature])
        result.append(tmp)
    """
	#return 'member %s' % json.dumps(result, indent=4, ensure_ascii=False)
    return 'member %s' % json.dumps(result, indent=4, ensure_ascii=False)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('integers', metavar='N', type=int, nargs='+',
                                            help='an integer for the accumulator')
    parser.add_argument('--sum', dest='accumulate', action='store_const',
                                            const=sum, default=max,
                                            help='sum the integers (default: find the max)')

    APP.run(debug=True)
