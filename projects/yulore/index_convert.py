import json
import index_convert_utils
from utils.index_utils import escapeEsSolrString, escapeCompanyToken


def obj2_index_obj(obj, from_type, to_type, args):
    #def obj2_index_obj(obj, from_obj_type, to_type, args):
    index_obj = None
    if from_type == 'group_str': # from group_str
        obj = json.loads(obj)
    elif from_type != 'group_obj':
        raise
    query_body = index_convert_utils.group_obj_2_es_doc(obj)
    #elif from_type == 'group_obj': # from entity_str, add by merge
    #    gid = 
    #    query_body = index_convert_utils.entity_obj_2_es_doc(gid , obj)
    if to_type == "EsIndexAdm":
        index_obj = {
            'body': {
                'doc': query_body,
                'doc_as_upsert': True
            },
            'index': args['tmp_index_name'],
            'doc_type': 'shop',
            'id': query_body['sid']
        }
    elif to_obj == "SolrIndexAdm":
        pass
    return index_obj
    
def get_gid_query_obj(index_type, eid):
    def get_es_query_gid_obj(eid):
        query_body = {
             "query" : {
             "nested": {
                 "path": "inputs",
                 "score_mode": "avg",
                 "query": {
                 "bool": {
                     "must": [
                     {"match": {"inputs.tempid": eid}}
                     ]
                 }
                 }
             }
             }
         }
        query_obj = {
             'body': query_body,
             'doc_type': 'shop',
             'size': 3
        }
        return query_obj

    def get_solr_query_gid_obj(eid):
        pass

    if index_type == "EsIndexAdm":
        return get_es_query_gid_obj(eid)
    elif index_type == "SolrIndexAdm":
        return get_solr_query_gid_obj(eid)
    else:
        raise

def get_query_obj(index_type, args, entity):
    if index_type == "EsIndexAdm":
        return get_es_query_obj(args, entity)
    elif index_type == "SolrIndexAdm":
        return get_solr_query_obj(args, entity)
    else:
        raise

def get_solr_query_obj(args, entity):
    fl = "name, address, tel, shopid, score"
    '''
    q = "(%s) AND (%s OR %s)" % \
        (" OR ".join(['tel:"%s"'% _['number'] for _ in entity['tel']]),
         'name:%s' % entity['name'],
         'address:%s' % entity['address']['street'])
    '''
    name = escapeEsSolrString(entity['name'].replace("()", ""))
    address = escapeEsSolrString(entity['address']['street'])
    
    if address != "":
        q = "(%s) OR (%s AND  %s)" % \
            (" OR ".join(['tel:"%s"'% _['number'] for _ in entity['tel']]),
             'name:%s' % name, 'address:%s' % address)
    else:
    # street is empty
        q = "(%s) OR %s" % \
            (" OR ".join(['tel:"%s"'% _['number'] for _ in entity['tel']]),
                'name:%s' % name)
    if entity['address']['district'] != "":
        fq = ("city_id: %s"% args['city_id'], "dis_id:%s" %  \
            args['district_name_id_map'][entity['address']['district']], "type:1")
    else:
        fq = ("city_id: %s"% args['city_id'], "type:1")

    query_obj = {
        'q': q,
        'fq': fq,
        # TO-DO: IMPORTANT !! REPLACE HERE
        'fl': fl,
        'q.op': 'OR',
        "rows": "3"
    }
    return query_obj


def get_es_query_obj(args, entity):
    def get_es_return_field():
        return {
            "_source": [
                "sid",
                "name",
                "address",
                "inputs.name",
                "inputs.address",
                "inputs.tel.number"
            ]
        }
    def get_es_filter_part(args, entity):
        _filter = {
                "bool": {
                    "should": [
                        {"term": {"city_id": args['city_id']}}
                    ]
                }
        }
        if entity['address']['district'] != "":
            _filter["bool"]["should"].append(
                {"term": {"distric": entity['address']['district']}})
        return _filter

    name = escapeEsSolrString(entity['name'].replace("()", ""))
    name = escapeCompanyToken(name)
    address = escapeEsSolrString(entity['address']['street'])

    query_body = {
        "query": {
            "bool": {"must":[], "filter":{}}
        }
    }

    name_address_obj = { "bool": { "should": [
        {"match": {"name": name}}
    ]}}
    if address  != "":
        name_address_obj['bool']['should'].append(
            {"match": {"address": address}})
    tel_obj = {"bool": {"should": []}}
    for tel in entity['tel']:
        _tel_obj = {
            "nested": {
                "path": "inputs",
                "query": {
                    "term": {"inputs.tel.number": tel['number']}
                }
            }
        }
        tel_obj['bool']['should'].append(_tel_obj)

    query_body['query']['bool']['must'].append(name_address_obj)
    query_body['query']['bool']['must'].append(tel_obj)
    query_body['query']['bool']['filter'].update(get_es_filter_part(args, entity))
    query_body.update(get_es_return_field())
    query_obj = {
        'body': query_body,
        'doc_type': 'shop',
        'size': 3
    }
    return query_obj


@classmethod
def convert_solr_query_result(query_result):
    gid_group_map = {}
    for _group in query_result:
        try:
            if _group['score'] < 3:
                continue
            _tmp = {'score': _group['score'], 
                    'sid': _group['shopid']}
            for fname in ['name', 'tel', 'address']:
                if fname == "tel":
                    _tmp['tel'] = [_tel.split(":")[-1] for _tel in _group['tel']]
                else:
                    _tmp[fname] = _group[fname]
        except Exception as e:
            print(str(e))
            pp(_group)
        gid_group_map[_tmp['sid']] = _tmp
    return gid_group_map

def convert_es_query_result(query_result):
    gid_group_map = {}
    for _group in query_result:
        try:
            if _group['_score'] < 20:
                continue
            _tmp = {'score': _group['_score'],
                    'sid': _group['_source']['sid']}
            for fname in ['name', 'tel', 'address']:
                if fname == "tel":
                    _tmp['tel'] = []
                    for entity in _group['_source']['inputs']:
                        for tel in entity["tel"]:
                            if tel['number'] not in _tmp['tel']:
                                _tmp['tel'].append(tel['number'])
                else:
                    _tmp[fname] = _group['_source'][fname]
        except Exception as e:
            print(str(e))
            pp(_group)
        gid_group_map[_tmp['sid']] = _tmp
    return gid_group_map

def convert_query_result(index_type, query_result):
    if index_type == "EsIndexAdm":
        return convert_es_query_result(query_result)
    elif index_type == "SolrIndexAdm":
        return convert_solr_query_result(query_result)
    else:
        raise

