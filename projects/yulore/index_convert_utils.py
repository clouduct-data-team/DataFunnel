from dateutil.parser import *
from pprintpp import pprint as pp

def get_latlng(entity):
    lat = lng = 0
    if 0 < entity['recommend_lat'] < 360:
        lat =  entity['recommend_lat']
    if 0 < entity['recommend_lng'] < 360:
        lng = entity['recommend_lng']
    return "%s, %s" % (lat, lng)

def get_mainname(group):
    if group['recommend_mainname'] in ['', None]:
        return ''
    return group['recommend_mainname']

def get_category_name(group):
    if group['recommend_category_name'] in ['', None]:
        return ''
    return group['recommend_category_name']

def get_category_id(group):
    if group['recommend_category_id'] in ['', None]:
        return ''
    return group['recommend_category_id']

def get_version(group):
    max_version = 19901231235959
    for entity in group['inputs']:
        version = int(entity['version'])
        if version > max_version:
            max_version = version
    #t = parse(str(max_version))
    #version = t.strftime('%Y%m%d%H%M%S')
    return str(max_version)

def get_district(group):
    return group['region']['district_name']
    
def get_district_id(group):
    return group['region']['district_id']

def get_city(group):
    return group['region']['city_name']
def get_city_id(group):
    return group['region']['city_id']
def get_province(group):
    return group['region']['province_name']
def get_province_id(group):
    return group['region']['province_id']

def get_tally(group):
    return len(group['inputs'])

def get_inputs(group):
    entity_list = []
    for entity in group['inputs']:
        entity_data = {}
        entity_data['tempid'] = entity['tempid']
        entity_data['name'] = entity['name']
        entity_data['category'] = entity['category_name']
        entity_data['address'] = entity['address']['street']
        entity_data['district'] = entity['address']['district']
        entity_data['source'] = entity['source_name']
        entity_data['link'] = entity['link']
        entity_data['version'] = entity['version']
        #t = parse(str(entity['version']))
        #entity_data['version'] = t.strftime('%Y%m%d %H:%M:%S')

        if entity['category_name'] in ['', None]:
            entity_data['category'] = []
        else:
            entity_data['category'] = entity['category_name']
        entity_data['district'] = entity['address']['district']
        tel = []
        for _tel in entity['tel']:
            tel_data = {}
            tel_data['number'] = _tel['number']
            tel_data['desc'] = _tel['desc']
            tel_data['type'] = _tel['type']
            tel.append(tel_data)
        entity_data['tel'] = tel
        entity_list.append(entity_data)
    return entity_list

def get_simhash(group):
    return ''


# DEPRECATED: entity_obj_2_es_doc is not been used 
def entity_obj_2_es_doc(gid, entity):
    convert_map = {
        'name': 'name',
        'mainname': 'name',
        #'name_key': get_name_key, #list but might be null
        'head': 'name',
        'category': 'category_name',
        'category_id': 'category_id',
        'district': 'district_name',
        'district_id': 'district_id',
        'city': 'city_name',
        'city_id': 'city_id',
        'province': 'province_name',
        'province_id': 'province_id',
        'version': 'version',
        'simhash': get_simhash
    }
    new_group = {
        'branch': '',
        'address': entity['address']['street'],
        'sid': gid,
        'tally': 1,
        'latlng': " 0, 0",
        'inputs': get_inputs({'inputs': [entity]})
    }
    for field, value in convert_map.items():
        if type(value) is str:
            new_group[field] = entity[value]
        else:
            new_group[field] = value(entity)
    return new_group

def group_obj_2_es_doc(group):

    convert_map = {
        'sid': 'sid',
        #'tempid': 'tempid',
        'name': 'recommend_name',
        'mainname': get_mainname,
        #'name_key': get_name_key, #list but might be null
        'head': 'recommend_head',
        'branch': 'recommend_branch',
        'address': 'recommend_address',
        'latlng': get_latlng,
        'category': get_category_name,
        'category_id': get_category_id,

        'district': get_district,
        'district_id': get_district_id,
        'city': get_city,
        'city_id': get_city_id,
        'province': get_province,
        'province_id': get_province_id,
        'tally': get_tally,
        'version': get_version,
        'inputs': get_inputs,
        'simhash': get_simhash
    }
    new_group = {}
    for field, value in convert_map.items():
        if type(value) is str:
            new_group[field] = group[value]
        else:
            new_group[field] = value(group)
    return new_group
