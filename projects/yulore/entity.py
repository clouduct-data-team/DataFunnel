from ABC.DictContainer import DictContainer
import json

class Entity(DictContainer):

    FEATURE_MAPPING = {
        'eid': 'tempid',
        'name': 'name',
        #'mainname': 'mainname',
        #'name_key': 'name_key',
        #'region': 'region',
        'address': 'address',
        'tel': 'tel'
    }

    @classmethod
    def get_stree(cls, obj):
        return "%s %s" % (obj['address']['district'], obj['address']['street'] )

    @classmethod
    def get_tel(cls, obj):
        return obj['tel'][0]

    def __init__(self, *args, **kwargs):
        super(Entity, self).__init__(*args, **kwargs)

if __name__ == '__main__':
    json_str = '{"sid":"6f79a3403ab744c295732246646b5a0c","recommend_name":"麦当劳(东四十条店)","recommend_mainname":"麦当劳","recommend_head":"麦当劳","recommend_branch":"东四十条店","recommend_catkey":"","recommend_address":"工体北路巨石大厦1层","recommend_lat":39.933859,"recommend_lng":116.437943,"recommend_category_name":[],"recommend_category_id":[],"region":{"country_code":86,"country_name":"中国","province_name":"北京","province_id":1,"city_id":2,"city_name":"北京","district_name":"东城区","district_id":3},"inputs":[{"sid":"6f79a3403ab744c295732246646b5a0c","name":"麦当劳(东四十条店)","mainname":null,"name_key":["麦当劳"],"region":{"country_code":86,"country_name":"中国","province_name":"北京","province_id":1,"city_id":2,"city_name":"北京","district_name":"东城区","district_id":3},"address":{"province":"北京","city":"北京","district":"东城区","street":"工体北路巨石大厦1层","extra":""},"postcode":"","lat":39.933859,"lng":116.437943,"category_name":["其他生活服务"],"category_id":[12],"tel":[{"number":"010-64156840","desc":"电话","type":3,"ranking":0,"score":0.0,"status":-1,"tel_branch":""}],"tag":[],"topic":[],"source_id":200,"source_name":"mapbaidu","link":"","ranking":-1,"tempid":"08766272b9a83f9d7fe8af2b150c6b62","version":"20160311095628","extend":{"addrlevel":1, "fax": "", "disp_order":"5984"},"hidden":0,"official_url":"","intro":"","score":0.0,"comment_count":-1,"logo":""}],"tally":1}'
    import json
    obj = json.loads(json_str)
    from pprintpp import pprint as pp
    #pp(obj['inputs'])
    for _obj in obj['inputs']:
        pp(Tempid().load(obj=_obj))
