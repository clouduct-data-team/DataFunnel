# -*- coding: utf-8 -*-
'''
'''
from ABC.feature import Feature
import random
from nltk.metrics.distance import jaccard_distance
from pprintpp import pprint as pp
from pprintpp import pformat as pf
import json
from collections import defaultdict
import time
import logging
import jieba
import jieba.analyse
from jieba.analyse.tfidf import TFIDF
from utils.index_utils import escapeEsSolrString as escapeEsSolrString
import index_convert_utils
logger = logging.getLogger('merge.feature')

class NameFeature(Feature):

    _jieba = TFIDF()
    _jieba.set_idf_path('projects/yulore/sample_data/name.txt')
    _jieba.set_stop_words('projects/yulore/sample_data/stopwords.txt')
    idx = 0
    name = 'name'
    candidate_threshold = 100000000

    def __init__(self):
        pass
        #similar_threshold = 10

    @classmethod
    def load_essential(cls, args):
        return {}

    @classmethod
    def get_keys(cls, name_str):
        #seg_list = NameFeature._jieba.cut_for_search(name_str)
        #seg_list = NameFeature._jieba.cut(name_str, cut_all=False)
        seg_list = []
        #NameFeature._jieba.analyse.extract_tags(name_str)
        token_weight_tuple_list = NameFeature._jieba.extract_tags(name_str, 
                                                                  withWeight=True,
                                                                 topK=3)
        for token_weight_tuple in token_weight_tuple_list:
            seg_list.append(token_weight_tuple[0])
        #https://github.com/fxsjy/jieba/blob/master/test/extract_tags_stop_words.py
        return seg_list


    @classmethod
    def get_feature(cls, entity):
        feature_field = NameFeature.name
        name_str = getattr(entity, feature_field, None)
        seg_list = []
        if name_str is not None:
            token_weight_tuple_list = NameFeature._jieba.extract_tags(name_str, 
                                                                      withWeight=True,
                                                                      topK=3)
            for token_weight_tuple in token_weight_tuple_list:
                seg_list.append(token_weight_tuple[0])
        return seg_list

    @classmethod
    def get_entity_similarity(cls, entity1, entity2):
        f1 = cls.get_feature(entity1)
        f2 = cls.get_feature(entity2)
        f1_str = " ".join(f1)
        f2_str = " ".join(f2)
        similarity = cls.get_similarity(f1, f2)
        result = {
            'similarity': similarity,
            'feature': "%s, %s : %s " % (
                json.dumps(f1_str, ensure_ascii=False),
                json.dumps(f2_str, ensure_ascii=False),
                similarity)
        }
        return result

    @classmethod
    def get_similarity(cls, f1, f2):
        if 0 in [len(f1), len(f2)]:
            return 1
        #print('get_similarity 1')
        #pp(f1)
        #pp(f2)
        s1, s2 = set(f1), set(f2)
        #print('get_similarity 2')
        #pp(s1)
        #pp(s2)
        union_set = s1.union(s2)
        intersection_set = s1.intersection(s2)
        
        default_miss = 15 

        union_set_counts = 0
        intersection_set_counts = 0
        for feature in intersection_set:
            if feature in NameFeature._jieba.idf_freq:
                #print("feature %s - %s in idf_freq" % (feature,NameFeature._jieba.idf_freq[feature]))
                intersection_set_counts += NameFeature._jieba.idf_freq[feature]
            else:
                print("feature %s not in idf_freq" % feature)
                intersection_set_counts += default_miss
                #time.sleep(0.3)

        union_set_counts = intersection_set_counts
        for feature in union_set:
            if feature in intersection_set:
                continue
            if feature in NameFeature._jieba.idf_freq:
                #print("feature %s - %s in idf_freq" % (feature,
                #                                       NameFeature._jieba.idf_freq[feature]))
                union_set_counts += NameFeature._jieba.idf_freq[feature]
            else:
                union_set_counts += default_miss
                #print("feature %s not in idf_freq" % feature)
                #time.sleep(0.3)
                
        #similarity = jaccard_distance(set(f1), set(f2))
        similarity = float(intersection_set_counts) / union_set_counts
        return similarity

class AddressFeature(Feature):

    _jieba = None
    idx=1

    _jieba = TFIDF()
    _jieba.set_idf_path('projects/yulore/sample_data/address.txt')
    _jieba.set_stop_words('projects/yulore/sample_data/stopwords_address.txt')
    idx = 0
    name = 'address'
    candidate_threshold = 5000000

    @classmethod
    def load_essential(cls, args):
        #pp({'district_name_id_map': AddressFeature.load_district_name_id_map(args)})
        return {'district_name_id_map': cls.load_district_name_id_map(args)}

    @classmethod
    def load_district_name_id_map(cls, args):
        district_name_id_map = {}
        map_fpath = "projects/yulore/sample_data/district_id_name_map_%s.csv" % args['country_code']
        with open(map_fpath, 'r') as f:
            for line in f:
                district_id, district_name, city_id = line.split(',')
                if city_id.strip() == args['city_id']:
                    district_name_id_map[district_name.replace('"','')] = district_id
        return district_name_id_map

    def __init__(self, args):
        pass
        # similar_threshold = 10

    @classmethod
    def get_keys(cls, address_obj):
        seg_list = []
        #token_weight_tuple_list = AddressFeature._jieba.cut_for_search(address_obj['street'])
        token_weight_tuple_list = AddressFeature._jieba.extract_tags(address_obj['street'])
        #pp(token_weight_tuple_list)
        for token_weight_tuple in token_weight_tuple_list:
            seg_list.append(token_weight_tuple)
        #extract_tags(content,  withWeight=True)
        #[('祥芳园', 7.03311445485), ('酒家', 3.45172194529)]
        
        #seg_list = NameFeature._jieba.cut(name_str, cut_all=False)
        #address_str = " ".join(feature_dict.values())
        #seg_list = AddressFeature._jieba.analyse.extract_tags(address_str)
        #https://github.com/fxsjy/jieba/blob/master/test/extract_tags_stop_words.py
        #seg_list
        #print('token address:' + address_obj['street'])
        #pp(seg_list)
        return seg_list

    @classmethod
    def get_feature(cls, entity):
        address_str = entity["address"]["street"]
        #address_str = getattr(entity, feature_field, None)
        #print('name_str', name_str)
        seg_list = []
        if address_str is not None:
            seg_list = AddressFeature._jieba.extract_tags(address_str)
        return seg_list

    @classmethod
    def get_entity_similarity(cls, entity1, entity2):
        f1 = cls.get_feature(entity1)
        f2 = cls.get_feature(entity2)
        f1_str = " ".join(f1)
        f2_str = " ".join(f2)
        similarity = cls.get_similarity(f1, f2)
        result = {
            'similarity': similarity,
            'feature': "%s, %s : %s " % (
                json.dumps(f1_str, ensure_ascii=False),
                json.dumps(f2_str, ensure_ascii=False),
                similarity)
        }
        '''
        f1 = AddressFeature.get_feature(entity1)
        f2 = AddressFeature.get_feature(entity2)

        logger.debug('=================AddressFeature:\n %s:%s \n%s:%s' % \
                     (entity1['address']['street'], pf(f1), entity2['address']['street'], pf(f2)))
        return AddressFeature.get_similarity(f1, f2)
        '''
        return result

    @classmethod
    def get_similarity(cls, f1, f2):
        if 0 in [len(f1), len(f2)]:
            return 1
        #print('get_similarity 1')
        #pp(f1)
        #pp(f2)
        s1, s2 = set(f1), set(f2)
        #print('get_similarity 2')
        #pp(s1)
        #pp(s2)
        union_set = s1.union(s2)
        intersection_set = s1.intersection(s2)
        
        default_miss = 15 

        union_set_counts = 0
        intersection_set_counts = 0
        for feature in intersection_set:
            if feature in AddressFeature._jieba.idf_freq:
                #print("feature %s - %s in idf_freq" % (feature,NameFeature._jieba.idf_freq[feature]))
                intersection_set_counts += AddressFeature._jieba.idf_freq[feature]
            else:
                print("feature %s not in idf_freq" % feature)
                intersection_set_counts += default_miss
                #time.sleep(0.3)

        union_set_counts = intersection_set_counts
        for feature in union_set:
            if feature in intersection_set:
                continue
            if feature in AddressFeature._jieba.idf_freq:
                #print("feature %s - %s in idf_freq" % (feature,
                #                                       NameFeature._jieba.idf_freq[feature]))
                union_set_counts += AddressFeature._jieba.idf_freq[feature]
            else:
                union_set_counts += default_miss
                #print("feature %s not in idf_freq" % feature)
                #time.sleep(0.3)
                
        #similarity = jaccard_distance(set(f1), set(f2))
        similarity = float(intersection_set_counts) / union_set_counts
        return similarity
    '''
    def get_similarity(cls, f1, f2):
        if 0 in [len(f1), len(f2)]:
            return 1
        similarity = jaccard_distance(set(f1), set(f2))
        return similarity
    '''

class TelFeature(Feature):
    #import jieba
    #import jieba.analyse
    name = 'tel'
    idx=2
    """
    [
        {"number":"010-85382313","desc":"电话","type":3,"ranking":0,"score":0.0,"status":0,"tel_branch":""}
    ]
    """
    candidate_threshold = 2000

    def __init__(self):
        pass

    @classmethod
    def load_essential(cls, args):
        return {}

    @classmethod
    def get_keys(cls, tel_list):
        feature_list = []
        for tel in tel_list:
            tokens = tel['number'].split("-")
            for token in tokens:
                if len(token) > 3:
                    feature_list.append(token)
        return feature_list

    @classmethod
    def get_feature(cls, entity):
        feature_list = []
        for tel in entity['tel']:
            #if tel['type'] == 3:
            tokens = tel['number'].split("-")
            for token in tokens:
                if len(token) > 3:
                    feature_list.append(token)
        return feature_list

    @classmethod
    def get_entity_similarity(cls, entity1, entity2):
        f1 = cls.get_feature(entity1)
        f2 = cls.get_feature(entity2)
        f1_str = " ".join(f1)
        f2_str = " ".join(f2)
        similarity = cls.get_similarity(f1, f2)
        result = {
            'similarity': similarity,
            'feature': "%s, %s : %s " % (
                json.dumps(f1_str, ensure_ascii=False),
                json.dumps(f2_str, ensure_ascii=False),

                similarity)
        }
        return result

    @classmethod
    def get_similarity(cls, f1, f2):
        if 0 in [len(f1), len(f2)]:
            return 0
        similarity = 1 - jaccard_distance(set(f1), set(f2))
        return similarity


class FeatureSet(object):
    map = {
        'name': NameFeature,
        'tel': TelFeature,
        'address': AddressFeature
    }


    def __init__(self, args):
        print('featureset __init__')
        for feature_name, feature_class in FeatureSet.map.items():
            self.__dict__[feature_name] = feature_class

        self.extend_data = self.load_essential(args)
        #pp(self.extend_data)
        print('finished')

    def load_essential(self, args):
        _args = {}
        for fname, f_class in FeatureSet.map.items():
            tmp = {fname: f_class.load_essential(args)}
            _args.update(tmp)
        return _args

    @classmethod
    def convert_solr_query_result(cls, query_result):
        gid_group_map = {}
        for _group in query_result:
            try:
                if _group['score'] < 3:
                    continue
                _tmp = {'score': _group['score'], 
                        'sid': _group['shopid']}
                for fname in FeatureSet.map.keys():
                    if fname == "tel":
                        _tmp['tel'] = [_tel.split(":")[-1] for _tel in _group['tel']]
                    else:
                        _tmp[fname] = _group[fname]
            except Exception as e:
                print(str(e))
                pp(_group)
            gid_group_map[_tmp['sid']] = _tmp
        return gid_group_map

    def convert_es_query_result(cls, query_result):
        gid_group_map = {}
        for _group in query_result:
            try:
                if _group['_score'] < 20:
                    continue
                _tmp = {'score': _group['_score'],
                        'sid': _group['_source']['sid']}
                for fname in FeatureSet.map.keys():
                    if fname == "tel":
                        _tmp['tel'] = []
                        for entity in _group['_source']['inputs']:
                            for tel in entity["tel"]:
                                if tel['number'] not in _tmp['tel']:
                                    _tmp['tel'].append(tel['number'])
                    else:
                        _tmp[fname] = _group['_source'][fname]
            except Exception as e:
                print(str(e))
                pp(_group)
            gid_group_map[_tmp['sid']] = _tmp
        return gid_group_map

    def convert_query_result(cls, index_type, query_result):
        if index_type == "EsIndexAdm":
            return cls.convert_es_query_result(query_result)
        elif index_type == "SolrIndexAdm":
            return cls.convert_solr_query_result(query_result)
        else:
            raise
 
    @classmethod
    def convert_payload_to_obj(cls, payload, gid, to_type, args):
        index_obj = None
        if gid is None: # from group_str
            group_obj = json.loads(payload)
            query_body = index_convert_utils.group_obj_2_es_doc(group_obj)
        elif type(gid) is str: # from entity_str, add by merge
            entity_obj = json.loads(payload)
            query_body = index_convert_utils.entity_obj_2_es_doc(gid , entity_obj)
        else:
            raise
        if to_type == "EsIndexAdm":
            index_obj = {
                'body': {
                    'doc': query_body,
                    'doc_as_upsert': True
                },
                'index': args['tmp-index-name'],
                'doc_type': 'shop',
                'id': query_body['sid']
            }
        elif to_obj == "SolrIndexAdm":
            pass
        return index_obj
        

    def get_query_obj(self, index_type, args, entity):
        if index_type == "EsIndexAdm":
            return self.get_es_query_obj(args, entity)
        elif index_type == "SolrIndexAdm":
            return self.get_solr_query_obj(args, entity)
        else:
            raise

    def get_solr_query_obj(self, args, entity):
        fl = "name, address, tel, shopid, score"
        '''
        q = "(%s) AND (%s OR %s)" % \
            (" OR ".join(['tel:"%s"'% _['number'] for _ in entity['tel']]),
             'name:%s' % entity['name'],
             'address:%s' % entity['address']['street'])
        '''
        name = escapeEsSolrString(entity['name'].replace("()", ""))
        address = escapeEsSolrString(entity['address']['street'])
        
        if address != "":
            q = "(%s) OR (%s AND  %s)" % \
                (" OR ".join(['tel:"%s"'% _['number'] for _ in entity['tel']]),
                 'name:%s' % name, 'address:%s' % address)
        else:
        # street is empty
            q = "(%s) OR %s" % \
                (" OR ".join(['tel:"%s"'% _['number'] for _ in entity['tel']]),
                    'name:%s' % name)
        if entity['address']['district'] != "":
            fq = ("city_id: %s"% args['city_id'], "dis_id:%s" %  \
                self.extend_data['address']['district_name_id_map'][entity['address']['district']], "type:1")
        else:
            fq = ("city_id: %s"% args['city_id'], "type:1")

        query_obj = {
            'q': q,
            'fq': fq,
            # TO-DO: IMPORTANT !! REPLACE HERE
            'fl': fl,
            'q.op': 'OR',
            "rows": "3"
        }
        return query_obj

    
    def get_es_query_obj(self, args, entity):
        def get_es_return_field():
            return {
                "_source": [
                    "sid",
                    "name",
                    "address",
                    "inputs.name",
                    "inputs.address",
                    "inputs.tel.number"
                ]
            }
        def get_es_filter_part(args, entity):
            _filter = {
                    "bool": {
                        "should": [
                            {"term": {"city_id": args['city_id']}}
                        ]
                    }
            }
            if entity['address']['district'] != "":
                _filter["bool"]["should"].append(
                    {"term": {"distric": entity['address']['district']}})
            return _filter
    
        name = escapeEsSolrString(entity['name'].replace("()", ""))
        address = escapeEsSolrString(entity['address']['street'])

        query_body = {
            "query": {
                "bool": {"must":[], "filter":{}}
            }
        }

        name_address_obj = { "bool": { "should": [
            {"match": {"name": name}}
        ]}}
        if address  != "":
            name_address_obj['bool']['should'].append(
                {"match": {"address": address}})
        tel_obj = {"bool": {"should": []}}
        for tel in entity['tel']:
            _tel_obj = {
                "nested": {
                    "path": "inputs",
                    "query": {
                        "term": {"inputs.tel.number": tel['number']}
                    }
                }
            }
            tel_obj['bool']['should'].append(_tel_obj)

        query_body['query']['bool']['must'].append(name_address_obj)
        query_body['query']['bool']['must'].append(tel_obj)
        query_body['query']['bool']['filter'].update(get_es_filter_part(args, entity))
        query_body.update(get_es_return_field())
        query_obj = {
            'body': query_body,
            'doc_type': 'shop',
            'size': 3
        }
        return query_obj

    @classmethod
    def get_feature(cls, entity):
        result = {}
        for f_name, f_class in cls.map.items():
            result[f_name] = f_class.get_feature(entity)
        return result

    def should_be_merge(self, group, entity):
        '''
        this function indicate whether group and entity should be merge together
        it first get similarity matrix between group and entity
        feature -> sim_matrix -> linkage_matrix -> screne_feature -> screne_classifier
        '''
        show_msg = False
        sim_matrix = self.get_group_entity_similarity(group, entity)
        result = {}

        distance_matrix, distance_matrix_hit = self.get_distance(sim_matrix)
        result['linkage'], result['distance'] = distance_matrix, distance_matrix_hit
        logger.debug('-------- Start -------')
        if show_msg:
            logger.debug("distance_matrix:\n%s\ndistance_matrix_hit:\n%s" % \
                         (pf(distance_matrix_hit), pf(distance_matrix)))

        screne_feature = self.get_screne_feature(distance_matrix)
        result['screne_feature'] = screne_feature
        if show_msg:
            logger.debug("screne_feature:\n%s" % pf(screne_feature))

        classify_result = self.screne_classify(screne_feature)
        result['classify_result'] = classify_result
        if show_msg:
            logger.debug("classify_result:\n%s" % pf(classify_result))
        logger.debug('-------- END -------')
        #return classify_result
        return result


    def should_be_merge_bt_groups(self, group1, group2):
        for eid1, entity1 in group1.map.items():
            result = self.should_be_merge(group2, entity1)
            merge_flag = result['classify_result'][1]
            if merge_flag == True:
                return result
        return result

    def get_entity_similarity(self, e1, e2):
        f_sim_map = {}
        for f_name, f_class in FeatureSet.map.items():
            result = f_class.get_entity_similarity(e1, e2)
            # result = {
            #    'similarity': simiarity,
            #    'feature': feature+ simiarity
            #}
            f_sim_map[f_name] = result['feature']
        return f_sim_map

    def get_group_entity_similarity(self, group, entity):
        result = []
        hit = 0
        for eid, _entity in group.map.items():
            similarity = self.get_entity_similarity(_entity, entity)
            result.append(similarity)
        return result

    def keys(self):
        _l = list(self.map.keys())
        _l.sort()
        return _l

    def items(self):
        for feature_name, feature_class in FeatureSet.map.items():
            yield feature_name, feature_class

    def get_distance(self, sim_matrix):
        # ref: http://tinyurl.com/3cm8fnq
        def complete_linkage_agglomerative(sim_matrix):
            distance_matrix = {}

            for feature_name, feature_class in self.map.items():
                distance_matrix[feature_name] = 0
            distance_matrix_hit = {}

            for sim in sim_matrix:
                for feature_name, value_with_feature in sim.items():
                    # ex: "\"北京齿科SOS救援中心国际\", \"北京SOS诊所国际\" : 0.5714285714285714 "
                    value = float(value_with_feature.split(u":")[-1])
                    if value >= distance_matrix[feature_name]:
                        distance_matrix_hit[feature_name] = value_with_feature
                        distance_matrix[feature_name] = value
            return distance_matrix, distance_matrix_hit

        def average_linkage_agglomerative(sim_matrix):
            distance_matrix, f_name_counts = {}, {}

            for feature_name, feature_class in self.map.items():
                distance_matrix[feature_name] = 0
                f_name_counts[feature_name] = 0

            distance_matrix_hit = {}

            for sim in sim_matrix:
                for feature_name, value_with_feature in sim.items():
                    # ex: "\"北京齿科SOS救援中心国际\", \"北京SOS诊所国际\" : 0.5714285714285714 "
                    value = float(value_with_feature.split(u":")[-1])
                    f_name_counts[feature_name] += value
            for feature_name, feature_class in self.map.items():
                distance_matrix[feature_name] = f_name_counts[feature_name]/len(sim_matrix)
            return distance_matrix, distance_matrix_hit

        def single_linkage_agglomerative(sim_matrix):
            distance_matrix = {}

            for feature_name, feature_class in self.map.items():
                distance_matrix[feature_name] = 1
            distance_matrix_hit = {}

            for sim in sim_matrix:
                for feature_name, value_with_feature in sim.items():
                    # ex: "\"北京齿科SOS救援中心国际\", \"北京SOS诊所国际\" : 0.5714285714285714 "
                    value = float(value_with_feature.split(u":")[-1])
                    if value <= distance_matrix[feature_name]:
                        distance_matrix_hit[feature_name] = value_with_feature
                        distance_matrix[feature_name] = value
            return distance_matrix, distance_matrix_hit
        return average_linkage_agglomerative(sim_matrix)

    def get_screne_feature(self, distance_matrix, input_format='linkage'):
        threshold = {
            'name': [0.3, 0.6],
            'tel': [0.4, 0.6],
            'address': [0.3, 0.5]
        }
        screne_feature = {}
        for feature, distance in distance_matrix.items():
            if input_format != 'linkage':
                distance = float(distance.split(u":")[-1])
            if distance >= threshold[feature][1]:# indicate identical 
                screne_feature[feature] = 1
            elif distance >= threshold[feature][0]:
                screne_feature[feature] = 0.5
            else:
                screne_feature[feature] = 0
        return screne_feature
    
    def get_e2e_cohesion(self, similarity):
        cohesion = 0
        for f_name, sim_str in similarity.items():
            if f_name == 'name':
                _similarity = float(sim_str.split(':')[-1])
                cohesion += 0.2 * _similarity
            if f_name == 'tel':
                _similarity = float(sim_str.split(':')[-1])
                cohesion += 0.6 * _similarity
            if f_name == 'address':
                _similarity = float(sim_str.split(':')[-1])
                cohesion += 0.4 * _similarity
        return cohesion

    def screne_classify(self, screne_feature):
        switcher = {
            # (Name, Tel, Address)
            # --- name --- 1
            (  1,   1,   1): ( 1, True,   0),
            (0.5,   1,   1): (10, True,   1), # 名稱相近，其餘一樣
            (  1,   1, 0.5): ( 2, True,   2), # 地址相似
            (  1, 0.5,   1): ( 4, True,   3), # 這狀況比較特殊
            (0.5,   1, 0.5): (11, True,   6), #
            (0.5, 0.5,   1): (13, True,   7),
            (  1, 0.5, 0.5): ( 5, True,   8), # 這狀況比較特殊
            (  1,   1,   0): ( 3, True,   9),   # 這也很怪
            (  1,   0,   1): ( 7, False, 21), # 電話抓錯？
            (  0,   1,   1): (19, False, 19),
            (  1, 0.5,   0): ( 6, False, 23), # 有可能抓取錯誤，分店名沒抓到
            (  1,   0, 0.5): ( 8, False, 19),
            (  1,   0,   0): ( 9, False, 25), # 有可能抓取錯誤，分店都用總店名表示
            #---- name --- 0.5
            (0.5,   1,   0): (12, True, 20),
            (0.5, 0.5, 0.5): (14, True, 25),
            (0.5, 0.5,   0): (15, True, 23),
            (0.5,   0,   1): (16, True, 22),
            (0.5,   0, 0.5): (17, True, 26),
            (0.5,   0,   0): (18, True, 28), # 
            #---- name --- 0
            (  0,   1, 0.5): (20, False, 19),
            (  0,   1,   0): (21, False, 19), # 電話號碼被登記走了
            (  0, 0.5,   1): (22, False, 20),
            (  0, 0.5, 0.5): (23, False, 23),
            (  0, 0.5,   0): (24, False, 27), #
            (  0,   0,   1): (25, False, 19), # 同地址不同單位，園區、校園
            (  0,   0, 0.5): (26, False, 29),
            (  0,   0,   0): (27, False, 30)
        }
        tuple_screne_feature = (screne_feature['name'],
                                screne_feature['tel'],
                                screne_feature['address'])
        return switcher.get(tuple_screne_feature, None)
