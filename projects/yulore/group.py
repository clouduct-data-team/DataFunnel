# -*- coding: utf-8 -*-
from ABC.group import BaseGroup
from entity import Entity
import json
import uuid
import recommend
import orm

class Group(BaseGroup):
    Entity = Entity

    MAPPING = {
        'id': 'sid',
        'list': 'inputs',
        'eid': 'eid'
    }

    @classmethod
    def get_recommend(cls, group):
        args = {}
        args['source_ranking_map'] = orm.get_source_ranking_map()
        recommend_fields = {
            'recommend_name': recommend.name,
            'recommend_mainname': recommend.mainname,
            'recommend_head': recommend.head,
            'recommend_branch': recommend.branch,
            'recommend_catkey': '',
            'recommend_address': recommend.address,
            'recommend_lat': recommend.lat,
            'recommend_lng': recommend.lng,
            'recommend_category_name': '',
            'recommend_category_id': '',
            'region': '',
            'tally':recommend.tally 
        }
        recommend_result = {}
        for field_name, field_value in recommend_fields.items():
            if callable(field_value):
                recommend_result[field_name] = field_value(group, args)
            else:
                recommend_result[field_name] = field_value
        return recommend_result

    def gen_gid(cls, group):
        return str(uuid.uuid4()).replace("-", "")

    def __init__(self, *args, **kwargs):
        super(Group, self).__init__(*args, **kwargs)

    def to_redis(self):
        dump = {
          'sid': self.id,
          'inputs': []
        }
        for key, entity in self.map.items():
            dump['inputs'].append(entity.to_json())
        return json.dumps(dump,  ensure_ascii=False)

if __name__ == '__main__':
    json_str = '{"sid":"6f79a3403ab744c295732246646b5a0c","recommend_name":"麦当劳(东四十条店)","recommend_mainname":"麦当劳","recommend_head":"麦当劳","recommend_branch":"东四十条店","recommend_catkey":"","recommend_address":"工体北路巨石大厦1层","recommend_lat":39.933859,"recommend_lng":116.437943,"recommend_category_name":[],"recommend_category_id":[],"region":{"country_code":86,"country_name":"中国","province_name":"北京","province_id":1,"city_id":2,"city_name":"北>京","district_name":"东城区","district_id":3},"inputs":[{"sid":"6f79a3403ab744c295732246646b5a0c","name":"麦当劳(东四十条店)","mainname":null,"name_key":["麦>当劳"],"region":{"country_code":86,"country_name":"中国","province_name":"北京","province_id":1,"city_id":2,"city_name":"北京","district_name":"东城区","district_id":3},"address":{"province":"北京","city":"北京","district":"东城区","street":"工体北路巨石大厦1层","extra":""},"postcode":"","lat":39.933859,"lng":116.437943,"category_name":["其他生活服务"],"category_id":[12],"tel":[{"number":"010-64156840","desc":"电话","type":3,"ranking":0,"score":0.0,"status":-1,"tel_branch":""}],"tag":[],"topic":[],"source_id":200,"source_name":"mapbaidu","link":"","ranking":-1,"tempid":"08766272b9a83f9d7fe8af2b150c6b62","version":"20160311095628","extend":{"addrlevel":1, "fax": "","disp_order":"5984"},"hidden":0,"official_url":"","intro":"","score":0.0,"comment_count":-1,"logo":""}],"tally":1}'
    from tempid import Tempid as Entity
    ShopGroup.Entity = Entity 
    shop = ShopGroup(json_str=json_str)
    from pprintpp import pprint as pp 
    pp(shop)
