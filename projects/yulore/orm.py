from pony.orm import Database, PrimaryKey, Required, sql_debug, db_session


db = Database()


class Source(db.Entity):
    _table_ = 'dict_source'
    id = PrimaryKey(int, auto=True)
    source = Required(str)
    source_ch = Required(str)
    source_url = Required(str)
    config = Required(str)
    status = Required(int)
    ranking = Required(int)

class Catkey(db.Entity):
    _table_ = 'dict_catkey'
    id = PrimaryKey(int, auto=True)
    catkey = Required(str)
    country_code = Required(int)

# pny.sql_debug(True)
db.bind(
    'mysql', user='bigdata', passwd='bigdb@2015qwe',
    host='172.18.19.13', port=3306, db='yule')
sql_debug(True)
db.generate_mapping(create_tables=False)


@db_session
def get_catkey():
    return {
        catkey.catkey: catkey.country_code
        for catkey in Catkey.select()
    }
@db_session
def get_source_ranking_map():
    return {
        source.source: source.ranking
        for source in Source.select()
    }

@db_session
def get_source_ch_map():
    return {
        source.source: source.source_ch
        for source in Source.select()
    }


if __name__ == '__main__':
    source_ranking_map = get_source_ranking_map()
    from pprintpp import pprint as pp
    pp(source_ranking_map)
