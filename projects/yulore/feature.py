# -*- coding: utf-8 -*-
from ABC.featureset import FeatureSet
import uuid
import copy
import sys
sys.path.append('projects/yulore/')
import json
import ujson
from collections import OrderedDict
import recommend
from utils.logger import get_logger
logger = get_logger('Feature')


class Feature(FeatureSet):

    NON_EXTEND_FIELDS = set([
        'name', 'address', 'country_code', 'country_name',
        'province_name', 'province_id',
        'city_id', 'city_name', 'district_name',
        'district_id', 'postalcode', 'lat', 'lng',
        'category_name', 'category_id', 'source_id',
        'source_name', 'link', 'tel', 'tag', 'topic',
        'ranking', 'tempid', 'version',
    ])

    TEL_ADDITION_FIELDS = dict(
        ranking=0,
        score=0,
        status=0,
        tel_branch=0,
    )

    ENTITY_ORDERED_KEY = [
          "sid", "name", "mainname", "name_key", "region", "address",
          "postcode", "lat", "lng", "category_name", "category_id", "tel",
          "tag", "topic", "source_id", "source_name", "link", "ranking",
          "tempid", "version", "extend", "hidden"]

    GROUP_ORDERED_KEY = [
          "sid", "recommend_name", "recommend_mainname", "recommend_head",
          "recommend_branch", "recommend_catkey", "recommend_address",
          "recommend_lat", "recommend_lng", "recommend_category_name",
          "recommend_category_id", "region", "inputs", "tally"]

    '''
    entity_keys = [
            'sid',
            'lat',
            'lng',
            'category_name',
            'category_id',
            'region',
            'address',
            'tel',
            'source_id',
            'source_name',
            'hidden',
            'postcode',
            'link',
            'tempid',
            'ranking',
            'name',
            'name_key',
            'topic', 
            'tag',
            'version', 
            'mainname',
            'intro',
            'score',
            'logo',
            'comment_count',
    ]
    '''

    map = {
        'name': None,
        'tel': None,
        'address': None
    }

    entity = {
        'eid': 'tempid',
        'name': 'name'
    }
    group = {
        'gid': 'sid',
        'list': 'inputs'
    }

    def __init__(self, args):
        pass

    @classmethod
    def dump_group(cls, group_obj):
        for i, e in enumerate(group_obj['inputs']):
            kvs = []
            for k in cls.ENTITY_ORDERED_KEY:
                if k in e:
                    kvs.append((k, e[k]))
                    del e[k]
            kvs.extend(e.items())
            group_obj['inputs'][i] = OrderedDict(kvs)
        group_obj['tally'] = len(group_obj['inputs'])
        kvs = []
        for k in cls.GROUP_ORDERED_KEY:
            if k in group_obj:
                kvs.append((k, group_obj[k]))
                del group_obj[k]
        kvs.extend(group_obj.items())
        return json.dumps(OrderedDict(kvs), ensure_ascii=False)

    def get_most_like_gid(merge_candidate_gid_sim_list):
        sim, candidate = 0, merge_candidate_gid_sim_list[0]
        for gid_sim_reason_tuple in merge_candidate_gid_sim_list:
            #logger.debug("\t\t%s" % str(gid_sim_reason_tuple))
            if float(gid_sim_reason_tuple[1]) > sim:
                sim = float(gid_sim_reason_tuple[1])
                candidate = gid_sim_reason_tuple
        return candidate[0]

    def load_district_name_id_map(args):
        district_name_id_map = {}
        map_fpath = "projects/yulore/sample_data/district_id_name_map_%s.csv" % args['country_code']
        with open(map_fpath, 'r') as f:
            for line in f:
                district_id, district_name, city_id = line.split(',')
                if city_id.strip() == args['city_id']:
                    district_name_id_map[district_name.replace('"','')] = district_id
        return district_name_id_map

    def load_source_ranking_map():
        #import orm
        #return orm.get_source_ranking_map()
        with open('projects/yulore/source_ranking_map.json', 'r') as f:
            source_ranking_map = json.load(f)
        return source_ranking_map
    def load_essential(args):
        #pp({'district_name_id_map': AddressFeature.load_district_name_id_map(args)})
        appendix = {
        #        'district_name_id_map': Feature.load_district_name_id_map(args),
        #        'source_ranking_map':  Feature.load_source_ranking_map()
        }
        return appendix

    @classmethod
    def e2g_should_merge(cls, args, entity, group, query_group_result):
        should_merge, sim, reason = False, 0, ''
        if query_group_result['score'] >= 20:
            should_merge, sim, reason = True, query_group_result['score'], ''
        #_sim, reason = cls.get_e2g_sim(args, entity, group)
        #if _sim > 0.7 :
        #    should_merge = True
        return should_merge, sim, reason

    @classmethod
    def g2g_should_merge(cls, args, group1, group2):
        should_merge = True
        sim = 0
        reason = ""
        return should_merge, sim, reason


    @classmethod
    def get_e2g_sim(cls, args, entity, group):
        sim_list, reason_list = [], []
        for _entity in group[Feature.group['list']]:
            _sim, _reason = cls.get_e2e_sim(entity, _entity)
            sim_list.append(_sim)
            reason_list.append(_sim)
        
        sim = reason = None
        return sim, reason

    @classmethod
    def get_e2e_sim(cls, args, entity1, entity2):
        sim = reason = None
        return sim, reason

    @classmethod
    def get_g2g_sim(cls, args, group1, group2):
        sim = reason = None
        return sim, reason

    @classmethod
    def get_entity_feature(cls, args, entity):
        feature_map = {
            'fname1': None,
            'fname2': None
        }
        return feature_map

    @classmethod
    def get_recommand_group(cls, args, group_obj):
        pass

    @classmethod
    def get_region(cls, entity):
        region = {
            'country_code': entity['country_code'],
            'country_name': entity['country_name'],
            'province_name': entity['province_name'],
            'province_id': entity['province_id'],
            'city_id': entity['city_id'],
            'city_name': entity['city_name'],
            'district_name': entity['district_name'],
            'district_id': entity['district_id']
        }
        return region


    '''
    @classmethod
    def get_extend(cls, entity):
        extend = {}
        for key, value in entity.items():
            if key not in cls.entity_keys:
                extend[key] = value
        extend_json_str = json.dumps(extend, ensure_ascii=False)
        return extend_json_str
    '''

    @classmethod
    def convert_e4g(cls, gid, entity_obj):
        extend_keys = set(entity_obj.keys()) - cls.NON_EXTEND_FIELDS
        for tel in entity_obj['tel']:
            tel.update(cls.TEL_ADDITION_FIELDS)
        entity_obj.update(dict(
            sid=gid,
            region=dict(
                country_code=entity_obj.pop('country_code'),
                country_name=entity_obj.pop('country_name'),
                province_name=entity_obj.pop('province_name'),
                province_id=entity_obj.pop('province_id'),
                city_id=entity_obj.pop('city_id'),
                city_name=entity_obj.pop('city_name'),
                district_name=entity_obj.pop('district_name'),
                district_id=entity_obj.pop('district_id'),
            ),
            source_id=int(entity_obj['source_id']),
            hidden=0,
            postcode=entity_obj.pop('postalcode', ''),
            name_key=entity_obj.get('name_key', []),
            topic=None,
            mainname=entity_obj.get('mainname'),
            intro=entity_obj.get('intro', ''),
            ranking=-1,
            score=entity_obj.get('score', -1.0),
            logo=entity_obj.get('logo', ''),
            comment_count=entity_obj.get('comment_count', -1),
        ))
        if extend_keys:
            entity_obj['extend'] = ujson.dumps({
                    k: entity_obj.pop(k)
                    for k in extend_keys
                }, ensure_ascii=False, escape_forward_slashes=False, sort_keys=True)
        return entity_obj


    @classmethod
    def convert_entity_with_inputs(cls, sid, entity):

        new_entity = copy.deepcopy(entity)

        tel = []
        tel_addition_fields = {
            'ranking': 0,
            'score': 0,
            'status': 0,
            'tel_branch': 0
        }
        for tmp_tel in new_entity['tel']:
            tmp_tel.update(tel_addition_fields)
            tel.append(tmp_tel)

        new_entity_extal_fields = {
            'sid': sid,
            'mainname': None,
            'name_key': cls.get_name_key(new_entity), 
            'region': cls.get_region(new_entity),
            'address': new_entity['address'],
            'tel': tel,
            'postcode': new_entity.get('postalcode', ''),
            'intro': cls.get_intro(new_entity),
            'logo': cls.get_logo(new_entity),
            'ranking': -1,
            'hidden': 0,
            'score': 0,
            'comment_count': cls.get_comment_count(entity),
            'topic': []
        }

        new_entity.update(new_entity_extal_fields)
        return new_entity

    @classmethod
    def get_comment_count(cls, entity):
        if 'comment_count' in entity:
            return entity['comment_count']
        return ''

    @classmethod
    def get_logo(cls, entity):
        if 'logo' in entity:
            return entity['logo']
        return ''

    @classmethod
    def get_intro(cls, entity):
        if 'intro' in entity:
            return entity['intro']
        return ''

    @classmethod
    def get_name_key(cls, entity):
        if 'name_key' in entity:
            return entity['name_key']
        return []

    @classmethod
    def convert_e2g(cls, args, entity):
        sid = str(uuid.uuid4()).replace("-", "")
        group = {
            'sid': sid,
            'tally': 1,
            'region': cls.get_region(entity),
            #'inputs': [cls.convert_entity_with_inputs(sid, entity)]
            'inputs': [cls.convert_e4g(sid, entity)]
        }
        #recommend_dict = recommend.get_recommend(group, args)
        #group.update(recommend_dict)
        return group
