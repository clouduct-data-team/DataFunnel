import re
from pprintpp import pprint as pp
import orm

def get_recommend_category_name(group, args):
    cat_name_list = []
    for entity in group['inputs']:
        for category_name in entity['category_name']:
            if category_name not in cat_name_list:
                cat_name_list.append(category_name)
    return cat_name_list

def get_recommend_category_id(group, args):
    cat_id_list = []
    for entity in group['inputs']:
        for category_id in entity['category_id']:
            if category_id not in cat_id_list:
                cat_id_list.append(category_id)
    return cat_id_list

def get_recommend(group, args):
    #args['source_ranking_map'] = orm.get_source_ranking_map()
    recommend_fields = {
        'recommend_name': name,
        'recommend_mainname': mainname,
        'recommend_head': head,
        'recommend_branch': branch,
        'recommend_catkey': '',
        'recommend_address': address,
        'recommend_lat': lat,
        'recommend_lng': lng,
        'recommend_category_name': get_recommend_category_name,
        'recommend_category_id': get_recommend_category_id,
        'region': region,
        'tally': tally
    }
    recommend_result = {}
    for field_name, field_value in recommend_fields.items():
        if callable(field_value):
            recommend_result[field_name] = field_value(group, args)
        else:
            recommend_result[field_name] = field_value
    return recommend_result


def lcs(a, b):
    """
    >>> lcs('thisisatest', 'testing123testing')
    'tsitest'
    """
    '''
    print('xstr', xstr, 'ystr', ystr)
    if not xstr or not ystr:
        return ""
    x, xs, y, ys = xstr[0], xstr[1:], ystr[0], ystr[1:]
    if x == y:
        return x + lcs(xs, ys)
    else:
        return max(lcs(xstr, ys), lcs(xs, ystr), key=len)
    '''
    lengths = [[0 for j in range(len(b)+1)] for i in range(len(a)+1)]
    # row 0 and column 0 are initialized to 0 already
    for i, x in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                lengths[i+1][j+1] = lengths[i][j] + 1
            else:
                lengths[i+1][j+1] = max(lengths[i+1][j], lengths[i][j+1])
    # read the substring out from the matrix
    result = ""
    x, y = len(a), len(b)
    while x != 0 and y != 0:
        if lengths[x][y] == lengths[x-1][y]:
            x -= 1
        elif lengths[x][y] == lengths[x][y-1]:
            y -= 1
        else:
            assert a[x-1] == b[y-1]
            result = a[x-1] + result
            x -= 1
            y -= 1
    return result

def mlcs(string_list):
    lcs_str = string_list[0]
    if len(string_list) < 2:
        #print('<<<<2', lcs_str)
        return lcs_str
    for i in range(1, len(string_list)):
        lcs_str = lcs(lcs_str, string_list[i])
    #print(">>>>2")
    return lcs_str


def name(group, args):
    """
    default return mlcs, if empty return longest name instead
    """
    name_list = [entity['name'] for entity in group['inputs']]
    #print("name_list", name_list)
    _name = mlcs(name_list)
    if _name == "":
        return max(name_list, key=len)
    #print(" !!!!!!!!!!!!!!")
    #pp(group)
    return _name


def get_hotel_hall(name):
    #if len(name) > 3 and u"大学" in name and name.indexOf(u"大学") > 1:
    return ''
    

def mainname(group, args):
    name_list = [entity['name'] for entity in group['inputs']]

    if len(group['inputs']) == 1:
        source = group['inputs'][0]['source_name']
        source_ranking = args['source_ranking_map'][source]
        if source_ranking >= 200 and source_ranking<=400:
            return source
    rmd_name = name(group, args)
    if rmd_name == '':
        return ''
    rmd_head = escape_rmd_name(rmd_name)
    if len(rmd_head) != len(rmd_name):
        # means rmd_name is contain branch
        return rmd_head
    else:
        #if rmd_name not in mainname_list:
        #    # add rmd_name to mainname_list
        #    mainname_list[rmd_name] = True
        #return rmd_name
        # TO-DO: else part of code is porting from java.
        return rmd_head
    
def head(group, args):
    rmd_name = name(group, args)
    #print('rmd_name', rmd_name)
    return escape_rmd_name(rmd_name)

def contain_branch(rmd_name):
    if len(rmd_name) == len(escape_rmd_name(rmd_name)):
        return False
    return True

def escape_rmd_name(rmd_name):
    # remove branch within () （）
    token = [u"(","u（"]
    try:
        match = re.findall(u'\(|\（', rmd_name)
    except:
        #print(">", rmd_name, "<")
        raise
    if match != []:
        return rmd_name[:rmd_name.index(match[0])]
    return rmd_name


def branch(group, args):
    return ''

def catkey(group, args):
    return ''

def address(group, args):
    return max([entity['address']['street'] for entity in group['inputs']], key=len)

def lat(group, args):
    return latlng(group, args)[0]

def lng(group, args):
    return latlng(group, args)[1]

def latlng(group, args):
    def latlng_sum(latlng_tuple):
        return latlng_tuple[0] + latlng_tuple[1]
    return max([(entity['lat'], entity['lng']) for entity in group['inputs']], key=latlng_sum)
        


def category_name(group):
    return ''



def region(group, args):
    _region = {}
    fields = ['district_name', 'district_id', 'city_name', 'city_id',
     'province_name', 'province_id', 'country_name', 'country_code']
    for field in fields:
        field_vote_map = {}
        for _entity in group['inputs']:
            entity_field_value = _entity['region'][field]
            if entity_field_value == '':
                continue
            if entity_field_value not in field_vote_map:
                field_vote_map[entity_field_value] = 0
            field_vote_map[entity_field_value] += 1

        max_vote, max_field_value = 0, ''
        for field_value, vote in field_vote_map.items():
            if vote > max_vote:
                max_field_value = field_value
                max_vote = vote
        _region[field] = max_field_value
    return _region


def tally(group, args):
    return len(group['inputs'])


def get_branch_company_name(name):
    # in: XXX公司OO分公司
    # in: XXX公司(OO分公司)
    # out: OO
    if name.index(u'分公司') > name.index(u"公司"):
        name = name.replace("(", "").replace(")", "")
        return name[name.index(u"公司")+2: name.index(u"分公司")]
    return ''

