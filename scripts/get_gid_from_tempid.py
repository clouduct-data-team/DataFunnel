# -*- coding: utf-8 -*-

"""Module description"""

__author__ = 'chandler'

import json
import argparse
import json
from pprintpp import pprint as pp
import time
import uuid
import os
import logging
logger = logging.getLogger('merge.screne_merge')
from collections import defaultdict
from multiprocessing import Process, Manager
from multiprocessing import Pool, cpu_count
manager = Manager()


def read_cluster_file(args, pid, tempid_map, num_consumers):
    tempid_gid_map = {}

    print('loading by %s' % pid)
    try:
        fpath = args['cluster']
    except KeyError as err:
        raise KeyError('args did not contain cluster')
    load_limit = 0
    if args['CLUSTER']['load_limit'].isdigit():
        load_limit = int(args['CLUSTER']['load_limit'])
    with open(fpath, 'r') as cluster_file:
        for idx, group_str in enumerate(cluster_file, start=0):
            if idx % num_consumers != pid:
                continue
            if load_limit != 0 and idx >= load_limit:
                break
            try:
                group = json.loads(group_str)
            except:
                if '***SUCCESS***' in group_str:
                    continue
            if len(idx) % 1024 == 0:
                print('loading %s by process[%s]' % (idx, pid))

    return cluster, gid_feature_map, feature_index_map


def load_cluster(self, cluster, idx_mgr, args):

    num_consumers = cpu_count() * 2
    pool = Pool(processes=num_consumers)
    results = []
    d, gid_feature_map  = manager.dict(), manager.dict()
    for pid in range(0, num_consumers):
        result = pool.apply_async(read_cluster_file, args=(args, pid, idx_mgr, num_consumers))
        results.append(result)
    pool.close()
    pool.join()
    print('finish')
    time.sleep(3)
    for result in results:
        for gid, group in result.get()[0].items():
            cluster.map[gid] = group
            idx_mgr.gid_feature_map[gid] = result.get()[1][gid]
        for f_name in FeatureSet.map.keys():
            for feature, gid_list in result.get()[2].items():
                for _gid in gid_list:
                    idx_mgr.feature_index_map[f_name].add_doc(key=feature, value=_gid)


    print('finish again')
    return cluster, idx_mgr

def get_tempid_dict(args):
    tempids_map = {}
    with open(args['tempid'], 'r') as f:
        for line in f:
            tempids_map[line.strip()] = True
    return tempids_map

def get_tempid_gid_map(args, tempid_dict):
    tempid_gid_map = {}
    with open(args['cluster'], 'r') as f:
        for line in f:
            if '***SUCCESS***' in line:
                continue
            group = json.loads(line)
            for entity in group['inputs']:
                if entity['tempid'] in tempid_dict:
                    tempid_gid_map[entity['tempid']] = group['sid']
                    print(tempid_gid_map[entity['tempid']], group['sid'])
    return tempid_gid_map

def dump_tempid_gid(args, tempid_gid_map):
    with open(args['output'], 'w') as f:
        for tempid, gid in tempid_gid_map.items():
            f.write("%s\t%s\n"%(tempid, gid))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--cluster', action="store", dest="cluster", required=True)
    parser.add_argument('--tempid', action="store", dest="tempid", required=True)
    parser.add_argument('--output', action="store", dest="output", required=False)

    args = vars(parser.parse_args())
    tempids_dict = get_tempid_dict(args)
    tempid_gid_map = get_tempid_gid_map(args, tempids_dict)
    dump_tempid_gid(args, tempid_gid_map)

