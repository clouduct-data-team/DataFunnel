# -*- coding: utf-8 -*-
import redis
import argparse 
import json
from pprintpp import pprint as pp
import jieba
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer,CountVectorizer
import nltk
from nltk.corpus import stopwords


if __name__ == '__main__':
    """
    USAGE:
        python idf_generator.py --cluster_in=/home/ubuntu/DataFunnel/sample_data/cluster/2.txt
        --idf_path=/tmp/address.txt --field=recommend_address
    """
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--cluster_in', action="store", dest="cluster_in", required=True)
    parser.add_argument('--idf_path', action="store", dest="idf_path", required=True)
    parser.add_argument('--field', action="store", dest="field", required=True)
    parser.add_argument('--start', action="store", dest="start", required=False, type=int)
    parser.add_argument('--end', action="store", dest="end", required=False, type=int)
    parser.add_argument('--load_limit', action="store", dest="load_limit", required=False, type=int)
    args = vars(parser.parse_args())
    pp(args)
    fpath = args['cluster_in']
    load_limit = 0
    if args['load_limit'] is not None:
        load_limit = args['load_limit']
    start = 0
    if 'start' in args and args['start']:
        start = int(args['start'])
    #cluster_in = Cluster(group_class=Group)
    corpus = []
    with open(fpath, 'r') as cluster_file:
        for idx, group_str in enumerate(cluster_file, start=0):
            if '********SUCCESS********' in  group_str:
                continue
            if start > idx:
                continue
            if load_limit != 0 and idx >= load_limit:
                break
            obj = json.loads(group_str)

            if args['field'] not in obj or args['field'] == "":
                continue
            field = obj[args['field']]
            seg_list = jieba.cut(field)
            doc_sting = ' '.join(seg_list) + '\n'
            corpus.append(doc_sting)

    stop_words = stopwords.words('english')
    cvec = CountVectorizer(stop_words=stop_words) 
    counts = cvec.fit_transform(corpus)
    #pp(cvec.vocabulary_)
    idx = cvec.vocabulary_[u'分公司']
    #pp(counts.todense())
    tfidfTransformer = TfidfTransformer()
    idf = tfidfTransformer.fit(counts)
    #pp(idf.idf_)
    #pp(idf.idf_[idx])
    #pp(len(cvec.vocabulary_))
    idf_path = args['idf_path']
    with open(idf_path, 'w+') as f_idf:
        for term, idx in cvec.vocabulary_.items():
            counts = idf.idf_[idx]
            out_str = u"%(term)s %(counts)s\n" % {'term': term, 'counts': counts}
            f_idf.write(out_str)
