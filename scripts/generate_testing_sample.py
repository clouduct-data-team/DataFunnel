# -*- coding: utf-8 -*-
'''Module Description

INPUT

'''
import sys
sys.path.append("../")
import argparse
from settings import GROUP_TEMPID_MAP, TEMPID
from pprintpp import pprint as pp
import re
import json

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='')
    PARSER.add_argument('--input', action="store", help="Input file contains \
                    gids, one gid per line.", dest='input', required=True)
    PARSER.add_argument('--output', action="store", help="Output file contains \
                        labeled class(#new-gid), tempid and helps info \
                        separated by comma.", dest='output', required=True)
    ARGS = PARSER.parse_args()
    gid_tempid_map = {}
    with open(ARGS.input, 'r') as f:
        for line in f:
            if '#' == line[0]:
                continue
            gid = line.split(u'#')[0].strip()
            tempids = list(GROUP_TEMPID_MAP.smembers(gid))
            group_biggest_number = 0
            for tempid in tempids:
                tempid = tempid.decode('utf-8')
                number_str = ''.join(re.findall('\d+', tempid.strip()))
                number_int = int(number_str)
                if number_int > group_biggest_number:
                    group_biggest_number = number_int
            with open(ARGS.output, 'a+') as fout:
                for tempid in tempids:
                    tempid = tempid.decode('utf-8').strip()
                    tempid_obj = json.loads(TEMPID.get(tempid).decode('utf-8'))
                    pp(tempid_obj)
                    number = ''
                    add_obj = tempid_obj['address']
                    for tel in tempid_obj['tel']:
                        number += "%s " % tel['number']
                    address = "%s%s%s" % (add_obj['city'], add_obj['district'], add_obj['street'])
                    fout.write('%s,%s#%s,%s,%s\n' %(group_biggest_number, tempid,
                               tempid_obj['name'], number, address))
