# -*- coding: utf-8 -*-
'''Module Description

'''
import sys
sys.path.append("../")
import argparse
from settings import GROUP_TEMPID_MAP, TEMPID
from pprintpp import pprint as pp
import re
import json
import requests 
import operator
from sklearn.metrics import f1_score, classification_report
from sklearn.metrics import jaccard_similarity_score
from sklearn.metrics import adjusted_rand_score
import time


def get_ari_table(truth_list, predict_list):
    '''
    inputs: 
        two list of truth, predict lables.
    outputs: 
        ari_table
        {
           'TP': [ (idx_1, idx_2), ...],
           'FP': [ (idx_3, idx_6), ...],
           'FN': [(...), ... ],
           'TN': [(...), ...]
        }
    '''
    result = {
        'TP': [], 'FP': [], 'FN': [], 'TN': []
    }

    for idx1, _ in enumerate(truth_list):
        for idx2, _ in enumerate(truth_list[idx1+1:], idx1+1):
            truth1, truth2 = truth_list[idx1], truth_list[idx2]
            predict1, predict2 = predict_list[idx1], predict_list[idx2]
            if truth1 == truth2:
                if predict1 == predict2:
                    result['TP'].append( (idx1, idx2) )
                if predict1 != predict2:
                    result['FN'].append( (idx1, idx2) )
            elif truth1 != truth2:
                if predict1 == predict2:
                    result['FP'].append( (idx1, idx2) )
                if predict1 != predict2:
                    result['TN'].append( (idx1, idx2) )
    return result


def get_ari_index(ari_table):
    right_counts = len(ari_table['TP']) + len(ari_table['TN'])
    total_counts = len(ari_table['FP']) + len(ari_table['FN']) + right_counts
    return float(right_counts) / float(total_counts)


if __name__ == "__main__":
    with open('result.txt', 'r') as f:
        obj = json.loads(f.read())
    tempids = obj['tempids']
    inputs = obj['truth']
    outputs = obj['predict']

    #inputs = [1,1,1,2,1,2]
    #outpus = [1,2,3,4,3,3]

    ari_table = get_ari_table(inputs, outputs)
    ari_index = get_ari_index(ari_table)
    pp(inputs)

    pp(outputs)
    #pp(ari_table)
    print(len(ari_table['TP']), len(ari_table['FP']), len(ari_table['FN']), len(ari_table['TN']))
    pp(ari_index)
    print(len(tempids), len(inputs), len(outputs))
    time.sleep(10)

    tempids = obj['tempids']
    for fn_idx_tuple in ari_table['TN']:
        idx1, idx2 = fn_idx_tuple[0], fn_idx_tuple[1]
        print(idx1, idx2, tempids[idx1], tempids[idx2])
        payload = {
            'type': 'eid',
            'id_1': tempids[idx1], 
            'id_2': tempids[idx2]
        }
        similarity_resp = requests.post('http://localhost:5000/similarity', 
                                   data=payload)
        similarity =  json.loads(similarity_resp.text)
        #pp(similarity)
        distance_resp = requests.post("http://localhost:5000/distance", 
                                 data=similarity)
        distance = json.loads(distance_resp.text)
        pp(distance)
        #distance_matrix, distance_matrix_hit = distance['distance_matrix'], distance['distance_matrix_hit'
        time.sleep(1)
        '''
        pp(distance.text)
        screne_feature = requests.post("http://localhost:5000/feature",
                                data=distance)
        pp(screne_feature.text)
        '''
