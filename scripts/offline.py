# -*- coding: utf-8 -*-
'''
Module offline
'''

import sys
sys.path.append("../")
import argparse
#from settings import GROUP_TEMPID_MAP, TEMPID
from pprintpp import pprint as pp
import re
import json
import os


def get_merged_file_path(directory):
    merged_file_list = []
    city_list = os.listdir(directory)
    for city in city_list:
        fpath = "%s/%s/%s.txt" % (directory, city, city)
        merged_file_list.append(fpath)
    return merged_file_list

def offline_source(cluster_list, output_path, field_name, field_value):
    for input_idx, cluster_file in enumerate(cluster_list):
        city_id = cluster_file.split('/')[-1].replace('.txt', '')
        dir_path = '%s/%s' % (output_path, city_id)
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        fout_path = "%s/%s/%s.txt" % (output_path, city_id, city_id)
        print('input:%s' % cluster_file)
        print('output:%s' % fout_path)
        # /home/filesystem/dataflow/merge_output/L20161111/out/cluster/China/lifestyle/50/50.txt
        with open(cluster_file, 'r') as f, open(fout_path, 'w') as f_out:
            for line_idx, line in enumerate(f):
                if line_idx % 1024 == 0:
                    out_str = "[%s/%s] [%s]" % (input_idx, len(cluster_list), line_idx)
                skip_dump = False
                if '*************************' in line:
                    continue
                group = json.loads(line)
                inputs = group['inputs']
                _inputs = []
                for idx, tempid in enumerate(inputs):
                    if tempid[field_name] == field_value or (field_name=='version' and
                                                             int(tempid[field_name]) <
                                                             int(field_value)) :
                        if len(inputs) == 1:
                            skip_dump = True
                        else:
                            #pp(tempid[field_name)
                            del inputs[idx]
                    else:
                        _inputs.append(tempid)
                group['inputs'] = _inputs
                group["tally"] = len(_inputs)
                if not skip_dump:
                    str_to_dump = json.dumps(group, ensure_ascii=False) + "\n"
                    f_out.write(str_to_dump)
            f_out.write('*************************SUCCESS*************************')


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='')
    PARSER.add_argument('--input_cluster', action="store", help="Input cluster contains \
                        gids, one gid per line.", dest='input', required=True)
    PARSER.add_argument('--output_cluster', action="store", help="Output cluster contains \
                        labeled class(#new-gid), tempid and helps info \
                        separated by comma.", dest='output', required=True)
    PARSER.add_argument('--field_name', action="store", help="gids, one gid per line.", dest='field_name', required=True)
    PARSER.add_argument('--field_value', action="store", help="one gid per line.", dest='field_value', required=True)
    ARGS = PARSER.parse_args()
    file_list = get_merged_file_path(ARGS.input)
    offline_source(file_list, ARGS.output, ARGS.field_name, ARGS.field_value)
