# -*- coding: utf-8 -*-
'''Module Description

'''
import sys
sys.path.append("../")
import argparse
from settings import GROUP_TEMPID_MAP, TEMPID
from pprintpp import pprint as pp
import re
import json
import requests 
import operator
from sklearn.metrics import f1_score, classification_report
from sklearn.metrics import jaccard_similarity_score
from sklearn.metrics import adjusted_rand_score


def load_truth_data(ARGS):
    tempid_gid_map = {}
    gid_tempids_map = {}
    if type(ARGS) is dict:
        inputs = ARGS['input']
    else:
        inputs = ARGS.input
    with open(inputs, 'r') as f:
        for line in f:
            if '#' == line[0]:
                continue
            gid, tempid = line.split(u'#')[0].strip().split(',')
            tempid_gid_map[tempid] = gid
            if gid not in gid_tempids_map:
                gid_tempids_map[gid] = []
            if tempid not in gid_tempids_map[gid]:
                gid_tempids_map[gid].append(tempid)
    return tempid_gid_map, gid_tempids_map

def get_prediction(keys):
    payload = {'tempids': json.dumps(list(keys))}
    result = requests.post('http://localhost:5000/merge_tempids', data=payload)
    #pp(result.text)
    return json.loads(result.text)

'''
def get_sorted_gids_by_tempid_counts(tempid_gid_map):
    sorted_gid_order_list = []
    gid_tempids_map = {}
    for tempid, gid in tempid_gid_map.items():
        if gid not in gid_tempids_map:
            gid_tempids_map[gid] = []
        gid_tempids_map[gid].append(tempid)
    # pp(gid_tempids_map)
    sorted_gid_order_list = [k for k in sorted(gid_tempids_map, key=lambda k: len(gid_tempids_map[k]), reverse=True)]
    return sorted_gid_order_list, gid_tempids_map
'''

def get_shifted_label(tempid_gid_map, skip_shifted_gid=None):
    '''
    input: tempid_gid_map (Dict)
    output: tempid_shift_gid_map
    shift_gid is chose from tempid with small int(hash) within a group
    '''
    gid_tempids_map = {}
    for tempid, gid in tempid_gid_map.items():
        if gid not in gid_tempids_map:
            gid_tempids_map[gid] = []
        if tempid not in gid_tempids_map[gid]:
            gid_tempids_map[gid].append(tempid)

    tempid_shifted_gid_map = {}
    for gid, tempids in gid_tempids_map.items():
        min_hash = None
        for tempid in tempids:
            shifted_tempid = int(''.join([s for s in tempid if s.isdigit()]))
            if min_hash is None:
                min_hash = shifted_tempid
            if shifted_tempid < min_hash:
                min_hash = shifted_tempid
        for tempid in tempids:
            tempid_shifted_gid_map[tempid] = min_hash
    return tempid_shifted_gid_map

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='')
    PARSER.add_argument('--input', action="store", help="Input file contains \
                    gids, tempid for each line.", dest='input', required=True)
    PARSER.add_argument('--output', action="store", help="Output file contains \
                        labeled class(#new-gid), tempid and helps info \
                        separated by comma.", dest='output', required=True)
    ARGS = PARSER.parse_args()
    tempid_gid_map, gid_tempids_map = load_truth_data(ARGS)
    
    max_length_gid, max_length = '', 0

    for gid, tempids in gid_tempids_map.items():
        if len(tempids) > max_length:
            max_length_gid = gid
    d = gid_tempids_map

    sorted_gid_by_length = [k for k in sorted(d, key=lambda k: len(d[k]), reverse=True)]
    max_group = 10
    tempid_list = []
    truth = {}
    for gid in sorted_gid_by_length[:max_group]:
        tempids = gid_tempids_map[gid]
        testing_sample = {tempid: tempid_gid_map[tempid] for tempid in tempids}
        shifted_truth = get_shifted_label(testing_sample)
        truth.update(shifted_truth)
        tempid_list.extend(tempids)

    predict = {}
    predict_tempid_gid_map = get_prediction(tempid_list)

    predict_gid_tempids_map = {}
    for tempid, gid in predict_tempid_gid_map.items():
        if gid not in predict_gid_tempids_map:
            predict_gid_tempids_map[gid] = []
        if tempid not in predict_gid_tempids_map[gid]:
            predict_gid_tempids_map[gid].append(tempid)

    for gid, tempids in predict_gid_tempids_map.items():
        predict_sample = {tempid: predict_tempid_gid_map[tempid] for tempid in tempids}

        shifted_predict = get_shifted_label(predict_sample)
        predict.update(shifted_predict)

    keys = list(truth.keys())
    truth_list = [str(truth[key]) for key in keys]
    predict_list = [str(predict[key]) for key in keys]

    labels = list(set(truth_list))
    print(classification_report(truth_list, predict_list, labels=labels))
    # ref:
        # http://stackoverflow.com/questions/37615544/f1-score-per-class-for-multi-class-classification
    print('len(tempid_gid_map): %s ' % len(tempid_gid_map))
    print('len(truth): %s , len(truth_list): %s, len(set(truth_list)): %s' % (len(truth), len(truth_list), len(set(truth_list))))
    print('len(predict): %s, len(predict_list): %s, len(set(predict_list)): %s' % (
        len(predict), len(predict_list), len(set(predict_list))))
    print(adjusted_rand_score(truth_list, predict_list))
    

    obj_to_dump = {
        'tempids' : keys,
        'truth' : truth_list,
        'predict': predict_list
    }
    with open('result.txt', 'w') as f:
        f.write(json.dumps(obj_to_dump, ensure_ascii=False))
