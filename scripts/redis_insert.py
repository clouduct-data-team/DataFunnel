# -*- coding: utf-8 -*-
import redis
import argparse 
import json
from pprintpp import pprint as pp
redis_host = "localhost"
redis_port = 6479
redis_db = 0
POOL = redis.ConnectionPool(host=redis_host,port=redis_port,db=redis_db)
POOL2 = redis.ConnectionPool(host=redis_host,port=redis_port,db=1)
r_tempid = redis.StrictRedis(connection_pool=POOL, charset="utf-8", decode_responses=True)
r_group = redis.StrictRedis(connection_pool=POOL2, charset="utf-8", decode_responses=True)
#print(r.smembers(u'朝阳区'))
#r_tempid.flushall()
#r_group.flushall()
import time
#time.sleep(10)
#print('fuck off')
"""
print(r.dbsize())
r.set('foo', 'bar')
print(r.get('foo'))
r.set('foo', 'fuck')
r.set('foo', ['fuck', 'you'])
print(r.get('foo'))
print(r.dbsize())
print(r.keys())
c = u'c'
ker = [u'我', u'愛', u'測', c]
for _k in ker:
    r.sadd('ker', _k)
r.sadd('ker', _k)
r.sadd('kkk2', u'我')
r.sadd('kkk2', u'測')
print(r.sismember('ker', c))
print(r.smembers('ker'))
print(c in r.smembers('ker'))
print(r.smembers('ker'))
print(type(r.smembers('ker')))
for _k in r.smembers('ker'):
    x = _k.decode('utf-8')
    if x == u'我':
        print('bi')
    print(x)
test = [1, 2, 3]
r.rpush('kkk', *test)
r.mget('kkk')
print(r.mget('kkk'))
intr = r.sinter(keys=['ker','kkk2'])
for item in intr:
    print(item.decode('utf-8'))
print("*"*10)
print(r.sunion(keys=['ker', 'kkk2']))
"""

from redisindex import RedisIndex as Index
from indexmanager import IndexManager
from featureset import FeatureSet
from rediscluster import RedisCluster as Cluster
from shopgroup import ShopGroup as Group
from tempid import Tempid as Entity
Group.Entity = Entity

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--cluster', action="store", dest="cluster", required=True)
    parser.add_argument('--start', action="store", dest="start", required=False)
    parser.add_argument('--end', action="store", dest="end", required=False)
    args = vars(parser.parse_args())

    feature_set = FeatureSet()
    index_manager = IndexManager(feature_set=feature_set, index=Index)

    fpath = args['cluster']
    if 'load_limit' in args:
        load_limit = args['load_limit']
    else:
        load_limit = 0
    start = 0
    if 'start' in args and args['start']:
        start = int(args['start'])
    #cluster = Cluster(group_class=Group)
    with open(fpath, 'r') as cluster_file:
        for idx, group_str in enumerate(cluster_file, start=0):
            if '********SUCCESS********' in  group_str:
                continue
            if start > idx:
                continue
            if load_limit != 0 and idx >= load_limit:
                break
            obj = json.loads(group_str)
            tempid_list = []
            for tempid in obj['inputs']:
                del tempid['extend']
                r_tempid.set(tempid['tempid'], json.dumps(tempid, ensure_ascii=False))
                #pp(tempid)
                tempid_list.append(tempid['tempid'])
                #time.sleep(0.007)

            #r_group.delete(obj['sid'])
            #time.sleep(0.02)
            r_group.sadd(obj['sid'], *set(tempid_list))
            """
            shopgroup = Group(json_str=group_str)
            cluster.add_group(obj=shopgroup)
            #index_manager.add_group(shopgroup)
            """
            if idx % 1000 == 0:
                print(idx)
            if idx > 0 and idx % 500000 == 0:
                print(idx)
                r_tempid.save()
                #r_group.save()

        #r_tempid.save()
        r_group.save()
