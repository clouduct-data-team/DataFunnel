from ABC.cluster import Cluster
import json

__author__ = 'chandler'

class FileCluster(Cluster):

    def __init__(self, *args, **kwargs):
        """
        from shop import Shop as Group
        Group.Entity = Tempid
        usage: fc = FileCluster(group_class=Group, fpath='sample_data/2.txt', load_limit=10)
        """
        if 'group_class' not in kwargs:
            raise

        self.Group_Class = kwargs['group_class']
        self.map = {}

        if 'fpath' in kwargs:
            if 'load_limit' in kwargs:
                load_limit  = kwargs['load_limit']
            else:
                load_limit = 0
            fpath = kwargs['fpath']
            with open(fpath, 'r') as cluster_file:
                for idx, group_str in enumerate(cluster_file, start=1):
                    if load_limit != 0 and idx > load_limit:
                        break
                    #group = self.Group_Class(json_str=group_str)
                    #self.map[group.id] = group 
                    self.add_group(json_str=group_str)

    def add_group(self, **kwargs):
        # TO-DO move add_group() to ABC/cluster
        if 'json_str' not in kwargs and 'obj' not in kwargs:
            raise
        if 'json_str' in kwargs:
            group = self.Group_Class(json_str=kwargs['json_str'])
        else: # obj in kwargs
            group = kwargs['obj']
        self.set(gid=group.id,  group=group)
    

if __name__ == '__main__':
    from tempid import Tempid
    from shopgroup import ShopGroup as Group
    Group.Entity = Tempid
    fc = FileCluster(group_class=Group, fpath='sample_data/2.txt', load_limit=10)
    from pprintpp import pprint as pp 
    for group in fc:
        pp(group)
    fc.del_group(group_id="6f79a3403ab744c295732246646b5a0c")
    pp(fc.map["6f79a3403ab744c295732246646b5a0c"])
    #print(fc)
    #fc.load()
    #pp(fc)
    """
    for idx, group in enumerate(fc):
        pp(group)
    """
