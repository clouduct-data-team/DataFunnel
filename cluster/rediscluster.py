# -*- coding: utf-8 -*-
from ABC.cluster import Cluster
import json
import redis 
from pprintpp import pprint as pp
from utils.logger import get_logger
import time
logger = get_logger('RedisCluster')

class RedisCluster(Cluster):

    def __init__(self, args):
        # ex: title: 0 , tel: 1, etc: ...
        logger.debug("\t [CLUSTER] [REDIS] [INIT: %s] " % args['cluster_host'])
        redis_host, redis_port = args['cluster_host'].split(':')
        POOL = redis.ConnectionPool(host=redis_host, port=int(redis_port),
                                    db=int(args['cluster_db']), max_connections=16)
        self.redis = redis.StrictRedis(connection_pool=POOL, charset="utf-8", decode_responses=True)
        self.args = args
        self.POOL = POOL
        self.map = {}


    def set(self, gid, group_str, group_obj=None):
        logger.debug("\t[CLUSTER]\t[SET] \t[%s]"% gid)
        #group_dump_str = group.to_redis()
        if group_obj is not None:
            group_str = json.dumps(group_obj, ensure_ascii=False)
        res = self.redis.set(gid, group_str)
        if not res:
            logger.error("\t[CLUSTER]\t[SET] \t[%s] False\n\t%s "% (gid, group_str))
        #self.map[group.id] = group

    def get(self, gid):
        try:
            group_dump_str = self.redis.get(gid)
        except Exception as e:
            logger.error("=========================================================")
            logger.error("\tRedisCluster Error:{error}, reset connection".format(error=str(e)))
            time.sleep(3)
            self.redis = redis.StrictRedis(connection_pool=self.POOL, charset="utf-8",
                                           decode_responses=True)
            return self.get(gid)
        if group_dump_str == None:
            return None
        group_str = group_dump_str.decode('utf-8')
        return group_str

    def add_group(self, gid, group_obj):
        group_str = json.dumps(group_obj, ensure_ascii=False)
        logger.debug("\t[CLUSTER]\t[ADD GROUP] \t[%s]"% (gid))
        res = self.redis.set(gid, group_str)
        if not res:
            logger.error("\t[CLUSTER]\t[ADD GROUP] \t[%s] \n%s False\n\t%s "% (gid, str(res), group_str))

    def del_group(self, gid):
        logger.debug("\t[CLUSTER]\t[DEL GROUP] \t[%s]"% gid)
        if self.redis.get(gid) is not None:
            res = self.redis.delete(gid)
            del self.map[gid]
            if not res:
                logger.error("\t[CLUSTER]\t[DEL GROUP] \t[%s] False\n\t%s "% (gid))

    def __len__(self):
        return self.redis.dbsize()
    
    def __reduce__(self):
        return (self.__class__, (self.args, ))

if __name__ == '__main__':
    __author__ = 'chandler'
    redis_host = "localhost"
    redis_port = 6579
    redis_db = 0
    from tempid import Tempid
    from shopgroup import ShopGroup as Group
    Group.Entity = Tempid
    fc = FileCluster(group_class=Group, fpath='sample_data/2.txt', load_limit=10)
    from pprintpp import pprint as pp 
    for group in fc:
        pp(group)
    fc.del_group(group_id="6f79a3403ab744c295732246646b5a0c")
    pp(fc.map["6f79a3403ab744c295732246646b5a0c"])
    #print(fc)
    #fc.load()
    #pp(fc)
    """
    for idx, group in enumerate(fc):
        pp(group)
    """
