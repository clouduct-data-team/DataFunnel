# -*- coding: utf-8 -*-
import os
import json
from pprintpp import pprint as pp
#from group import Group
#from entity import Entity
from merge.loader import MergeLoader
import time
import re
from feature import Feature
from pathlib import Path


from utils.logger import get_logger
logger = get_logger('Export')


myre = re.compile(u'['
                  u'\U0001F300-\U0001F64F'
                  u'\U0001F680-\U0001F6FF'
                  u'\u2600-\u26FF\u2700-\u27BF]+', 
          re.UNICODE)

def get_diff_type_and_group(cluster_diff, gid):
    for diff_type, cluster in cluster_diff.items():
        if diff_type == 'new':
            continue
        # new, modified, deleted. It shouldn't be exist new
        # cause new group id would exit in gid of orgi
        if gid in cluster:
            return diff_type, cluster[gid]
    return None, None

'''
def get_group_dump_str(group_line_str, group, input_eid_str_map):
    # group_line_str: string load from cluster 
    # group: return from get_diff_type_and_group
    # input_eid_str_map: eid -> str
    dump_group = {}
    dump_group[Group.MAPPING['list']] = []
    new_eieds = group.map.keys()
    if group_line_str is None:
        # new group
        old_eids = []
    else:
        old_group_obj = json.loads(group_line_str)
        old_eids = [obj[Entity.FEATURE_MAPPING[Group.MAPPING['eid']]] for obj in
                    old_group_obj[Group.MAPPING['list']]]
        dump_group = old_group_obj

    diff_eids = list(set(new_eieds) - set(old_eids))

    for eid in diff_eids:
        if eid in old_eids:
        #  eid is in offline
            for idx, obj in enumerate(old_group_obj[Group.MAPPING['list']]):
                if eid == obj[Entity.FEATURE_MAPPING[Group.MAPPING['eid']]]:
                    # hit , idx is the entity own that eid
                    break
            # remove entity from dump_group
            del dump_group[Group.MAPPING['list']][idx]
        else:
            dump_group[Group.MAPPING['list']].append(json.loads(input_eid_str_map[eid].strip()))
    
    recommend_dict = Group.get_recommend(dump_group)
    dump_group.update(recommend_dict)
    dump_group[Group.MAPPING['id']] = group.id

    #dump_str = json.dumps(dump_group, ensure_ascii=False).replace('\\"',"\"")
    dump_str = json.dumps(dump_group, ensure_ascii=False)
    obj = json.loads(dump_str)
    return dump_str + "\n"
'''

def get_group_dump_str(args, group_obj, recommend):
    #group_obj = Feature.get_recommand_group(args, group_obj)
    #group_str = json.dumps(group_obj, ensure_ascii=False)
    recommend_group = recommend.get_recommend(group_obj)
    group_obj.update(recommend_group)
    group_str = Feature.dump_group(group_obj)
    return group_str


def export(args, deleted_groups, modified_groups, new_groups, recommend):
    if args['dump_enable'] == "no":
        return True
    if args['cluster_load_limit'].isdigit():
        load_limit = int(args['cluster_load_limit'])
    else:
        load_limit = -1

    if not Path(args['cluster_path']).is_file():
        import uuid
        cluster_path = str(uuid.UUID('{00010203-0405-0607-0809-0a0b0c0d0e0f}'))
    else:
        cluster_path = args['cluster_path']

    with open(cluster_path, 'r', encoding='utf-8') as f:
        total_groups = sum(1 for _ in f)


    with open(cluster_path, 'r', encoding='utf-8') as cluster_file,\
         open(args['dump_full_path'], 'w', encoding='utf-8') as full_file, \
         open(args['dump_changed_path'], 'w', encoding='utf-8') as changed_file, \
         open(args['dump_deleted_path'], 'w', encoding='utf-8') as deleted_file:

        for idx, group_str in enumerate(cluster_file, start=0):
            if load_limit >0 and idx > load_limit:
                break
            if idx % 2048 == 0:
                logger.debug("[FULL {idx}/{total}] ".format(idx=idx, total=total_groups))
            if group_str.startswith("*"):
                break
            group_str = group_str.strip()
            #gid = group_str[:50].split('","')[0].replace('{"sid":"', '').replace('\"','').replace(',','')
            # gid = group_str[:50].split('", "')[0].replace('{"sid": "', '').replace('\"','').replace(',','')
            gid = group_str[:50].split(',')[0].split('":')[-1].replace('"', '')
            if len(gid) not in [32, 40]:
                logger.error('wrong gid\t' + gid + "\t" + group_str[:50])

            if gid in deleted_groups or gid in modified_groups:
                continue

            group_str = myre.sub('', group_str)
            full_file.write(group_str+"\n")

        for modified_idx, (gid, group_obj) in enumerate(modified_groups.items(), start=1):
            group_str = get_group_dump_str(args, group_obj, recommend)
            logger.debug("[Modify {idx}/{total}] [sid: {sid}]".format(idx=modified_idx,
                                                                      total=len(modified_groups),
                                                                  sid=gid))

            changed_file.write("{group_str}\n".format(group_str=group_str))
            full_file.write(group_str+"\n")

        for new_idx, (gid, group_obj) in enumerate(new_groups.items(), start=1):
            group_str = get_group_dump_str(args, group_obj, recommend)
            logger.debug("[New {idx}/{total}] [sid: {sid}]".format(idx=new_idx,
                                                                   total=len(new_groups),
                                                                   sid=gid))

            changed_file.write("{group_str}\n".format(group_str=group_str))
            full_file.write(group_str+"\n")

        for gid in deleted_groups.keys():
            deleted_file.write("{gid}\n".format(gid=gid))

        full_file.write(args['dump_footer']+"\n")
        changed_file.write(args['dump_footer']+"\n")
        deleted_file.write(args['dump_footer']+"\n")

    logger.info("[DUMP] [Finished]")


