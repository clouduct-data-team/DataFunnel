# -*- coding: utf-8 -*-
import uuid
import os
import sys
import pytest
from pprintpp import pprint as pp
import subprocess
import json


settings = {
    'INPUT': 'tests/__TYPE__/input.txt',
    'CLUSTER': 'tests/__TYPE__/cluster.txt',
    'OFFLINE': 'tests/__TYPE__/offline.txt',
    'FULL': 'tests/__TYPE__/output/cluster_result.txt',
    'CHANGED': 'tests/__TYPE__/output/change_result.txt',
    'DELETE': 'tests/__TYPE__/output/delete_result.txt',
    'GT_FULL': 'tests/__TYPE__/ans/cluster_result.txt',
    'GT_CHANGED': 'tests/__TYPE__/ans/change_result.txt',
    'GT_DELETE': 'tests/__TYPE__/ans/delete_result.txt'
}
#@pytest.fixture
def load_cluster(fpath):
    cluster = {}
    with open(fpath, 'r', encoding='utf-8') as f:
        for line in f:
            if 'SUCCESS' in line:
                continue
            #print(fpath,">",line,"<")
            group = json.loads(line)

            cluster[group['sid']] = group
            cluster[group['sid']]['inputs'] = {entity['tempid']:entity for entity in group['inputs']}
    return cluster

def apply_action(action_type):
    clone_settings = dict(settings)
    with open('tests/execute.sh.mock', 'r') as f:
        mock = f.read()
        for key, value in clone_settings.items():
            replace_target = '__%s__'%key
            value = value.replace('__TYPE__', action_type)
            mock = mock.replace(replace_target, value)
            clone_settings[key] = value
        mock = mock.replace('__EXEC__', sys.executable)
        mock = mock.replace('__COUNTRY__', 'China')

    #if action_type == 'create':
    #    mock = mock.replace('__INDEX__', 'india_lifestyle')
    #else:
    mock = mock.replace('__INDEX__', 'test_l20170913_115')

    with open('tests/execute.sh', 'w', encoding='utf-8') as f:
        f.write(mock)
    subprocess.call(['sh', 'tests/execute.sh'])
    return clone_settings

def compare_cluster_result(settings):
    GT_FULL = load_cluster(settings['GT_FULL'])
    FULL = load_cluster(settings['FULL'])
    if GT_FULL.keys() != FULL.keys():
        for sid in set(GT_FULL.keys()) ^ set(FULL.keys()):
            if sid in GT_FULL:
                print(GT_FULL[sid], 'GT contains cluster keys error', sid)
            if sid in FULL:
                print(FULL[sid], 'FULL contans cluster keys error', sid)
        return False

    gt_entity_map = {}
    for sid, group in GT_FULL.items():
        if group['inputs'].keys != FULL[sid]['inputs'].keys():
            for entity in set(group['inputs'].keys) ^ set(FULL[sid]['inputs'].keys()):
                print(sid, '> inputs > entity diff', entity['tempid'])
            return False
        for entity in group['inputs']:
            for key, value in entity.items():
                if value != FULL[sid]['inputs'][key]:
                    print(sid, '> inputs >', key, value, '<>', FULL[sid]['inputs'][key])
                    return False
    return True

def compare_offline_result(settings):
    return True



def test_create():
    action_settings = apply_action('create')

    GT_FULL = load_cluster(action_settings['GT_FULL'])
    FULL = load_cluster(action_settings['FULL'])
    #print(len(GT_FULL), len(FULL))
    
    gt_group = list(GT_FULL.values())[0]
    group = list(FULL.values())[0]
    
    for field, value in gt_group.items():
        print('>>>>>>>>>>>>>>>>>>', field)
        if field in ['sid', 'inputs']:
            continue
        gt_json_str = str(json.dumps(value, sort_keys=True))
        group_json_str = str(json.dumps(group[field], sort_keys=True))
        assert len(gt_json_str) == len(group_json_str)
        assert gt_json_str == group_json_str

def test_merge():
    action_settings = apply_action('merge')

    GT_FULL = load_cluster(action_settings['GT_FULL'])
    FULL = load_cluster(action_settings['FULL'])
    #print(len(GT_FULL), len(FULL))
    
    gt_group = list(GT_FULL.values())[0]
    group = list(FULL.values())[0]
    
    for field, value in gt_group.items():
        if field in ['sid', 'inputs']:
            continue
        gt_json_str = str(json.dumps(value, sort_keys=True))
        group_json_str = str(json.dumps(group[field], sort_keys=True))
        assert len(gt_json_str) == len(group_json_str)
        assert gt_json_str == group_json_str
def test_update():
    action_settings = apply_action('update')
    GT_FULL = load_cluster(action_settings['GT_FULL'])
    FULL = load_cluster(action_settings['FULL'])

    gt_group = list(GT_FULL.values())[0]
    group = list(FULL.values())[0]

    gt_json_str = str(json.dumps(gt_group, sort_keys=True))
    group_json_str = str(json.dumps(group, sort_keys=True))

    assert len(gt_json_str) == len(group_json_str)
    assert gt_json_str == group_json_str

def test_offline():
    action_settings = apply_action('offline')
    GT_FULL = load_cluster(action_settings['GT_FULL'])
    FULL = load_cluster(action_settings['FULL'])

    gt_group = list(GT_FULL.values())
    group = list(FULL.values())
    assert len(gt_group) == len(group) == 0
