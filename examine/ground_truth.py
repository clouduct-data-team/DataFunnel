import json
from feature import Feature
from utils.logger import get_logger
logger = get_logger('GroundTruth')
from pprintpp import pprint as pp
from sklearn.metrics import adjusted_rand_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
import logging
import time


def load_ground_truth(args, pid, num_consumers,  inputs):
    eid_gid_map = {}
    with open(args['ground_truth_path'], 'r') as f:
        for idx, group_str in enumerate(f, start=0):
            if idx % num_consumers != pid or group_str.startswith('*'):
                continue
            group_str = group_str.strip()
            group_obj = json.loads(group_str)
            gid = group_obj[Feature.group['gid']]
            for entity_obj in group_obj[Feature.group['list']]:
                if entity_obj[Feature.entity['eid']] in inputs:
                    eid_gid_map[entity_obj[Feature.entity['eid']]] = gid
    logger.debug("\t [PID: {pid}] [GROUND-TRUTH] [FINISH:{num}]".format(
                    pid=pid, num=len(eid_gid_map)))
    return eid_gid_map


def get_classification_report(y_true, y_pred, mode='detail', average=None):
    if mode != 'detail':

        precision = precision_score(y_true, y_pred, average=average)
        recall = recall_score(y_true, y_pred, average=average)
        f_score = f1_score(y_true, y_pred, average=average)
        accuracy = accuracy_score(y_true, y_pred)
        result = "\n\taccuracy_score:{a}\tprecision:{p}\trecall:{r}\tf1-score:{f}".format(
            a=accuracy, p=precision, r=recall, f = f_score)

    return classification_report(y_true, y_pred)


def get_truth_pred_lables(g_cluster, truth_eid_gid_map, pred_eid_gid_map):
    logger.debug("[GET TRUE_PRED_LABLES] [truth_eid_gid_map :{len1}] [pred_eid_gid_map :{len2} ]"\
                 .format(len1=len(truth_eid_gid_map), len2=len(pred_eid_gid_map)))

    #idx_eid_map = {}
    lables_true,  lables_pred = [], []
    logger.setLevel(logging.INFO)
    for idx, (eid, gid) in enumerate(truth_eid_gid_map.items(), start=0):
        #idx_eid_map[idx] = eid
        lables_true.append(gid)
        if eid not in pred_eid_gid_map:
            logger.error("\t[EXAMINE] [MISSING EID in PREDICT] [EID:{eid}".format(eid=eid))
            continue
        pred_gid = pred_eid_gid_map[eid]
        #ture_false_result, pred_gid = same_with_ground_truth(g_cluster, eid,
        #        pred_eid_gid_map[eid], truth_eid_gid_map, display=False)
        lables_pred.append(pred_gid)
    logger.setLevel(logging.DEBUG)
    return lables_true, lables_pred


def same_with_ground_truth(g_cluster, eid, gid, eid_gid_map, similar_gid_result_map=None, display=True):
    def judge_by_score_diff(similar_gid_result_map, pred_gid, truth_gid):
        if truth_gid not in similar_gid_result_map:
            logger.error("\t[JUDGE] truth_gid[{truth_gid}] not in similar_gid_result_map".format(truth_gid=truth_gid))
            time.sleep(5)
            return False
        if pred_gid not in similar_gid_result_map:
            logger.error("\t[JUDGE] pred_score[{pred_gid}] not in similar_gid_result_map".format(pred_gid=pred_gid))
            time.sleep(5)
            return False
        
        truth_score = similar_gid_result_map[truth_gid]['score']
        pred_score = similar_gid_result_map[pred_gid]['score']
        score_diff = pred_score - truth_score 
        if pred_score > 100:
            diff_range = 35 
        elif pred_score > 80:
            diff_range = 25
        elif pred_score > 60:
            diff_range = 15
        elif pred_score > 40:
            diff_range = 10
        else:
            diff_range = 5
        if score_diff <= diff_range:
            return True
        return False

    def is_belong_new_group(truth_group_obj, eid):
        '''
        for entity_obj in truth_group_obj[Feature.group['list']]:
            if entity_obj[Feature.entity['eid']] == eid:
                return True
        '''
        if len(truth_group_obj[Feature.group['list']]) <= 4:
            return True
        return False

    def is_not_in_search_result(truth_gid, similar_gid_result_map):
        if truth_gid not in similar_gid_result_map:
            return True
        return False


    truth_gid = eid_gid_map[eid]
    if truth_gid  == gid:
        if display:
            logger.info("\t[CORRECT] [exact match] {gid} - {eid}".format(gid=gid, eid=eid))
        return True, gid
    
    group_str = g_cluster.get(truth_gid)
    if group_str is None:
        return False, gid
    truth_group_obj = json.loads(group_str)

    
    if is_not_in_search_result(truth_gid, similar_gid_result_map):
        if display:
            logger.warn("\t[CORRECT] [not in search_result] {gid1}<=>{gid2} - {eid}".format(gid1=truth_gid, gid2=gid, eid=eid))
        return True, eid_gid_map[eid]
    
    if is_belong_new_group(truth_group_obj, eid):
        if display:
            logger.debug("\t[CORRECT] [new group] {gid1}<=>{gid2} - {eid}".format(gid1=truth_gid, gid2=gid, eid=eid))
        return True, eid_gid_map[eid]
    
    if judge_by_score_diff(similar_gid_result_map, gid, eid_gid_map[eid]):
        if display:
            logger.warn("\t[CORRECT] [diff_sim<20] {gid1}<=>{gid2} - {eid}".format(gid1=truth_gid,
                                                                                   gid2=gid, eid=eid))
        return True, eid_gid_map[eid]
    
    if display:
        logger.error("\t[WRONG] Truth:{truth_gid} =><= {pred_gid}".format(truth_gid=truth_gid,
                                                                     pred_gid = gid))
    return False, gid


def get_precision_recall_score(y_true, y_pred):
    precision = precision_score(y_true, y_pred)
    recall = metrics.recall_score(y_true, y_pred)
    return precision, recall

def get_precision_recall_fscore_support(y_true, y_pred):
    return precision_recall_fscore_support(y_true, y_pred)

def get_rand_score(lables_true, lables_pred):
    return adjusted_rand_score(lables_true, lables_pred)
