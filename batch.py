# -*- coding: utf-8 -*-
"""Example Google style docstrings.

eature -> similarity -> scene_feature -> scene_classifier

"""
import configparser
from pprintpp import pprint as pp
import json
import argparse
from utils import args as args_utls


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument(
        '--project ', action="store", dest="project", required=True)
    parser.add_argument(
        '--action', action="store", dest="action", required=True)
    parser.add_argument(
        '--conf', action="store", dest="conf", required=False)
    args = vars(parser.parse_args())
    args_utls.set_project_path(args)
    args = args_utls.get_args(args)
    from merge.entity_merge import EntityMerge
    from merge.loader import MergeLoader

    import logging
    logger = logging.getLogger('merge.batch')
    logging.basicConfig(filename='/tmp/logs', level=logging.DEBUG)

    if args['action'] == 'offline':
        # OVERWRITE OFFLINE 
        print('offline it\'s ')
        offline_tempid_map = MergeLoader.load_offline(args)
        print(len(offline_tempid_map))
        exit(1)
 
    elif args['action'] == 'entity_merge':
        merge_loader = MergeLoader(args)

        entity_merge = EntityMerge(args=args)
        entity_merge.merge(
            inputs=merge_loader.inputs,
            cluster=merge_loader.cluster,
            idx_mgr=merge_loader.idx
        )
        print('%'*30)
        pp(merge_loader.cluster)
        print(type(merge_loader.cluster))
        #for key in merge_loader.cluster.map.keys():
        #    print(type(merge_loader.cluster.map[key]))

    elif args['action'] == 'group_merge':
        pass
    from export import dump
    dump(entity_merge.cluster, entity_merge.cluster_diff, args)

