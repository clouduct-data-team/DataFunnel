# -*- coding: utf-8 -*-

escapeRules = {'+': r'\+',
               '-': r'\-',
               '&': r'\&',
               '|': r'\|',
               '!': r'\!',
               '(': r'\(',
               ')': r'\)',
               '{': r'\{',
               '}': r'\}',
               '[': r'\[',
               ']': r'\]',
               '^': r'\^',
               '~': r'\~',
               '*': r'\*',
               '?': r'\?',
               ':': r'\:',
               '"': r'\"',
               ';': r'\;',
               ' ': r'\ '}

def escapedSeq(term):
    """ Yield the next string based on the
        next character (either this char
        or escaped version """
    for char in term:
        if char in escapeRules.keys():
            yield escapeRules[char]
        else:
            yield char

def escapeEsSolrString(term):
    if len(term) > 0 and term[0] in ["+", "-"]:
        term = term[1:]
    if len(term) > 0 and term[-1] in ["+", "-"]:
        term = term[:-1]
    """ Apply escaping to the passed in query terms
        escaping special characters like : , etc"""
    term = term.replace('\\', r'\\')   # escape \ first
    return "".join([nextStr for nextStr in escapedSeq(term)])

def escapeCompanyToken(name):
    ReplaceTokens = [u'分公司', u'股份有限公司', u'有限责任公司', u'责任有限公司', u'有限公司']
    for ReplaceToken in ReplaceTokens:
        name = name.replace(ReplaceToken, '')
    return name
