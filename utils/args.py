import configparser
import argparse
import sys
from pprintpp import pprint as pp

def ArgsConfParser():
    conf_parser = argparse.ArgumentParser(
        description=__doc__, # printed with -h/--help
        # Don't mess with format of description
        formatter_class=argparse.RawDescriptionHelpFormatter,
        # Turn off help, so we print all options in response to -h
        add_help=False
        )
    conf_parser.add_argument("-c", "--conf",
                        help="Specify config file", metavar="FILE")

    args, remaining_argv = conf_parser.parse_known_args()

    defaults = { "option":"default" }
    print(">", args.conf)
    if args.conf:
        config = configparser.ConfigParser()
        config.read([args.conf])
        defaults.update(dict(config.items("Defaults")))

    # Parse rest of arguments
    # Don't suppress add_help here so it will handle -h
    parser = argparse.ArgumentParser(
        # Inherit options from config_parser
        parents=[conf_parser]
        )
    parser.set_defaults(**defaults)
    parser.add_argument('--project', action="store")
    parser.add_argument('--action', action="store")
    parser.add_argument('--country_code', action="store")
    parser.add_argument('--city_id', action="store")
    parser.add_argument('--dry_run', action="store")
    parser.add_argument('--parallel', action="store")
    parser.add_argument('--concurrent', action="store")
    parser.add_argument('--country', action="store")
    parser.add_argument('--ground_truth_enable', action="store")
    parser.add_argument('--ground_truth_load', action="store")
    parser.add_argument('--ground_truth_path', action="store")
    parser.add_argument('--ground_truth_type', action="store")
    parser.add_argument('--ground_truth_format', action="store")
    parser.add_argument('--ground_truth_host', action="store")
    parser.add_argument('--ground_truth_db', action="store")

    parser.add_argument('--input_load_enable', action="store")
    parser.add_argument('--input_load_limit', action="store")
    parser.add_argument('--input_group_lookup_enable', action="store")
    parser.add_argument('--input_path', action="store")

    parser.add_argument('--offline_load_enable', action="store")
    parser.add_argument('--offline_path', action="store")
    parser.add_argument('--offline_limit', action="store")

    parser.add_argument('--cluster_load_enable', action="store")
    parser.add_argument('--cluster_load_limit', action="store")
    parser.add_argument('--cluster_path', action="store")
    parser.add_argument('--cluster_type', action="store")
    parser.add_argument('--cluster_host', action="store")
    parser.add_argument('--cluster_db', action="store")

    parser.add_argument('--index_load_enable', action="store")
    parser.add_argument('--index_type', action="store")
    parser.add_argument('--index_host', action="store")
    parser.add_argument('--index_name', action="store")

    parser.add_argument('--reset_tmp_index', action="store")
    parser.add_argument('--tmp_index_type', action="store")
    parser.add_argument('--tmp_index_host', action="store")
    parser.add_argument('--tmp_index_name', action="store")

    parser.add_argument('--dump_enable', action="store")
    parser.add_argument('--dump_full_path', action="store")
    parser.add_argument('--dump_changed_path', action="store")
    parser.add_argument('--dump_deleted_path', action="store")
    parser.add_argument('--dump_statistics_path', action="store")
    parser.add_argument('--dump_footer', action="store")


    args = parser.parse_args(remaining_argv)
    kv_args = vars(args)
    pp(kv_args)
    return kv_args

    #return conf_parser

'''
def DfArgsParser():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument('--project', action="store", dest="project", required=True)
    parser.add_argument('--cluster_in', action="store", dest="cluster_in", required=True)
    parser.add_argument('--cluster_out', action="store", dest="cluster_out", required=False)
    parser.add_argument('--conf', action="store", dest="conf", required=False)

    #parser.add_argument('--parallel', action="store", dest="parallel", required=False)
    #parser.add_argument('--ground-truth-enable', action="store", dest="ground-truth-enable", required=False)
    return parser
'''

def set_project_path(args):
    import sys
    project_path = "projects/%s" % args['project']
    sys.path.append(project_path)
    return args


def read_from_ini(ini_path):
    config = configparser.ConfigParser()
    config.sections()
    config.read(ini_path)
    args = {}
    for section in config.sections():
        args[section] = dict(config[section])
    if args == {}:
        raise 'read empty ini'
    return args


def get_args(args):
    if args['conf'] is None:
        ini_path = "projects/%s/settings.ini" % args['project']
    else:
        ini_path = args['conf']

    args.update(read_from_ini(ini_path))
    return args
