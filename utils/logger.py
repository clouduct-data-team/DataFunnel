import logging


LOG_LEVEL = logging.INFO
LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s\t%(name)-14s| %(log_color)s%(message)s%(reset)s"
from colorlog import ColoredFormatter
'''
from cmreslogging.handlers import CMRESHandler
es_handler = CMRESHandler(hosts=[{'host': '172.18.19.71', 'port': 9200}],
                       auth_type=CMRESHandler.AuthType.NO_AUTH,
                       es_index_name="merge_log")
'''

loggers = {}

def get_logger(logger_name):
    '''
    logging.root.setLevel(LOG_LEVEL)
    formatter = ColoredFormatter(LOGFORMAT)
    stream = logging.StreamHandler()
    stream.setLevel(LOG_LEVEL)
    stream.setFormatter(formatter)
    log = logging.getLogger('pythonConfig')
    log.setLevel(LOG_LEVEL)
    log.addHandler(stream)
    '''
    global loggers
    if loggers.get(logger_name):
        return loggers.get(logger_name)
    logger = logging.getLogger(logger_name)
    logger.setLevel(LOG_LEVEL)
    ch = logging.StreamHandler()
    ch.setLevel(LOG_LEVEL)
    #formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    formatter = ColoredFormatter(LOGFORMAT)
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    #logger.addHandler(es_handler)

    return logger
