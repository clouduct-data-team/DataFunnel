# -*- coding: utf-8 -*-
# !/usr/bin/env python
import argparse
import os
import codecs
from pprintpp import pprint as pp
import json
import time
from multiprocessing import Process, Manager
from multiprocessing import Pool, cpu_count


def map_func(q, pid, kwargs):
    total_counter, total_offline = 0, 0
    print('\t[%s] multiprocess_offline_tel start' % kwargs['pid'])
    load_limit, counter, error = 0, 0, 0
    if 'load_limit' in kwargs:
        load_limit  = kwargs['load_limit']
    tmp = []
    with open(kwargs['cluster_in'], 'r') as cluster_file:
        for idx, group_str in enumerate(cluster_file, start=0):
            if '*' ==  group_str[0]:
                continue

            if load_limit != 0 and idx >= load_limit:
                break

            if idx % (kwargs['num_consumers']) != pid:
                continue

            try:
                map_kernel_result = kwargs['map_kernel'](group_str, kwargs)
                tmp.append(map_kernel_result)
                counter += 1
            except:
                print('\tmissing sid %s - %s, %s' % idx, kwargs['cluster_in'], counter)
                continue
            if  counter % 512 == 0:
                while q.qsize() > 8:
                    print('\t [%s] sleep 5, q.size: %s > 3' % (kwargs['pid'], q.qsize()))
                    time.sleep(0.1)
                q.put(tmp)
                tmp = []
                print('\tloading %s by process[%s], %s' % (idx, pid, counter))

    q.put(tmp)
    tmp = []
    print('\t[%s] finish map_func:(%s)' % (kwargs['pid'], counter))
    return idx, total_counter, total_offline, error


def reduce_func(q, kwargs):
    if 'cluster_out' in kwargs:
        output_file = codecs.open(kwargs['cluster_out'], 'w', 'utf-8')

    counter = 0
    break_flag = False
    pid = os.getppid()
    print('\t output_manager pid:%s' % pid)
    while 1:
        if break_flag:
            break
        m = q.get()

        print('\tget new, queue size:%s' % q.qsize())
        if m == 'kill':
            print('\tkill cluster_output_manager')
            break_flag = True
            break

        for map_kernel_result in m:
            counter += 1
            if 'reduce_kernel' in kwargs:
                output_msg = kwargs['reduce_kernel'](map_kernel_result, kwargs)
            else:
                output_msg = map_kernel_result
            if output_msg is None:
                continue
            if 'cluster_out' in kwargs:
                output_file.write(output_msg + "\n")
        q.task_done()
    if 'cluster_out' in kwargs:
        output_file.flush()
    print('\tfinish cluster_output_manager:%s' % counter)
    return True


def results_aggregator(map_results, reduce_results, q, kwargs):
    results = []
    for result in map_results:
        r = result.get()

    q.put('kill')
    for result in reduce_results:
        r = result.get()

    return results


def parallel_cluster_process(kwargs): 
    #ouput_file_dict, cluster_root, city, offline_field_value, dump_type):
    manager = Manager()
    if 'num_consumers' in kwargs:
        num_consumers = kwargs['num_consumers']
    else:
        num_consumers = 6
    kwargs['num_consumers'] = num_consumers
    pool = Pool(processes=num_consumers)
    q = manager.Queue(30)
    map_results, reduce_results = [], []

    result = pool.apply_async(reduce_func, args=(q, kwargs,))
    reduce_results.append(result)

    for pid in range(0, num_consumers):
        kwargs['pid'] = pid
        result = pool.apply_async(map_func, args=(q, pid, kwargs))
        map_results.append(result)

    if 'results_aggregator' not in kwargs:
        results_aggr_func = results_aggregator
    else:
        results_aggr_func = kwargs['results_aggregator']

    results = results_aggr_func(map_results, reduce_results, q, kwargs)

    pool.close()
    pool.join()
    print('\tFinish parallel_cluster_process')
    time.sleep(5)
    return True, results


if __name__ == '__main__':
    def map_kernel(group_str, kwargs):
        group = json.loads(group_str)
        return group

    def reduce_kernel(group, kwargs):
        return "%s\n" % json.dumps(group, ensure_ascii=False)


    def results_aggregator(map_results, reduce_results, q, kwargs):
        return None

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--cluster_in', action="store", dest="cluster_in", required=True)
    parser.add_argument('--cluster_out', action="store", dest="cluster_out", required=True)
    kwargs = vars(parser.parse_args())
    kwargs['map_kernel'] = map_kernel
    kwargs['reduce_kernel'] = reduce_kernel
    kwargs['results_aggregator'] = results_aggregator
    pp(kwargs)
    parallel_cluster_process(kwargs)

