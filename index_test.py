# -*- coding: utf-8 -*-
# !/usr/bin/env python
import argparse
import os
import codecs
from pprintpp import pprint as pp
import json
import time
#from multiprocessing import Process, Manager
#from multiprocessing import Pool, cpu_count
from utils import args as args_utils
#from utils import parallel_cluster_processor as parallel_utils
#import math
#import importlib

#import os
#import psutil


if __name__ == '__main__':
    args = args_utils.ArgsConfParser()
    args_utils.set_project_path(args)
    from feature import Feature
    from merge.loader import MergeLoader
    from merge.entity_merge  import EntityMerge
    from index.indexmgr import IndexManager
    feature_set = Feature(args)
    index_mgr = IndexManager(feature_set, args)

    merge_loader = MergeLoader(args)

    #import orm
    #args['source_ranking_map'] = orm.get_source_ranking_map()
    entity_merge = EntityMerge(args=args)
    print("new_eid_entity_map: ", len(merge_loader.new_eid_entity_map))
    print("update_eid_entity_map: ", len(merge_loader.update_eid_entity_map))
    print("offline_eid_list: ", len(merge_loader.offline_eid_list))

    print('-----------------------------------------------------')
    new_groups, modified_groups, deleted_groups =  {}, {}, {}


    _modified_groups = entity_merge.update_eids(
        args, 
        merge_loader.update_eid_entity_map,
        merge_loader.offline_eid_gid_map, 
        merge_loader.cluster,
        merge_loader.idx_mgr
    )
    modified_groups.update(_modified_groups)

    _modified_groups, new_groups = entity_merge.merge(
        inputs=merge_loader.new_eid_entity_map,
        #inputs_raw = merge_loader.inputs_raw,
        cluster=merge_loader.cluster,
        group_truth_e2g_map=merge_loader.group_truth_e2g_map,
        idx_mgr=merge_loader.idx_mgr,
        eid_2_gid_group_map = merge_loader.eid_2_gid_group_map
    )

    pp(merge_loader.eid_2_gid_group_map)
    with open('/tmp/eid_2_gid.txt', 'w') as f:
        for eid, group in merge_loader.eid_2_gid_group_map.items():
            hit = 0
            for gid, _ in group.items():
                hit = 1
                f.write("%s,%s\n"%(eid, gid))
                break
            if hit == 0:
                f.write("%s, %s\n"%(eid, ''))

    #sys.exit(1)
    #pp(merge_loader.offline_eid_gid_map)
    modified_groups.update(_modified_groups)

    _modified_groups, _deleted_groups = entity_merge.offline_eids(args, merge_loader.offline_eid_list,
                              merge_loader.offline_eid_gid_map, merge_loader.cluster)
    modified_groups.update(_modified_groups)
    deleted_groups.update(_deleted_groups)


    '''
    print("merge_loader.inputs: ", len(merge_loader.inputs), type(merge_loader.inputs))
    print("modified_groups: ", len(modified_groups), type(modified_groups))
    print("new_groups:", len(new_groups), type(new_groups))
    print("deleted_groups:", len(deleted_groups), type(deleted_groups))
    '''

    print("modified_groups: ", len(merge_loader.update_eid_entity_map),
            type(merge_loader.update_eid_entity_map))
    print("new_groups:", len(merge_loader.new_eid_entity_map),
            type(merge_loader.new_eid_entity_map))
    print("deleted_groups:", len(merge_loader.offline_eid_list),
            type(merge_loader.offline_eid_list))

    from orm import get_source_ranking_map, get_source_ch_map, get_catkey

    source_ranking = get_source_ranking_map()
    source_ch = get_source_ch_map()
    cat_key = get_catkey()
    import marisa_trie
    catkey_prefix_tree = marisa_trie.Trie(cat_key.keys())

    from recommend.mrecommend import MRecommend
    recommend = MRecommend(args['country'], source_ranking, source_ch, catkey_prefix_tree)

    import export
    export.export(args, deleted_groups, modified_groups, new_groups, recommend)

    #pp(entity)
    #print('<<<<<<<<<<<<<<<<<<<<<')
    #query_obj = FeatureSet.get_query_obj(args['index-type'], args, entity)
    #pp(index_adm.query(query_obj))
    #pp(entity)
    #print('<<<<<<<<<<<<<<<<<<<<<')
    #query_obj = FeatureSet.get_solr_query_obj(args, entity=entity)
    #pp(solr_idx_adm.query(query_obj))
    #print('>>>>>>>>>>>>>>>>>>>>>')
    #query_obj = FeatureSet.get_es_query_obj(args, entity=entity)
    #pp(query_obj)
    #pp(es_idx_adm.query(query_obj))
    #print('=====================')
    
    

    '''
    init_obj = {
        'solr_uri': "http://172.18.21.221:8983/solr",
        'solr_index': "yuloreFWD2_shard1_replica1"
    }
    solr_idx_adm = SolrIndexAdm(init_obj)
    init_obj = {
        'es_host': "120.92.21.103",
        'es_index': "china_group"
    }
    es_idx_adm = EsIndexAdm(init_obj)
    fl = ",".join(FeatureSet.map.keys()) + ",shopid"
    '''
    '''
    for eid, entity in input_eid_str_map.items():
        pp(entity)
        print('<<<<<<<<<<<<<<<<<<<<<')
        query_obj = FeatureSet.get_query_obj(args['index-type'], args, entity)
        pp(index_adm.query(query_obj))
        #pp(entity)
        #print('<<<<<<<<<<<<<<<<<<<<<')
        #query_obj = FeatureSet.get_solr_query_obj(args, entity=entity)
        #pp(solr_idx_adm.query(query_obj))
        #print('>>>>>>>>>>>>>>>>>>>>>')
        #query_obj = FeatureSet.get_es_query_obj(args, entity=entity)
        #pp(query_obj)
        #pp(es_idx_adm.query(query_obj))
        #print('=====================')
    '''
