# -*- coding: utf-8 -*-
# !/usr/bin/env python
import argparse
import os
import codecs
from pprintpp import pprint as pp
import json
import time
from multiprocessing import Process, Manager
from multiprocessing import Pool, cpu_count
from utils import args as args_utils
from utils import parallel_cluster_processor as parallel_utils
import math


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--project', action="store", dest="project", required=True)
    parser.add_argument('--cluster_in', action="store", dest="cluster_in", required=True)
    parser.add_argument('--cluster_out', action="store", dest="cluster_out", required=True)
    parser.add_argument('--conf', action="store", dest="conf", required=False)
    kwargs = vars(parser.parse_args())
    kwargs = args_utils.get_args(kwargs)
    args_utils.set_project_path(kwargs)
    kwargs = args_utils.get_args(kwargs)
    from group import Group
    from entity import Entity
    from featureset import FeatureSet
    feature_set = FeatureSet()
    feature_list = feature_set.map.keys()

    def map_kernel(group_str, kwargs):
        def average(s): return sum(s) * 1.0 / len(s)

        group = Group(json_str=group_str)

        if len(group.map) <= 2:
            return None

        cohesion = {
            'gid': group.id,
            'max': -1,
            'min': 1,
            'member': {}
        }

        cohesion_cached = {}
        for idx_1, (eid_1, entity_1) in enumerate(group.map.items()):
            cohesion_cached[idx_1] = {}
            cohesion_total = 0
            for idx_2, (eid_2, entity_2) in enumerate(group.map.items()):
                if idx_1 == idx_2:
                    continue
                if idx_1 > idx_2:
                    _cohesion = cohesion_cached[idx_2][idx_1]
                else:
                    similarity = feature_set.get_entity_similarity(entity_1, entity_2)
                    _cohesion = feature_set.get_e2e_cohesion(similarity)
                    cohesion_cached[idx_1][idx_2] = _cohesion

                if _cohesion > cohesion['max']:
                    cohesion['max'] = _cohesion
                if _cohesion < cohesion['min']:
                    cohesion['min'] = _cohesion
                cohesion_total += _cohesion

            cohesion['member'][eid_1] = {
                'cohesion': cohesion_total / len(group.map),
                'feature': feature_set.get_feature(entity_1)
            }

        print('*'*10)
        cohesion_list = [_['cohesion'] for _ in cohesion['member'].values()]
        cohesion['avg'] = average(cohesion_list)
        variance = list(map(lambda x: (x - cohesion['avg'])**2, cohesion_list))
        cohesion['std_dev'] = math.sqrt(average(variance))
        return cohesion

    def reduce_kernel(map_kernel_result, kwargs):
        """
        return "%s\n" % json.dumps(group, ensure_ascii=False)
        """
        try:
            if map_kernel_result is None:
                return None
            group_json = json.dumps(map_kernel_result, ensure_ascii=False)
            return group_json
        except Exception as e:
            print('>>>'+str(e))
        return None

    kwargs['map_kernel'] = map_kernel
    kwargs['reduce_kernel'] = reduce_kernel
    #kwargs['load_limit'] = 100
    parallel_utils.parallel_cluster_process(kwargs)
    """
    {
        'gid_1': {
            'cohesion': {
                'avg': ,
                'min': ,
                'max': ,
                'std_div':
            },
            'list': {
                'eid': 1,
                'eid': 2,
                'eid': 3,
            }
        },
        ...
    }

    """
