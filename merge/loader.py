# -*- coding: utf-8 -*-
import json
import time
from feature import Feature
from ABC.cluster import Cluster
from index.indexmgr import IndexManager
from multiprocessing import Manager, Pool, cpu_count
import examine.ground_truth
from utils.logger import get_logger
import time
from pathlib import Path
from pprintpp import pprint as pp
logger = get_logger('MergeLoader')

"""Module description"""

__author__ = 'chandler'


manager = Manager()



class MergeLoader(object):

    def __init__(self, args, **kwargs):
        self.args = args
        self.offline_eids = self.__class__.load_offline_eids(args)
        self.feature_set = Feature(args)
        cluster = self.init_cluster(args)
        idx_mgr = self.init_idx_mgr(args)
        self.inputs = self.__class__.load_inputs(args)
        self.eid_2_gid_group_map = self.__class__.load_eid_2_gid_group_map(args, self.inputs, idx_mgr, mapping_type='merge')
        merge_gid_cadidate_map = self.__class__.convert_eid_gids_2_gid_map(self.eid_2_gid_group_map)
        #self.offline_eid_gid_map = self.__class__.load_offline_eid_gid_map(self.offline_eids, idx_mgr)
        self.offline_eid_gid_map = self.__class__.load_eid_2_gid_group_map(args, self.offline_eids, idx_mgr, mapping_type='offline')
        offline_gid_cadidate_map = {gid: True for (eid, gid) in self.offline_eid_gid_map.items()}
        logger.info("[LOADER] [OFFLINE-EID-GID-MAP: %s]", len(self.offline_eid_gid_map))
        self.cluster, self.idx_mgr = self.__class__.load_cluster_idx_mgr(args, 
                    cluster, idx_mgr, merge_gid_cadidate_map, offline_gid_cadidate_map)

        self.args.update(Feature.load_essential(args))
        logger.info('[LOADER] [CLUSTER: %s]  [INPUTS: %s] [OFFLINE: %s]' %(
            len(self.cluster), len(self.inputs), len(self.offline_eids)))

        self.new_eid_entity_map, self.update_eid_entity_map, self.offline_eid_list = self.__class__.process_inputs_overlay(self.inputs, self.offline_eid_gid_map)

        self.group_truth_e2g_map = self.load_ground_truth(args, self.new_eid_entity_map)
        if args['ground_truth_enable'] == "yes":
            logger.info("[LOADER] [GROUND-TRUTH: %s]", len(self.group_truth_e2g_map))
        self.inputs = None

    @classmethod
    def process_inputs_overlay(cls, inputs, offline_eid_gid_map):
        input_eids_set = set(inputs.keys())
        offline_eids_set = set(offline_eid_gid_map.keys())
        new_eids = list(input_eids_set - offline_eids_set)
        update_eids = list(input_eids_set & offline_eids_set)
        offline_eids = list(offline_eids_set - input_eids_set)

        new_eid_entity_map, update_eid_entity_map, offline_eid_list = {}, {}, []
        for eid in new_eids:
            new_eid_entity_map[eid] = inputs[eid]
        for eid in update_eids:
            update_eid_entity_map[eid] = inputs[eid]
        for eid in offline_eids:
            offline_eid_list.append(eid)

        return new_eid_entity_map, update_eid_entity_map, offline_eid_list


    @classmethod
    def load_offline_eids(cls, args):
        offline_eids = {}
        if args['offline_load_enable'] != "yes":
            return offline_eids
        logger.debug('[LOADER] [OFFLINE] [START]')
        offline_path = Path(args['offline_path'])
        if not offline_path.is_file():
            return offline_eids
        with open(args['offline_path'], 'r', encoding='utf-8') as f:
            for line in f:
                offline_eids[line.strip()] = True
        logger.debug('[LOADER] [OFFLINE] [FINISH:%s]' % len(offline_eids))
        return offline_eids


    @classmethod
    def load_eid_2_gid_group_map(cls, args, eid_input_map, idx_mgr, mapping_type):
        eid_2_gid_group_map = {}

        for eid, entity_obj in eid_input_map.items():
            if mapping_type in "offline":
                gid = idx_mgr.query_gid_by_eid(eid)
                if gid is not None:
                    eid_2_gid_group_map[eid] = gid
            if mapping_type == 'merge':
                if  args['input_group_lookup_enable'] == 'no':
                    gid_group_map = {}
                else:
                    gid_group_map = idx_mgr.query_similar_group_by_entity(entity_obj)
                eid_2_gid_group_map[eid] = gid_group_map
        return eid_2_gid_group_map

    @classmethod
    def convert_eid_gids_2_gid_map(cls, eid_gids_map):
        gid_map = {}
        for eid, gid_group_map in eid_gids_map.items():
            for gid in gid_group_map.keys():
                gid_map[gid] = True
        return gid_map

    @classmethod
    def load_offline_eid_gid_map(cls, offline_tempid, idx_mgr):
        offline_eid_gid_map = {}
        for eid in offline_tempid.keys():
            gid = idx_mgr.query_gid_by_eid(eid)
            if gid is not None:
                offline_eid_gid_map[eid] = gid
        return offline_eid_gid_map


    def init_cluster(self, args):
        logger.debug('[LOADER] [CLUSTER] [START]')
        if args['cluster_type'] == 'dict':
            from ABC.cluster import Cluster
        elif args['cluster_type'] == 'redis':
            from cluster.rediscluster import RedisCluster as Cluster
        else:
            raise
        cluster = Cluster(args)
        logger.info('[LOADER] [CLUSTER] [FINISH:%s]' %len(cluster.map))
        return cluster

    def init_idx_mgr(self, args):
        index_type = args['index_type']
        if index_type in ['EsIndexAdm', 'SolrIndexAdm']:
            idx_mgr = IndexManager(self.feature_set, args)
        elif index_type == 'dict':
            from ABC.index import Index
            idx_mgr = IndexManager(args, index=Index)
        elif index_type == 'REDIS':
            from index.redisindex import RedisIndex as Index
            idx_mgr = IndexManager(args, index=Index)
        else:
            raise
        return idx_mgr


    @classmethod
    def load_idx_mgr(cls, cluster, idx_mgr, args):
        print("[START] LOADING IDX_MGR ...")
        if args['index_load_enable'] == "no":
            return idx_mgr

        num_consumers = cpu_count() * 2
        pool = Pool(processes=num_consumers)
        for pid in range(0, num_consumers):
            pool.apply_async(cls.populate_idx_mgr,
                             args=(args, pid, num_consumers, idx_mgr, cluster))
        pool.close()
        pool.join()
        return idx_mgr

    @classmethod
    def load_cluster(cls, cluster, args):

        num_consumers = cpu_count() * 2
        pool = Pool(processes=num_consumers)
        results = []
        #d, gid_feature_map  = manager.dict(), manager.dict()
        for pid in range(0, num_consumers):
            result = pool.apply_async(cls.populate_cluster,
                                      args=(args, pid, num_consumers))
            results.append(result)
        pool.close()
        pool.join()
        time.sleep(0.3)
        for result in results:
            gid_group_str_map = result.get()
            for gid, group_str in gid_group_str_map.items():
                cluster.map[gid] = group_str
        print('Finished load %s' % len(cluster.map))
        return cluster

    @classmethod
    def load_cluster_idx_mgr(cls, args, cluster, idx_mgr, merge_gid_cadidate_map,
                             offline_gid_cadidate_map):
        if args['cluster_load_enable'] == args['index_load_enable'] == 'no':
            return cluster, idx_mgr
        num_consumers = cpu_count() * 2
        pool = Pool(processes=num_consumers)
        results = []
        #d, gid_feature_map  = manager.dict(), manager.dict()
        for pid in range(0, num_consumers):
            result = pool.apply_async(cls.populate_cluster_idx_mgr,
                                      args=(args, pid, num_consumers, cluster, idx_mgr,
                                            merge_gid_cadidate_map, offline_gid_cadidate_map))
            results.append(result)
        pool.close()
        pool.join()
        time.sleep(0.3)
        if args['cluster_type'] == 'dict':
            for result in results:
                cluster_per_process = result.get()[0]
                for gid, group_str in cluster_per_process.map.items():
                    cluster.set(gid, group_str)
        logger.info('[LOAD_CLUSTER_IDX_MGR] [FINISHE] Counts:%s' % len(cluster.map))
        return cluster, idx_mgr

    def load_inputs(args):
        if args['input_load_enable'] != 'yes':
            return {}
        logger.debug('[LOADER] [INPUTS] [START]')
        # To-Do inputs consider use dict instead of list
        try:
            fpath = args['input_path']
            if not Path(fpath).is_file():
                return {}
        except KeyError as err:
            raise KeyError('args did not contain input')
        input_load_limit = 0
        inputs = {}
        if args['input_load_limit'].isdigit():
            input_load_limit = int(args['input_load_limit'])

        with open(fpath, 'r', encoding='utf-8') as input_file:
            for idx, tempid_str in enumerate(input_file, start=0):
                tempid_str = tempid_str.strip()
                if input_load_limit != 0 and idx == input_load_limit:
                    break
                try:
                    obj = json.loads(tempid_str)
                except Exception as e:
                    if tempid_str.startswith("*"):
                        break
                    print(idx, str(e))
                inputs[obj[Feature.entity['eid']]] = obj
        logger.info('[LOADER] [INPUTS] [FINISH: %s]' % len(inputs))
        return inputs

    @classmethod
    def populate_idx_mgr(cls, args, pid, num_consumers, idx_mgr, cluster):
        for idx , (gid, group_str) in enumerate(cluster.map.items(), start=0):
            if idx % num_consumers != pid:
                continue
            print(idx, pid, gid, len(cluster.map))
            idx_mgr.add_group(group_str)
        return ""

    @classmethod
    def populate_cluster(cls, args, pid, num_consumers):
        cluster = {}
        print('loading by %s' % pid)
        try:
            fpath = args['cluster_path']
            if not Path(fpath).is_file():
                return {}
        except KeyError as err:
            raise KeyError('args did not contain cluster')
        load_limit = 0
        if args['cluster_load_limit'].isdigit():
            load_limit = int(args['cluster_load_limit'])
        if args['cluster_type'] == 'dict':
            cluster = Cluster(args)
        with open(fpath, 'r', encoding='utf-8') as cluster_file:
            for idx, group_str in enumerate(cluster_file, start=0):
                if (load_limit != 0 and idx == load_limit) or group_str.startswith("*"):
                    break
                if idx % num_consumers != pid:
                    continue
                try:
                    group_str = group_str.strip()
                    obj = json.loads(group_str)
                    cluster[obj[Feature.group['gid']]] = group_str
                    #inputs[obj[Feature.entity['eid']]] = obj

                    #sid = group_str[:50].split('","')[0].replace('{"sid":"', '')
                    #cluster[sid] = group_str
                except Exception as e:
                    break
                if len(cluster) % 1024 == 0:
                    print('loading %s by process[%s], %s' % (idx, pid, len(cluster)))
        print('finished %s'%pid)
        return cluster


    @classmethod
    def populate_cluster_idx_mgr(cls, args, pid, num_consumers, cluster, idx_mgr,
                                 merge_gid_cadidate_map=None, offline_gid_cadidate_map=None):
        #print(len(merge_gid_cadidate_map), len(offline_gid_cadidate_map), '<<<<')

        logger.debug('[PID:{:>2}/{:>2}] [CLUSTER_IDX] [START]'.format(pid, num_consumers))
        try:
            fpath = args['cluster_path']
        except KeyError as err:
            raise KeyError('args did not contain cluster')
        load_limit = 0
        if args['cluster_load_limit'].isdigit():
            load_limit = int(args['cluster_load_limit'])
        if args['cluster_type'] == 'dict':
            cluster = Cluster(args)

        with open(fpath, 'r', encoding='utf-8') as cluster_file:
            for idx, group_str in enumerate(cluster_file, start=0):
                if (load_limit != 0 and idx == load_limit) or group_str.startswith("*"):
                    break
                if idx % num_consumers != pid:
                    continue
                try:
                    pass_1, pass_2 = False, False
                    group_str = group_str.strip()
                    # gid = group_str[:50].split('", "')[0].replace('{"sid": "', '').replace('\"','').replace(',','')
                    gid = group_str[:50].split(',')[0].split('":')[-1].replace('"', '')

                    # TO-DO: Add extract_sid from Feature
                    if len(gid) not in [32, 40]:
                        logger.error('wrong gid\t' + gid + "\t" + group_str[:50])
                    if merge_gid_cadidate_map is not None and gid not in merge_gid_cadidate_map:
                        pass_1 = True
                    if offline_gid_cadidate_map is not None and gid not in offline_gid_cadidate_map:
                        pass_2 = True
                    if pass_1 and pass_2:
                        continue
                    cluster.set(gid, group_str)
                    if args['index_load_enable'] == 'yes':
                        idx_mgr.add_group(group_str=group_str, gid=gid)
                except Exception as e:
                    break
                if len(cluster) % 4096 == 0:
                    logger.debug('[PID:{:>2}/{:>2}] [CLUSTER_IDX] [LOAD   :{:>7}]'.format(pid, num_consumers, len(cluster)))
        logger.debug('[PID:{:>2}/{:>2}] [CLUSTER_IDX] [FINISH: {:>7}]   '.format(pid, num_consumers, len(cluster)))
        return cluster, idx_mgr

    @classmethod
    def ml_load_ground_truth(cls, args, pid, num_consumers,  inputs):
        if args['ground_truth_load'] != 'yes':
            return {}

        group_truth_e2g_map = {}
        with open(args['ground_truth_path'], 'r', encoding='utf-8') as f:
            for idx, group_str in enumerate(f, start=0):
                if idx % num_consumers != pid or group_str.startswith('*'):
                    continue
                group_str = group_str.strip()
                group_obj = json.loads(group_str)
                gid = group_obj[Feature.group['gid']]
                for entity_obj in group_obj[Feature.group['list']]:
                    if entity_obj[Feature.entity['eid']] in inputs:
                        group_truth_e2g_map[entity_obj[Feature.entity['eid']]] = gid
        logger.debug("\t        [GROUND-TRUTH PID: {pid}] [FINISH:{num}]".format(
            pid=pid, num=len(group_truth_e2g_map)))
        return group_truth_e2g_map

    @classmethod
    def load_ground_truth(cls, args, inputs):
        logger.info('[LOADER] [GROUND-TRUTH] [START:%s]' % len(inputs))
        if args['ground_truth_enable'] != 'yes':
            return {}
        if args['ground_truth_path'] == '':
            logger.error("\t[GROUND-TRUTH] [PATH MISSING: %s]" % args['ground-truth-path'])
            raise
        group_truth_e2g_map = {}
        num_consumers = cpu_count() * 2
        pool = Pool(processes=num_consumers)
        results = []
        #d, gid_feature_map  = manager.dict(), manager.dict()
        for pid in range(0, num_consumers):
            result = pool.apply_async(examine.ground_truth.load_ground_truth,
                                      args=(args, pid, num_consumers, inputs))
            results.append(result)
        pool.close()
        pool.join()
        time.sleep(0.3)
        if args['ground_truth_type'] == 'dict':
            for result in results:
                _group_truth_e2g_map = result.get()
                for eid, gid in _group_truth_e2g_map.items():
                    group_truth_e2g_map[eid] = gid
        logger.info('[LOADER] [GROUND-TRUTH] [FINISH:%s]' % len(inputs))
        return group_truth_e2g_map

    '''
    def __repr__(self):
        pass
    '''

