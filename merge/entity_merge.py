# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from ABC.merge import Merge
from feature import Feature
#from group import Group
import json

from examine.ground_truth import *
"""Module description"""
import time
from inspect import currentframe, getframeinfo


__author__ = 'chandler'

from multiprocessing import Process, Manager
from multiprocessing import Pool, cpu_count
from pprintpp import pprint as pp
manager = Manager()


from utils.logger import get_logger
logger = get_logger('EntityMerge')

g_inputs = ''
g_inputs_raw = ''
g_cluster = ''
g_group_truth_e2g_map = ''
g_eid_2_gid_group_map =''

class EntityMerge(Merge):

    def __init__(self, args):
        super(EntityMerge, self).__init__(args)


    @classmethod
    def offline_eids(cls, args, offline_eids, eid_gid_map, cluster):
        modified_groups, deleted_groups  = {}, {}
        offline_len = len(offline_eids)
        for idx, eid in enumerate(offline_eids, start=1):
            logger.debug("%s /%s\t%s" % (idx, offline_len, eid))

            gid = eid_gid_map[eid]
            if gid is None:
                logger.error("\t[OFFLINE] [GID not in EID_GID_MAP] [EID:{eid}]".format(eid=eid))
                continue

            if gid in modified_groups:
                group_obj = modified_groups[gid]
            else:
                group_str = cluster.get(gid)
                if group_str is None:
                    logger.error("\t[OFFLINE] [GROUP_STR not in CLUSTER] [GID:{gid}]".format(gid=gid))
                    continue
                group_obj = json.loads(group_str)
            if len(group_obj[Feature.group['list']]) == 1:
                logger.info("\t[OFFLINE] [Deleted GROUPS] [GID: {gid}] ".format(gid=gid))
                deleted_groups[gid] = True
            else:
                del_entity = None
                for _entity in group_obj[Feature.group['list']]:
                    if _entity[Feature.entity['eid']] == eid:
                        del_entity = _entity
                        break
                if del_entity is None:
                    logger.error("\t[OFFLINE] [ENTITY not in GROUP] [GID: {gid}, EID: {eid}]".format(gid=gid, eid=eid))
                else:
                    logger.debug("\t[OFFLINE] [ENTITY in GROUP] [GID: {gid}, EID: {eid}]".format(gid=gid, eid=eid))
                    group_obj[Feature.group['list']].remove(del_entity)
                    modified_groups[gid] = group_obj
            logger.debug("modified_groups: {a}, deleted_groups:{b}".format(a=len(modified_groups), b=len(deleted_groups)))

        for eid in deleted_groups.keys():
            if eid in modified_groups:
                del modified_groups[eid]

        return modified_groups, deleted_groups

    @classmethod
    def update_eids(cls, args, update_eid_entity_map, eid_gid_map, cluster, idx_mgr):
        modified_groups = {}
        update_len = len(update_eid_entity_map)
        for idx, (eid, entity) in enumerate(update_eid_entity_map.items(), start=1):
            gid = eid_gid_map[eid]
            logger.debug("%s /%s\t[gid: %s, eid:%s]" % (idx, update_len, gid, eid))
            if gid is None:
                logger.error("\t[UPDATE] [GID not in EID_GID_MAP] [EID:{eid}]".format(eid=eid))
                continue
            group_str = cluster.get(gid)
            if group_str is None:
                logger.warn("\t[UPDATE] [GROUP_STR not in CLUSTER] [GID:{gid}]".format(gid=gid))
                continue
            group_obj = json.loads(group_str)

            del_entity = None
            for _entity in group_obj[Feature.group['list']]:
                if _entity[Feature.entity['eid']] == eid:
                    del_entity = _entity
                    break
            if del_entity is None:
                logger.error("\t[UPDATE] [ENTITY not in GROUP] [GID: {gid}, EID: {eid}]".format(gid=gid, eid=eid))
            else:
                group_obj[Feature.group['list']].remove(del_entity)
                cls.add_entity_to_group(args, cluster, idx_mgr,  group_obj, entity, modified_groups)
        return modified_groups



    @classmethod
    def merge_mul(cls, args, pid, num_consumers, idx_mgr):
        global g_inputs
        global g_cluster
        global g_group_truth_e2g_map
        global g_eid_2_gid_group_map
        pred_group_truth_e2g_map = {}
        modified_groups, new_groups, deleted_groups = {}, {}, {}
        #print(pid, len(g_inputs_raw), id(g_inputs_raw),g_inputs_raw[pid], id(g_inputs_raw[pid]))
        input_len = len(g_inputs)

        for idx, (eid, entity_obj) in enumerate(g_inputs.items(), start=0):
            if idx % 10000 == 0:
                logging.debug(pid, idx, eid, id(entity_obj))

            if idx % num_consumers != pid:
                continue

            logger.debug("%s /%s\t%s\t%s\t%s\t%s" %
                        (idx, input_len, eid, entity_obj['name'],
                         entity_obj['address']['street'], entity_obj['tel'][0]['number']))

            similar_gid_result_map = g_eid_2_gid_group_map[eid]

            if len(similar_gid_result_map) == 0:
                group_obj = Feature.convert_e2g(args, entity_obj)
                gid = group_obj[Feature.group['gid']]
                logger.debug("\t[CREATE GROUP] \tgid:[{gid}]\teid:[{eid}]\tcase:[{case}]".format(
                            gid=gid, eid=eid, case=0))
                cls.add_group(args, g_cluster, idx_mgr, new_groups, group_obj)
                if args['ground_truth_enable'] == 'yes':
                    judge_result, pred_gid = same_with_ground_truth(g_cluster, eid, gid, g_group_truth_e2g_map, similar_gid_result_map)
                    pred_group_truth_e2g_map[eid] = pred_gid
                continue

            merge_candidate_gid_sim_list = []
            for gid, query_group_result in similar_gid_result_map.items():
                group_str = g_cluster.get(gid)

                if group_str is None:
                    logger.error("\t[NOT IN CLUSTER!!] gid[{gid} not in cluster".format(gid=gid))
                    continue

                group_obj = json.loads(group_str)
                should_merge, sim, reason = Feature.e2g_should_merge(args, entity_obj, group_obj, query_group_result)
                if should_merge is True:
                    merge_candidate_gid_sim_list.append((gid, sim, reason))
                    continue

            if len(merge_candidate_gid_sim_list) == 0:
                group_obj = Feature.convert_e2g(args, entity_obj)
                gid = group_obj[Feature.group['gid']]
                logger.debug("\t[CREATE GROUP] \tgid:[{gid}]\teid:[{eid}]\tcase:[{case}]".format(
                    gid=gid, eid=eid, case=1))
                cls.add_group(args, g_cluster, idx_mgr, new_groups, group_obj)

            elif len(merge_candidate_gid_sim_list) >= 1:
                most_like_gid = Feature.get_most_like_gid(merge_candidate_gid_sim_list)
                group_str = g_cluster.get(most_like_gid)
                group_obj = json.loads(group_str)
                gid, eid = group_obj[Feature.group['gid']], entity_obj[Feature.entity['eid']]

                cls.add_entity_to_group(args, g_cluster, idx_mgr,  group_obj, entity_obj, modified_groups)

            if len(merge_candidate_gid_sim_list) > 1:
                # TO-DO: log candidates for group-group merge
                logger.debug("\t[LOG] Keep all candidates for groups merge")
                logger.debug("\t[SIM GROUPS]: {sim_groups}".format(
                    sim_groups=json.dumps(merge_candidate_gid_sim_list, ensure_ascii=False)))

            if args['ground_truth_enable'] == 'yes':
                judge_result, pred_gid = same_with_ground_truth(g_cluster, eid, gid, g_group_truth_e2g_map, similar_gid_result_map)
                pred_group_truth_e2g_map[eid] = pred_gid

        logger.info("len(new_groups) = {new_groups}, len(modified_groups) = {modified_groups}".format(
                modified_groups=len(modified_groups),  new_groups=len(new_groups)))
        logger.info("[Merge MUL] [%s] [finish]"%pid)
        #time.sleep(30)
        return modified_groups, new_groups, pred_group_truth_e2g_map

    @classmethod
    def merge_result(cls, merger, mergee):
        # merger  <-- mergee
        list_key = Feature.group['list']
        for gid, group_obj in mergee.items():
            if gid in merger:
                merger[gid][list_key].update(group_obj[list_key])
            else:
                merger[gid] = group_obj
        return merger


    def merge(self, inputs, cluster, group_truth_e2g_map, idx_mgr, eid_2_gid_group_map):
        global g_inputs
        global g_cluster
        global g_idx_mgr
        global g_group_truth_e2g_map
        global g_eid_2_gid_group_map

        g_inputs = inputs
        g_cluster, g_idx_mgr = cluster, idx_mgr
        g_group_truth_e2g_map = group_truth_e2g_map
        g_eid_2_gid_group_map = eid_2_gid_group_map
        pred_group_truth_e2g_map = {}
        modified_groups, new_groups = {}, {}
        manager = Manager()
        result_set = manager.dict()

        if self.args['parallel'] == 'yes':
            num_consumers = int(self.args['concurrent'])
            pool = Pool(processes=num_consumers, maxtasksperchild=1)
            results = []
            logger.info('[Entity Merge] [Start]')
            for pid in range(0, num_consumers):
                result = pool.apply_async(EntityMerge.merge_mul,
                                          args=(self.args, pid, num_consumers, idx_mgr))
                results.append(result)
                
            pool.close()
            pool.join()
            logger.debug("[Merge] [Join] [Finish]")

            for idx, result in enumerate(results):
                result.get()
                _modified_groups, _new_groups, _pred_group_truth_e2g_map = result.get()
                #modified_groups.update(_modified_groups)
                modified_groups = EntityMerge.merge_result(modified_groups, _modified_groups)
                new_groups.update(_new_groups)
                pred_group_truth_e2g_map.update(_pred_group_truth_e2g_map)
                result_set.pop(pid, None)

        else:
            modified_groups, new_groups, pred_group_truth_e2g_map = EntityMerge.merge_mul(self.args, 0, 1, idx_mgr)


        if self.args['ground_truth_enable'] == 'yes':
            lables_true, lables_pred = get_truth_pred_lables(g_cluster, g_group_truth_e2g_map,
                                                            pred_group_truth_e2g_map)
            p_r_f_s = get_classification_report(lables_true, lables_pred, mode=None).split("avg / total")[-1]
            print("P/R/F/S:{p_r_f_s}".format(p_r_f_s=p_r_f_s))

        result = None

        logger.info("FINISH MERGE")
        return modified_groups, new_groups


