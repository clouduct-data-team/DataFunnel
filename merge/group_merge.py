# -*- coding: utf-8 -*-
import logging
from collections import defaultdict
from ABC.merge import Merge

"""Module description"""

__author__ = 'chandler'
logger = logging.getLogger('abc.merge')


class GroupMerge(Merge):

    def __init__(self, args):
        super(GroupMerge, self).__init__(args)

    def merge(self, cluster, idx_mgr):

        def make_cached_key(prefix, postfix, group_version_dict):
            return "%s%03d%s%03d" % (prefix[:5], group_version_dict[prefix],
                                     postfix[:5], group_version_dict[postfix])

        def find_cache_key(group1, group2, group_version_dict):
            a, b, prefix, postfix = None, None, None, None
            for i in range(len(group1.id)):
                if a is None and group1.id[i].isdigit():
                    a = group1.id[i]
                if b is None and group2.id[i].isdigit():
                    b = group2.id[i]
                if a is not None and b is not None:
                    if a == b:
                        a = b = None
                        continue
                    elif a > b:
                        prefix, postfix = group1.id, group2.id
                    else:
                        prefix, postfix = group2.id, group1.id
                    return make_cached_key(prefix, postfix, group_version_dict)
            # very rare case
            return make_cached_key(group1.id, group2.id, group_version_dict)

        group_version_dict = defaultdict(int)
        gid_merge_cached_table, skip_list, rounds = {}, [], 0

        while True:
            rounds += 1
            count1, have_merged = 0, False
            print("cluster.map", len(cluster.map))
            for gid1 in list(cluster.map.keys()):
                count1 += 1
                count2 = 0
                group1 = cluster.get(gid1)
                if group1 is None:
                    continue

                similar_gids = idx_mgr.get_similar_gids(group1)

                for gid2 in similar_gids:
                    if gid1 == gid2 or gid2 in skip_list:
                        continue
                    count2 += 1
                    group2 = cluster.get(gid2)
                    if group2 is None:
                        continue
                    print("rounds:%s - len(cluster):%s - (%s,%s) - (%s, %s)" % \
                          (rounds, len(cluster.map), count1, count2, gid1, gid2))

                    cache_key = find_cache_key(group1, group2, group_version_dict)
                    if cache_key in gid_merge_cached_table:
                        result = gid_merge_cached_table[cache_key]
                    else:
                        result = self.feature_set.should_be_merge_bt_groups(group1, group2)
                        gid_merge_cached_table[cache_key] = result

                    merge_flag = result['classify_result'][1]

                    if merge_flag is True:
                        have_merged = True
                        self.merge_group(cluster, idx_mgr, group1, group2, 2)
                        group_version_dict[group1.id] += 1
                        skip_list.append(gid2)

            if not have_merged:
                print('finish !', len(cluster.map))
                break
        return cluster


