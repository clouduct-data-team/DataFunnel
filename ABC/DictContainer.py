# -*- coding: utf-8 -*-
import json

class DictContainer(object):
    # For those class extend from DictContainer,
    # You should define class FEATURE_MAPPING = {}
    """
    {
    'key1': 'key2',
    'key2': fun(),
    }
    """

    def __init__(self,  *args, **kwargs):
        if 'json_str' in kwargs:
            obj = json.loads(kwargs['json_str'])
        elif 'obj' in kwargs:
            obj = kwargs['obj']
        elif 'from_redis' in kwargs:
            obj = kwargs['from_redis']
        else:
            raise

		#self.feature = {}
        for k, v in self.__class__.FEATURE_MAPPING.items():
            if type(v) is str:
                if v not in obj:
                    raise
                val = obj[v]
            if callable(v):
                val  = v(obj=obj)
            setattr(self, k, val)

    def __repr__(self, indent=None):
        '''
        dump = {}
        for key in self.__class__.FEATURE_MAPPING.keys():
            dump[key] = getattr(self, key, '')
        if indent != None:
            return json.dumps(dump, sort_keys=True, indent=4, ensure_ascii=False)
        return json.dumps(dump, ensure_ascii=False)
        '''
        return self.to_json()

    def to_json(self, indent=None):
        dump = {}
        for key in self.__class__.FEATURE_MAPPING.keys():
            dump[key] = getattr(self, key, '')
        if indent != None:
            return json.dumps(dump, sort_keys=True, indent=4, ensure_ascii=False)
        return json.dumps(dump, ensure_ascii=False)

    def __getitem__(self, key):
        return self.__dict__.get(key)
