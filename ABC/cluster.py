import six
import abc
import json
#from group import Group


@six.add_metaclass(abc.ABCMeta)
class Cluster(object):

    def __init__(self, args):
        #if 'group_class' in kwargs:
        #    self.Group_Class = kwargs['group_class']
        #else:
        #    self.Group_Class = Group
        self.map = {}
        self.args = args

    def set(self, gid, group_str):
        self.map[gid] = group_str

    def get(self, gid):
        if gid not in self.map:
            return None
        return self.map[gid]

    def add_group(self, gid, group):
        group_str = json.dumps(group, ensure_ascii=False).replace('\\"',"\"")
        self.set(gid=gid, group=group)

    def del_group(self, group_id):
        try:
            del self.map[group_id]
        except KeyError:
            raise KeyError

    def __len__(self):
        return len(self.map)

    def __repr__(self):
        return repr(self.map)

    def __iter__(self):
        for _id, _group in self.map.items():
            yield _group

    def __items__(self):
        for _id, _group in self.map.items():
            yield _id, _group

