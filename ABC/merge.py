import six
import abc
import logging
from pprintpp import pprint as pp
from feature import Feature
#from group import Group
import os
import json
from utils.logger import get_logger
logger = get_logger('ABC.Merge')

@six.add_metaclass(abc.ABCMeta)
class Merge(object):

    def merge(args, inputs, cluster):
        """
        :param inputs: inputs
        :param cluster: previous merger result
        :return: merged cluster and statistics
        """
        pass

    def __init__(self, args):
        self.args = args
        #self.feature_set = FeatureSet(args)
        #self.new_groups = {}
        #self.modified_groups = {}
        #self.deleted_groups = {}

    def merge_group(self, cluster, idx_mgr, group1, group2, case):
        logger.debug('merge groups(case %s): %s %s' % (case, group1.id, group2.id))
        for _, entity in group2.map.items():
            self.add_entity_to_group(group1, idx_mgr, entity)
            idx_mgr.del_entity(group2, entity)

        idx_mgr.del_group(group2)
        cluster.del_group(group2.id)
        if group1.id not in self.new_groups:
            self.modified_groups[group1.id] = group1
        self.deleted_groups[group2.id] = group2
        return group1
    
    def add_group(args, cluster, idx_mgr, new_groups, group_obj):
        gid = group_obj[Feature.group['gid']]
        #group_str = json.dumps(group_obj, ensure_ascii=False).replace('\\"',"\"")
        #group_str = json.dumps(group_obj, ensure_ascii=False)
        if args['dry_run'] == 'no':
            cluster.add_group(gid, group_obj)
            idx_mgr.add_group(group_obj=group_obj)
            pass
        new_groups[gid] = group_obj
        logger.debug("\t[ADD GROUP] [Finish]  \tsid:[%s]  " % gid)
        return group_obj

    def add_entity_to_group(args, cluster, idx_mgr, group_obj, entity_obj, modified_groups):
        gid = group_obj[Feature.group['gid']]
        eid = entity_obj[Feature.entity['eid']]

        entity_obj = Feature.convert_e4g(gid, entity_obj)
        group_obj[Feature.group['list']].append(entity_obj)

        if args['dry_run'] == 'no':
            cluster.set(gid, group_obj=group_obj)
            idx_mgr.add_group(group_obj=group_obj)
        modified_groups[gid] = group_obj
        logger.debug("\t[ADD Entity TO Group]  sid:[{gid}]\teid:[{eid}] \t[Finish]".format(gid=gid,
                                                                                          eid=eid))
        return group_obj

    def get_cluster_diff(self):
        _exclude_new_gids = []
        for gid, group in self.new_groups.items():
            if gid not in self.cluster.map:
                # created, but delete or merged. not present in self.cluster
                _exclude_new_gids.append(gid)
        for gid in _exclude_new_gids:
            del self.new_groups[gid]

        _exclude_del_gids = []
        for gid, group in self.deleted_groups.items():
            if gid not in self.new_groups:
                _exclude_del_gids.append(gid)
        for gid in _exclude_del_gids:
            del self.deleted_groups[gid]

        _exclude_modified_gids = []
        for gid, group in self.modified_groups.items():
            if gid in self.new_groups:
                _exclude_modified_gids.append(gid)
            if gid in self.deleted_groups:
                _exclude_modified_gids.append(gid)

        for gid in _exclude_modified_gids:
            del self.modified_groups[gid]

        cluster_diff = {
            'new': self.new_groups,
            'modified': self.modified_groups,
            'deleted': self.deleted_groups
        }
        return cluster_diff

    # Deprecated
    def dump_partial(self, partial_dump_path, cluster):
        print(partial_dump_path)
        with open(partial_dump_path,  'w') as f:
            for key, group in cluster.items():
                # f.write(json.dumps(group, ensure_ascii=False))
                f.write(group.to_json() + "\n")

    def dump_result(self, cluster, inputs,  cluster_diff, args):
        output_path = "%s/%s" % (os.getcwd(), args['output_base'])
        print(output_path)
        if not os.path.isdir(output_path):
            raise "output_base is not exists"

        if args['dump_type'] == 'full':
            full_dump_path = '%s/full.txt' % output_path
            self.dump_full(full_dump_path, cluster, inputs, args)
        else:
            partial_dump_path = '%s/partial.txt' % output_path
            self.dump_partial(partial_dump_path, cluster)
