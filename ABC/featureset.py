# -*- coding: utf-8 -*-
import six
import abc
from utils.logger import get_logger
logger = get_logger('ABC.FeatureSet')

@six.add_metaclass(abc.ABCMeta)
class FeatureSet(object):
    map = {
        'name': None,
        'tel': None,
        'address': None
    }
    entity = {
        'eid': 'tempid',
        'name': 'name'
    }

    group = {
        'gid': 'sid',
        'list': 'inputs'
    }
    def __init__(self, args):
        pass

    @abc.abstractclassmethod
    def e2g_should_merge(cls, args, entity, group):
        should_merge = True
        sim = 0
        reason = ""
        return should_merge, sim, reason

    @abc.abstractclassmethod
    def g2g_should_merge(cls, args, group1, group2):
        should_merge = True
        sim = 0
        reason = ""
        return should_merge, sim, reason
    
    @abc.abstractclassmethod
    def convert_e2g(cls, args, entity):
        return group

    @abc.abstractclassmethod
    def get_e2g_sim(cls, args, entity, group):
        sim = reason = None
        return sim, reason

    @abc.abstractclassmethod
    def get_e2e_sim(cls, args, entity1, entity2):
        sim = reason = None
        return sim, reason

    @abc.abstractclassmethod
    def get_g2g_sim(cls, args, group1, group2):
        sim = reason = None
        return sim, reason

    @abc.abstractclassmethod
    def get_entity_feature(cls, args, entity):
        feature_map = {
            'fname1': None,
            'fname2': None
        }
        return feature_map

    @abc.abstractclassmethod
    def convert_e2g(cls, args, entity):
        group = {}
        return group
