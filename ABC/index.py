import six
import abc
import json
from collections import defaultdict

@six.add_metaclass(abc.ABCMeta)
class Index(object):
    
    def __init__(self, **kwargs):
        if 'name' not in kwargs:
            raise
        self.name = kwargs['name']
        self.idx = defaultdict(set)

    def query(self, key):
        if key in self.idx:
            return self.idx[key]
        return None

    def __repr__(self):
        return json.dumps({'name': self.name, 'idx': self.idx},
                          indent=4,  ensure_ascii=False)

    def get_most_like_group(self):
        pass

    def add_doc(self, key, value):
        if value not in self.idx[key]:
            self.idx[key].add(value)

    def del_doc(self, key, value):
        if value in self.idx[key]:
            self.idx[key].remove(value)


