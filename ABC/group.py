# -*- coding: utf-8 -*-
import json
from abc import ABCMeta
'''
'''


class abstractclassmethod(classmethod):
    __isabstractmethod__ = True

    def __init__(self, callable):
        callable.__isabstractmethod__ = True
        super(abstractclassmethod, self).__init__(callable)


class BaseGroup(object):

    MAPPING = {}
    Entity = None

    __metaclass__ = ABCMeta

    def __init__(self, *args, **kwargs):
        if 'entity' in kwargs:
            # create group from entity by setting id, entity
            entity = kwargs['entity']
            self.id = self.gen_gid(entity)
            self.list = [entity]
            eid = getattr(entity, self.__class__.MAPPING['eid'], None)
            self.map = {eid: entity}
        else:
            if 'MAPPING' in kwargs:
                self.__class__.MAPPING = kwargs['MAPPING']
            if 'from_redis' in kwargs:
                obj = json.loads(kwargs['from_redis'])
            elif 'json_str' in kwargs:
                obj = json.loads(kwargs['json_str'])
            elif 'obj' in kwargs:
                obj = kwargs['obj']
            else:
                raise

            if 'id' in obj:
                self.id = obj['id']
            else:
                self.id = obj[self.__class__.MAPPING['id']]
            self.map = {}
            if self.__class__.MAPPING['list'] in obj:
                for _obj in obj[self.__class__.MAPPING['list']]:
                    if 'from_redis' in kwargs:
                        self.add_entity(from_redis=_obj)
                    else:
                        self.add_entity(obj=_obj)

    @abstractclassmethod
    def gen_gid(cls, group):
        return cls()

    def add_entity(self, **kwargs):
        entity = None
        if 'from_redis' in kwargs:
            entity = self.__class__.Entity(from_redis=kwargs['from_redis'])
        elif 'json_str' in kwargs:
            obj = json.loads(kwargs['json_str'])
        elif 'obj' in kwargs:
            obj = kwargs['obj']
        elif 'entity' in kwargs:
            entity = kwargs['entity']
        else:
            raise
        if entity is None:
            entity = self.__class__.Entity(obj=obj)

        eid =  getattr(entity, self.__class__.MAPPING['eid'], None)
        if eid is not None and eid not in self.map:
            self.map[eid] = entity

    def delete_entity(self, **kwargs):
        if 'eid' in kwargs:
            entity = self.map[kwargs['eid']]
        elif 'entity' in kwargs:
            entity = kwargs['entity']
        else:
            raise

        entity_idx = self.list.index(entity)
        self.list.pop(entity_idx)
        eid = getattr(entity, self.__class__.MAPPING['eid'], None)
        if eid is not None:
            del self.map[eid]

    def __repr__(self):
        return self.to_json()
        #return json.dumps({'id': self.id, 'map':self.map}, indent=4,  ensure_ascii=False)

    def to_json(self, indent=4):
        temp = ""
        for eid, entity in  self.map.items():
            temp  += entity.to_json(indent=indent)
            return "{'id': %s, 'map': %s" % (self.id, temp)


if __name__ == '__main__':
    json_str = '{"sid":"6f79a3403ab744c295732246646b5a0c","recommend_name":"麦当劳(东四十条店)","recommend_mainname":"麦当劳","recommend_head":"麦当劳","recommend_branch":"东四十条店","recommend_catkey":"","recommend_address":"工体北路巨石大厦1层","recommend_lat":39.933859,"recommend_lng":116.437943,"recommend_category_name":[],"recommend_category_id":[],"region":{"country_code":86,"country_name":"中国","province_name":"北京","province_id":1,"city_id":2,"city_name":"北>京","district_name":"东城区","district_id":3},"inputs":[{"sid":"6f79a3403ab744c295732246646b5a0c","name":"麦当劳(东四十条店)","mainname":null,"name_key":["麦>当劳"],"region":{"country_code":86,"country_name":"中国","province_name":"北京","province_id":1,"city_id":2,"city_name":"北京","district_name":"东城区","district_id":3},"address":{"province":"北京","city":"北京","district":"东城区","street":"工体北路巨石大厦1层","extra":""},"postcode":"","lat":39.933859,"lng":116.437943,"category_name":["其他生活服务"],"category_id":[12],"tel":[{"number":"010-64156840","desc":"电话","type":3,"ranking":0,"score":0.0,"status":-1,"tel_branch":""}],"tag":[],"topic":[],"source_id":200,"source_name":"mapbaidu","link":"","ranking":-1,"tempid":"08766272b9a83f9d7fe8af2b150c6b62","version":"20160311095628","extend":{"addrlevel":1, "fax": "","disp_order":"5984"},"hidden":0,"official_url":"","intro":"","score":0.0,"comment_count":-1,"logo":""}],"tally":1}'
    MAPPING = {
        'id': 'sid',
        'list': 'inputs',
        'eid': 'eid'
    }
    import sys
    sys.path.append('./')
    sys.path.append('../')
    from tempid import Tempid as Entity
    Group.Entity = Entity 
    shop = Group(MAPPING=MAPPING, json_str=json_str)
    from pprintpp import pprint as pp 
    pp(shop)
