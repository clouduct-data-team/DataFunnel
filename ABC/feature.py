import six
import abc

@six.add_metaclass(abc.ABCMeta)
class Feature(object):

    @abc.abstractmethod
    def __init__(self):
        pass

    @abc.abstractmethod
    def get_keys(self):
        pass
