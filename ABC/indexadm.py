import six
from abc import ABCMeta, abstractmethod


@six.add_metaclass(ABCMeta)
class IndexAdm(object):

    @abstractmethod
    def __init__(self, init_obj):
        pass
    
    @abstractmethod
    def clear(self):
        pass

    @abstractmethod
    def add_group(self, group):
        pass

    @abstractmethod
    def del_group(self, group):
        pass

    @abstractmethod
    def add_entity(self, group, entity):
        pass

    @abstractmethod
    def del_entity(self, group, entity):
        pass

    @abstractmethod
    def query(self, query_obj):
        pass

