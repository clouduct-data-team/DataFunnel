import json
from pprintpp import pprint as pp 
import operator
from collections import defaultdict
import time
from featureset import FeatureSet as FS

class IndexManager(object):
    FeatureSet = FS()
    Index = ''

    def __init__(self, args, **kwargs):
        self.args = args
        if 'feature_set' in kwargs:
            self.__class__.FeatureSet = kwargs['feature_set']
        if 'index' not in kwargs:
            raise
        Index = kwargs['index']
        IndexManager.Index = Index

        if 'index_map' in kwargs:
            self.feature_index_map = kwargs['index_map']
        else:
            self.feature_index_map =  {}
            for feature_name, feature_class in IndexManager.FeatureSet.map.items():
                self.feature_index_map[feature_name] = Index(name=feature_name,
                                                             idx=feature_class.idx)
        self.gid_feature_map = {}

    def extract_entity_feature_map(entity):
        '''
            return {
                f_name: [ feature1, feature2]
            }
        '''
        feature_map = {}
        for f_name, f_class in IndexManager.FeatureSet.items():
            feature_map[f_name] = []
            features = f_class.get_feature(entity)
            for feature in features:
                if feature not in feature_map[f_name]:
                    feature_map[f_name].append(feature)
        return feature_map


    def extract_group_feature_map(group):
        '''
            return {
                f_name_1: [ feature1, feature2, ... ],
                f_name_2: [ feature1, feature2, ... ],
                ...
            }
        '''
        feature_map = {}
        for f_name, f_class in IndexManager.FeatureSet.items():
            feature_map[f_name] = []

        for entity_id, entity in group.map.items():
            entity_feature_map = IndexManager.extract_entity_feature_map(entity)
            for f_name, features in entity_feature_map.items():
                for feature in features:
                    if feature not in feature_map[f_name]:
                        feature_map[f_name].append(feature)
        return feature_map


    def add_feature_2_gid_feature_map(self, group, f_name, feature):
        if group.id not in self.gid_feature_map:
            self.gid_feature_map[group.id] = {}
        if f_name not in self.gid_feature_map[group.id]:
            self.gid_feature_map[group.id][f_name] = []
        if feature not in self.gid_feature_map[group.id][f_name]:
            self.gid_feature_map[group.id][f_name].append(feature)

    def add_group(self, group, gid_feature_map=None, feature_index_map=None):
        if gid_feature_map is None:
            gid_feature_map = self.gid_feature_map
        if feature_index_map is None:
            feature_index_map = self.feature_index_map
        fname_flist_map = IndexManager.extract_group_feature_map(group)
        # ex: {'name': ['金鱼池', '营业厅', '中国联通', '联通']}
        for f_name, features in fname_flist_map.items():
            for feature in features:
                if type(feature_index_map[f_name]) != IndexManager.Index:
                    if feature not in feature_index_map[f_name]:
                        feature_index_map[f_name][feature].add(group.id)
                else:
                    feature_index_map[f_name].add_doc(key=feature, value=group.id)
        gid_feature_map[group.id] = fname_flist_map

    def del_group(self, group):
        if group.id in self.gid_feature_map:
            del self.gid_feature_map[group.id]

    def add_entity(self, group, entity):
        fname_flist_map = IndexManager.extract_entity_feature_map(entity)
        for f_name, features in fname_flist_map.items():
            for feature in features:
                self.feature_index_map[f_name].add_doc(key=feature, value=group.id)
                self.add_feature_2_gid_feature_map(group, f_name, feature)

    def del_entity(self, group, entity):
        fname_flist_map = IndexManager.extract_entity_feature_map(entity)
        for f_name, features in fname_flist_map.items():
            for feature in features:
                self.feature_index_map[f_name].del_doc(key=feature, value=group.id)

    def get_similar_gids(self, group):
        similar_gids_counts_map = {}
        for f_name, features in self.gid_feature_map[group.id].items():
            similar_gids_per_feature = []
            feature_index = self.feature_index_map[f_name]
            for feature in features:
                similar_gids_per_feature.extend(feature_index.query(feature))
                for gid in similar_gids_per_feature:
                    if gid not in self.gid_feature_map:
                        continue
                    if gid not in similar_gids_counts_map:
                        similar_gids_counts_map[gid] = 0
                    similar_gids_counts_map[gid] += 1

        sorted_gids_counts_tuple_list = sorted(similar_gids_counts_map.items(),
                       key=operator.itemgetter(1), reverse=True)
        # [('gid3': 3), ('gid2': 2), ('gid1':1)]
        similar_gids = []
        topK = 3
        threshold = 2
        for gid_count_tuple in sorted_gids_counts_tuple_list[:topK]:
            if gid_count_tuple[1] >= threshold:
                similar_gids.append(gid_count_tuple[0])
        return similar_gids


    def get_gids_by_feature(self, f_name, feature, dedupe=True):
        if f_name not in self.feature_index_map:
            raise
        terms = IndexManager.FeatureSet.map[f_name].get_keys(feature)
        hit_gids_result = []
        feature_index = self.feature_index_map[f_name]

        for term in terms:
            hit_gids = feature_index.query(term)
            if hit_gids is None:
                continue
            for gid in hit_gids:
                if dedupe and gid in hit_gids_result:
                    continue
                hit_gids_result.append(gid)
        return hit_gids_result

    def get_candidate_gids_by_fname_entity(self, f_name, entity):
        feature_index = self.feature_index_map[f_name]
        gid_counts = defaultdict(int)
        _gids = self.get_gids_by_feature(f_name=f_name,
                 feature=entity[f_name], dedupe=False)
        for _gid in _gids:
            gid_counts[_gid] += 1
        return gid_counts

    def get_candidate_gids_by_fname(self, f_name, group):
        feature_index = self.feature_index_map[f_name]
        gid_counts = defaultdict(int)
        for entity_id, entity in group.map.items():
            _gids = self.get_gids_by_feature(f_name=f_name, 
                        feature=entity[f_name], dedupe=False)
            for _gid in _gids:
                gid_counts[_gid] += 1

        """
        sorted_gid_count_list = sorted(gid_counts.items(), key=operator.itemgetter(1))
        #[(gid, 4), (gid, 2), ...]
        gids =[]
        for idx, _gid_count in enumerate(sorted_gid_count_list):
            if idx > self.feature_set.map[f_name].candidate_threshold:
                break
            #for _gid, _count in _gid_count.items():
            gids.append(_gid_count[0])
        return gids
        """
        return gid_counts.keys()


    def get_candidate_gids_entity(self, entity):
        gids_hit_counts_map = {}
        gid_counts_map = defaultdict(int)
        for f_name, f_class in IndexManager.FeatureSet.items():
            candidate_gids_counts_map = self.get_candidate_gids_by_fname_entity(f_name, entity)
            for gid, counts in candidate_gids_counts_map.items():
                gid_counts_map[gid] += counts
        sorted_gid_count_list = sorted(gid_counts_map.items(),
                       key=operator.itemgetter(1), reverse=True)

        return_gid_count = []
        topK = 10
        threshold = 2
        if len(sorted_gid_count_list) >= topK:
            print('TopK over %s' % topK)
        for gid_count in sorted_gid_count_list[:topK]:
            if gid_count[1] >= threshold:
                return_gid_count.append(gid_count[0])
        return return_gid_count

    def get_candidate_gids(self, group):
        gids_hit_counts_map = {}
        gid_counts_map = defaultdict(int)
        for f_name, f_class in IndexManager.FeatureSet.items():
            candidate_gids = self.get_candidate_gids_by_fname(f_name, group)
            for gid in candidate_gids:
                gid_counts_map[gid] += 1

        sorted_gid_count_list = sorted(gid_counts_map.items(),
                       key=operator.itemgetter(1), reverse=True)
        return_gid_count = []
        for gid_count in sorted_gid_count_list:
            if gid_count[1] >= 2:
                return_gid_count.append(gid_count)
        return return_gid_count

    def __repr__(self):
        return repr(self.feature_index_map)

