# -*- coding: utf-8 -*-
import time
from pprintpp import pprint as pp
from flask import Flask, request, Response, jsonify
import json
from flask_cors import CORS, cross_origin
app = Flask(__name__)
CORS(app, resources= {
	r"/groups/*": {"origins": "*"}, 
	r"/group_*/*": {"origins": "*"}}
)
app.config['JSON_AS_ASCII'] = False




def load_cohesion(cohesion_path):
    cohesion, gid_cohesion_value_map = {}, {}
    sorted_key = 'avg'
    with open(cohesion_path, 'r') as f:
        for line in f:
            """
            {
                'gid': .. ,
                'std_dev': .. ,
                'max': .. ,
                'min': ..,
                'member': {
                    'eid_1': {
                        "cohesion": 0.15,
                        "feature": {
                            "content": [""], 
                            .....
                        }
                    }
                }
            }
            """
            group = json.loads(line)
            cohesion[group['gid']] = group
            gid_cohesion_value_map[group['gid']] = group[sorted_key]

    sorted_cohesion = sorted(gid_cohesion_value_map,  
                             key=lambda k: gid_cohesion_value_map[k],
                             reverse=True)

    #for k in sorted_cohesion:
    #    print(k, cohesion[k])
    
    return cohesion, gid_cohesion_value_map, sorted_cohesion


def load_cluster(cohesion, cluster_path):
    gid_groups_map = {}
    with open(cluster_path, 'r') as f:
        for line in f:
            if '*******' in line:
                continue
            group = json.loads(line.strip())
            #pp(group)
            #time.sleep(0.1)
            if group['sid'] not in cohesion:
                continue
            gid_groups_map[group['sid']] = group
    return gid_groups_map


cohesion, gid_cohesion_value_map, sorted_cohesion = load_cohesion('/home/filesystem/dataflow/merge_output/cohesion.txt')
print('finish load cohesion')
print(len(cohesion), len(gid_cohesion_value_map))
gid_groups_map = load_cluster(cohesion, '/home/filesystem/dataflow/merge_output/L20170706/out/cluster/China/lifestyle/2/2.txt')
print('finish load cluster')
print(len(gid_groups_map))
    
@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    r.headers['Content-Type'] = 'application/json; charset=utf-8'
    return r


@app.route("/groups/")
def groups():
    page_no = int(request.args.get('page', '1'))

    result = []
    counter = 0
    for gid in sorted_cohesion[10*(page_no-1):]:
        if gid not in gid_groups_map:
            continue
        counter += 1
        if counter == 10:
            break
        _group = {
            'gid': gid,
            'cohesion': gid_cohesion_value_map[gid],
            'name': gid_groups_map[gid]['recommend_name'],
            'counts': len(gid_groups_map[gid]['inputs'])
        }
        result.append(_group)
    to_dump = {'data': result}
    return json.dumps(to_dump, ensure_ascii=False)


@app.route("/groups/<string:group_id>")
def group(group_id):
    result = []
    _group = gid_groups_map[group_id]
    for entity in _group['inputs']:
        raw_list = []
        feature_list = []
        for key in ['name', 'address', 'tel']:
            raw_list.append(json.dumps(entity[key], ensure_ascii=False))
            feature_list.append(json.dumps(",".join(cohesion[group_id]['member'][entity['tempid']]['feature'][key]),ensure_ascii=False))

        _ = {
            'eid': entity['tempid'] + "<br/ >" + entity['link'],
            'source':  entity['source_name'],
            'version':  entity['version'],
            'raw': "<br />".join(raw_list),
            'feature': "<br />".join(feature_list),
            'cohesion': cohesion[group_id]['member'][entity['tempid']]["cohesion"]
        }
        result.append(_)

    to_dump = {
        'data': result,
        'display_fields': ['cohesion', 'eid', 'source', 'version', 'raw', 'feature']
    }
    return json.dumps(to_dump, ensure_ascii=False)


@app.route("/group_move_entity", methods=['GET', 'POST'])
def group_move_entity():
    result = {
        'from_gid': request.args.get('from_gid'),
        'to_gid': request.args.get('to_gid'),
        'eids': request.args.get('eids')
 
    }
    #pp(result)
    return json.dumps(result)


@app.route("/group_del_entity", methods=['GET', 'POST'])
def group_del_entity():
    result = {
        'from_gid': request.args.get('from_gid'),
        'eids': request.args.get('eids')
    }
    return json.dumps(result)



if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=11080, threaded=True)
