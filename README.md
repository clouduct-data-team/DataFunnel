[![build status](http://gitlab.dianhua.tw/search-team/DataFunnel/badges/develop/build.svg)](http://gitlab.dianhua.tw/search-team/DataFunnel/commits/develop)


# Datafunnel

Merge cleaned data from various crawling sources by similarity between each other.

## Installation

### system packages

Run `yum install -q -y epel-release`

Run `yum install -y -q python36`

Run `curl https://bootstrap.pypa.io/get-pip.py | python3.6`

Run `yum install -q -y python36-devel`

Run `yum install -q -y gcc-c++`

Run `yum install -q -y lapack lapack-devel blas blas-devel`

### python packages

Run `pip3.6 install -q -r requirements.txt`


## Run 

### arguments
- conf       // file path to default settings 
- project    // yulore
- index_name // name of index
- country    // country name (capitalized)
- input_path 
- offline_path
- cluster_path
- dump_full_path
- dump_change_path
- dump_deleted_path

### execution
- Source your virtualenv

- Run `python --arg1=<value1> , .... , --argN=<valueN>`

#### example 
- `python index_test.py --conf=projects/yulore/settings_default.ini --project=yulore --city_id=2 --input_path=/home/filesystem/dataflow/dataclean_output/China/L20180504-test/China/L20180504-test/output/China/lifestyle/2/2.txt.bak  --offline_path=/dataflow/merge_engine/DataFunnel/tests/merge/offline.txt --cluster_path=/dataflow/merge_output/China/L20180410/out/cluster/China/lifestyle/2/2.txt --dump_full_path=/dataflow/merge_engine/DataFunnel/tests/full/full_bj_L20180410/full.txt --dump_changed_path=/dataflow/merge_engine/DataFunnel/tests/full/full_bj_L20180410/changed.txt --dump_deleted_path=/dataflow/merge_engine/DataFunnel/tests/full/full_bj_L20180410/deleted.txt --index_name=merge_test_l20180410_bj --country=China`


# Working flow

```mermaid
graph TD
A[Set Project Path => main.args_utils.set_project_path] -->|import projects/yulore| B(Import Feature, MergeLoader, EntityMerge, IndexManager)
B -->|Create Feature, MergeLoader, EntityMerge IndexManager instance | C{Apply entity merge}
C -->|merge_loader.update_eid_entity_map| D[entity_merge.update_eids]
C -->|merge_loader.new_eid_entity_map| E[entity_merge.merge]
C -->|merge_loader.offline_eid_list| F[entity_merge.offline_eids]
D -->|update_modified_groups| D1[modified_groups]
E -->|new_groups| E1[new_groups]
E -->|update_modified_groups| D1
F-->|offline_groups| F1[delete_groups]
D1 -->O[Import get_source_ranking_map, get_source_ch_map, get_catkey]
E1 -->O
F1 -->O
O -->O1[Create marisa_trie => marisa_trie.Trie]
O1 -->|source_ranking, source_ch, catkey_prefix_tree| O2[Init MRecommend instance]
O2 -->|args, deleted_groups, modified_groups, new_groups, recommend| O3[Export => export.export]
```


# Merge tuning(deprecated)

## Cohension API 
  - python api.py --project=yulore  --conf=projects/yulore/settings.ini


## IDF Generator 
  - python idf_generator.py --cluster_in=/home/ubuntu/DataFunnel/sample_data/cluster/2.txt --idf_path=/tmp/ --field=recommend_name  --load_limit=1000

### evaluate
  - python evaluate.py --input=../sample_data/testing/gt/gid_1_output.text --output=123.text
ari: rm -f 1/* ; rm -f 0/* ; python ari.py
