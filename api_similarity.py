# -*- coding: utf-8 -*-
import time
from pprintpp import pprint as pp
from flask import Flask, request, Response, jsonify
import json
from flask_cors import CORS, cross_origin
from flask import Flask, jsonify
#from flask_swagger import swagger
from flasgger import Swagger
from flasgger import swag_from

app = Flask(__name__)
swagger = Swagger(app)

CORS(app, resources= {
	r"/spec/*": {"origins": "*"}, 
	r"/apidocs/*": {"origins": "*"}, 
	r"/groups/*": {"origins": "*"}, 
	r"/inter/groups/*": {"origins": "*"}, 
	r"/group_*/*": {"origins": "*"}}
)
app.config['JSON_AS_ASCII'] = False



@app.route("/spec")
def spec():
    swag = swagger(app)
    swag['info']['version'] = "1.0"
    swag['info']['title'] = "My API"
    return jsonify(swag)

def load_similarity(similarity_file):
    cohesion, gid_cohesion_value_map, similarity_dict = {}, {}, {}
    line_num = 0
    with open(similarity_file, 'r') as f:
        for line in f:
            line_num += 1
            """
            {
                'gid': .. ,
                'std_dev': .. ,
                'max': .. ,
                'min': ..,
                'member': {
                    'eid_1': {
                        "cohesion": 0.15,
                        "feature": {
                            "content": [""], 
                            .....
                        }
                    }
                }
            }
            """
            if line_num == 100:
                break
            group = json.loads(line)
            cohesion[group['source']['sid']] = group['max_score']
            similarity_dict[group['source']['sid']] = group
            gid_cohesion_value_map[group['source']['sid']] = group['max_score']

    sorted_cohesion = sorted(gid_cohesion_value_map,  
                             key=lambda k: gid_cohesion_value_map[k],
                             reverse=True)

    return cohesion, gid_cohesion_value_map, sorted_cohesion, similarity_dict

def load_cluster(cohesion, similarity_dict, cluster_path):
    gid_groups_map = {}

    gid_candidates = {}

    #f_fuck = open('/tmp/1234.txt', 'w')
    for sid, _ in cohesion.items():
        for _group in similarity_dict[sid]['similar']:
            gid_candidates[_group['group']['_source']['sid']] = True
        gid_candidates[sid] = True
    with open(cluster_path, 'r') as f:
        for line in f:
            if '*******' in line:
                continue
            group = json.loads(line.strip())
            if group['sid'] not in gid_candidates:
                continue
            #f_fuck.write(line)
            gid_groups_map[group['sid']] = group
    return gid_groups_map


cohesion, gid_cohesion_value_map, sorted_cohesion, similarity_dict  = load_similarity('/tmp/123.txt')
print('finish load cohesion')
print(len(cohesion), len(gid_cohesion_value_map))
gid_groups_map = load_cluster(cohesion, similarity_dict, '/tmp/1234.txt')
print('finish load cluster')
print(len(gid_groups_map))
    
@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    #r.headers['Content-Type'] = 'application/json; charset=utf-8'
    return r

@app.route("/inter/groups/")
@swag_from('swagger/inter_groups.yml')
def groups():
    page_no = int(request.args.get('page', '1'))

    result = []
    counter = 0
    for gid in sorted_cohesion[10*(page_no-1):]:
        if gid not in gid_groups_map:
            continue
        counter += 1
        if counter == 10:
            break
        _group = {
            'gid': gid,
            'cohesion': gid_cohesion_value_map[gid],
            'name': gid_groups_map[gid]['recommend_name'],
            'counts': len(similarity_dict[gid]["similar"])
        }
        result.append(_group)
    to_dump = {'data': result}
    return json.dumps(to_dump, ensure_ascii=False)

@app.route("/groups/<string:group_id>")
@swag_from('swagger/groups_sid.yml')
def group(group_id):
    result = []
    _group = gid_groups_map[group_id]
    for tempid in _group['inputs']:
       result.append({
            'eid': tempid['tempid'],
            'name': tempid['name'],
            'address': tempid['address']['street'],
            'tel': ",".join(_t["number"] for _t in tempid['tel'])
        })
    to_dump = {
        'data': result,
        'display_fields': ['eid', 'name', 'address', 'tel']
    }
    return json.dumps(to_dump, ensure_ascii=False)

@app.route("/inter/groups/<string:group_id>")
@swag_from('swagger/inter_groups_sid.yml')
def group_sim(group_id):
    result = []
    for candidate in similarity_dict[group_id]["similar"]:
        result.append(
            {'gid': candidate['group']['_source']['sid'],
             'name': candidate['group']['_source']['name'],
             'counts': len(gid_groups_map[candidate['group']['_source']['sid']]['inputs']),
             'cohesion': candidate["similarity"]
            })
    to_dump = {
        'data': result,
        'display_fields': ['gid', 'name', 'cohesion', 'counts']
    }
    return json.dumps(to_dump, ensure_ascii=False)


@app.route("/inter/group_move_entity", methods=['GET', 'POST'])
def group_move_entity():
    result = {
        'from_gid': request.args.get('from_gid'),
        'to_gid': request.args.get('to_gid'),
        'eids': request.args.get('eids')
 
    }
    #pp(result)
    return json.dumps(result)


@app.route("/group_del_entity", methods=['GET', 'POST'])
def group_del_entity():
    result = {
        'from_gid': request.args.get('from_gid'),
        'eids': request.args.get('eids')
    }
    return json.dumps(result)



if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=12080, threaded=True)
