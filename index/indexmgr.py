import json
from pprintpp import pprint as pp
import operator
from collections import defaultdict
import importlib
from feature import Feature
import index
#from featureset import FeatureSet
#from group import Group
#from entity import Entity
import index_convert

from utils.logger import get_logger
logger = get_logger('IndexManager')

class IndexManager(object):

    def __init__(self, fs, args):
        self.fs = fs
        self.index_load_enable = True if args['index_load_enable'] == "yes" else False
        self.args = args
        self.temporary_idx = self.init_index(args['tmp_index_type'],
                                                args['tmp_index_host'],
                                                args['tmp_index_name'])
        if self.args['reset_tmp_index'] == 'yes':
            self.temporary_idx.clear()
        '''
        POST  /china_groups_tmp/shop/_delete_by_query
        {
           "query": {
              "match_all": {}
           }
        }
        '''

        if self.index_load_enable:
            # yes: load data into temporary index
            # no: use pre-loaded index
            self.primary_idx = self.temporary_idx
        else:
            self.primary_idx = self.init_index(args['index_type'],
                                                args['index_host'],
                                                args['index_name'])

    def __reduce__(self):
        return (self.__class__, (self.fs, self.args, ))

    def init_index(self, index_type, index_host, index_name):
        IndexAdmType = importlib.import_module('index.' + index_type)
        index_adm = IndexAdmType.IndexAdm(index_host, index_name)
        return index_adm

    def read_cluster_file(self):
        return True

    def load_indices(self, args):
        # multiprocessing load from file
        pass

    def extract_entity_feature_map(entity):
        pass

    def extract_group_feature_map(group):
        pass

    def add_feature_2_gid_feature_map(self, group, f_name, feature):
        pass


    def add_group(self, group_obj=None, group_str=None, gid=None):
        # 1. from loader: idx_mgr.add_group(group_str)
        # 2. from merge: merge.add_group(group_obj)
        #    gid = obj[Feature.group['gid']]
        #    logger.debug('\t[ADD GROUP] [Start] - Load Group[%s] from Dump'% gid)
        #else:
        #    eid = obj[Feature.entity['eid']]
        if group_obj is not None:
            obj, from_type = group_obj, "group_obj"
            gid = obj[Feature.group['gid']]

        elif group_str is not None:
            obj,from_type = group_str, "group_str"

        logger.debug('\t[IDX_MGR] [ADD Group:%s] [Start]' % gid)
        index_obj = index_convert.obj2_index_obj(obj, from_type, self.args['tmp_index_type'], self.args)
        result = self.temporary_idx.add_group(index_obj)
        logger.debug('\t[IDX_MGR] [ADD Group:%s] [DONE]' % gid)
        return result
        #def add_group(self, group, gid_feature_map=None, feature_index_map=None):

    def del_group(self, group):
        pass

    def add_entity(self, group, entity):
        pass

    def del_entity(self, group, entity):
        pass

    def query_by_group(self, group):
        pass

    def query_gid_by_eid(self, eid):
        query_obj = index_convert.get_gid_query_obj(self.args['index_type'], eid)
        query_result = self.primary_idx.query(query_obj)
        if len(query_result) == 0:
            return None
        elif len(query_result) > 1:
            logger.error("\t[IDX_MGR] [QUERY GID ERROR]  [EID ({eid})] ".format(eid=eid))
            return None
        return query_result[0]['_id']

    def query_similar_group_by_entity(self, entity):
        #logger.debug('\t[Query IDX] [Start] %s\t%s ' % (entity[Feature.entity['eid']],
        #                                                entity[Feature.entity['name']]))
        gid_group_map = {}

        primary_query_obj = index_convert.get_query_obj(self.args['index_type'],
                                                     self.args, entity)
        primary_query_result = self.primary_idx.query(primary_query_obj)
        primary_gid_group_map = index_convert.convert_query_result(
            self.args['index_type'], primary_query_result)
        #gid_group_map = {'gid': {'score': score, 'sid': sid, ..other_features}

        gid_group_map.update(primary_gid_group_map)
        temporary_gid_group_map = {}

        if not self.index_load_enable:
            temporary_query_obj = index_convert.get_query_obj(self.args['tmp_index_type'],
                                                           self.args, entity)
            temporary_query_result = self.temporary_idx.query(temporary_query_obj)
            temporary_gid_group_map = index_convert.convert_query_result(
                self.args['tmp_index_type'], temporary_query_result)

        gid_group_map.update(temporary_gid_group_map)
        if len(gid_group_map) == 0:
            logger.warn("\t[Query IDX] [No hit] Query Object:\n\t%s" %
                        json.dumps(primary_query_obj, ensure_ascii=False))
        else:
            for gid, _ in gid_group_map.items():
                set_name = ''
                if gid in primary_gid_group_map and temporary_gid_group_map:
                    set_name = "COM"
                elif gid in primary_gid_group_map:
                    set_name = 'PRI'
                else:
                    set_name = 'TMP'
                logger.debug("\t%s\t%s\t%s\t%s\t%s\t%s" % (_['sid'], set_name,
                                    _['score'], _['name'], _['address'], _['tel']))
        #logger.debug('\t[Query IDX] [Finish] %s\t%s '%(entity[Feature.entity['eid']], entity[Feature.entity['name']]))
        #gid_group_map = {'gid': {'score': score, 'sid': sid, ..other_features}
        return gid_group_map

    def get_candidate_gids_entity(self, entity):
        group_score_map = self.query_by_entity(entity)
        return [_['sid'] for _ in group_score_map]

