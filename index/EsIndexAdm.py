import json
from ABC.indexadm import IndexAdm
from elasticsearch import Elasticsearch
from utils.logger import get_logger
logger = get_logger('EsIndexAdm')

class IndexAdm(IndexAdm):

    def __init__(self, index_host, index_name):
        '''
        hosts=[{'host': host, 'port': 443}, {}, .. ]
        '''
        self.args_index_host = index_host
        self.args_index_name = index_name

        hosts = []
        host_port_pairs = [host.strip() for host in index_host.split(',')]
        for host_port_pair in host_port_pairs:
            if ':' in host_port_pair:
                host, port = host_port_pair.split(":")
                port = int(port)
            else:
                host = host_port_pair
                port = 9200
            hosts.append({'host': host, 'port': port})

        self.index_host = hosts
        self.index_name = index_name
        self.es = Elasticsearch(self.index_host,
                                index=index_name,
                                maxsize=5,
                                timeout=30)
    def clear(self):
        try:
            res = self.es.delete_by_query(
                index = self.index_name,
                body = {"query": {"match_all": {}}}
            )
        except Exception as e:
            logger.error("[EsIndexAdm] [CLEAR ERROR] " + str(e)[:100])

    def add_group(self, index_obj):
        res = self.es.update(**index_obj)
        if 'result' in res and res['result'] in ['created', 'noop']:
            return True
        return False

    def del_group(self, group):
        pass

    def add_entity(self, group, entity):
        pass

    def del_entity(self, group, entity):
        pass

    def query(self, query_obj):
        #logger.debug("\t[EsIndexAdm] [Query: %s] " %(self.index_name))
        #logger.debug("\t" +json.dumps(query_obj, ensure_ascii=False))
        retry = 0
        while(True):
            try:
                docs = self.es.search(index=self.index_name,
                                      doc_type=query_obj['doc_type'],
                                      preference = '_primary',
                                      size=query_obj['size'],
                                      body=query_obj['body'])
                result = [doc for doc in docs['hits']['hits']]
            except Exception as e:
                print(str(e))
                retry += 1
                self.es = Elasticsearch(self.args_index_host,
                                index=self.args_index_name,
                                maxsize=25,
                                timeout=30)
                if retry <= 3:
                    continue
                raise e
            return result

    def info(self):
        indices = self.es.indices.get_alias()
        return indices

    def __reduce__(self):
        return (self.__class__, (self.args_index_host, self.args_index_name, ))
