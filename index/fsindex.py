#from featureset import FeatureSet
from ABC.index import Index 
from collections import defaultdict
import json


class FsIndex(Index):

    def __init__(self, **kwargs):
        super(FsIndex, self).__init__(**kwargs)

    def add_doc(self, **kwargs):
        # TO-DO Using group id 
        if 'key' in kwargs and 'value' in kwargs:
            key, value = kwargs['key'], kwargs['value']
            if value not in self.idx[key]:
                self.idx[key].append(value)
        elif 'dict' in kwargs:
            for key, value in kwargs['dict'].items():
                if value not in self.idx[key]:
                    self.idx[key].append(value)
        else:
            raise

    """
    def __getattr__(self, item):
        print("__getattr__")
        print(item)
        if item in self.__dict__:
            return self.__dict__.item
        if item in self.idx:
            return self.idx[item]
        return None
    """
    """
    def __contains__(self, value):
        print(value)
        if value in self.idx:
            return True
        print("miss")
        return False
    """


if __name__ == '__main__':
    pass
