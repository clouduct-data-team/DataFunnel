#from featureset import FeatureSet
from ABC.index import Index 
from collections import defaultdict
import json
import redis

redis_host = "localhost"
redis_port = 6379


class RedisIndex(Index):

    def __init__(self, **kwargs):
        if 'name' not in kwargs:
            raise
        if 'idx' not in kwargs:
            '''
            {'f_name': 1, 'f_name_2': 2, ..}
            '''
            raise
        
        self.name = kwargs['name']
        redis_db = kwargs['idx']
        # ex: title: 0 , tel: 1, etc: ... 
        POOL = redis.ConnectionPool(host=redis_host,port=redis_port,db=redis_db)
        self.idx = redis.StrictRedis(connection_pool=POOL, charset="utf-8", decode_responses=True)
        #self.idx = redis.Redis(redis_host,redis_port,redis_db)

    def add_doc(self, **kwargs):
        # TO-DO Using group id 
        if 'key' in kwargs and 'value' in kwargs:
            key, value = kwargs['key'], kwargs['value']
            if not self.idx.sismember(key, value):
                self.idx.sadd(key, value)
        elif 'dict' in kwargs:
            for key, value in kwargs['dict'].items():
                if not self.idx.sismember(key, value):
                    self.idx.sadd(key, value)
        else:
            raise

    def query(self, key):
        values = self.idx.smembers(key)
        if values is None:
            return list(value.decode('utf-8') in values)
        return None


    def get_union(self, keys):
        hits = self.idx.sunion(keys=keys)
        return list(hit.decode('utf-8') for hit in hits)
    
    def get_set_intersection(self, keys):
        intersection = self.idx.sinter(keys=keys)
        intersection = list(item.decode('utf-8') for item in intersection)
        return intersection

if __name__ == '__main__':
    pass
