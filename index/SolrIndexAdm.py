from ABC.indexadm import IndexAdm
import pysolr
import random
from utils.logger import get_logger

logger = get_logger('SolrIndexAdm')


class IndexAdm(IndexAdm):

    def __init__(self, index_host, index_name):
        '''
        index_host = ["http://172.18.21.221:8983/solr", ..]
        index_name = ["yuloreFWD3_shard1_replica1", ..]
        '''

        #solr_url = "{solr_uri}/{solr_index}/".format(**init_obj)
        self.args_index_host = index_host
        self.args_index_name = index_name

        index_host_list = index_host.split(',')
        index_name_list = index_name.split(',')
        host_idx_tuples = zip(index_host_list, index_name_list)
        self.solr = {}
        self.solr_url = {}
        for idx, host_idx_tuple in enumerate(host_idx_tuples, start=1):
            solr_url = "%s/%s" % (host_idx_tuple[0].strip(), host_idx_tuple[1].strip())
            self.solr_url[idx] = solr_url
            self.solr[idx] = pysolr.Solr(solr_url, timeout=20)

    def clear(self):
        # TO-DO: Implement this
        pass

    def add_group(self, group):
        logger.error('solr.add_group')
        return True

    def del_group(self, group):
        pass

    def add_entity(self, group, entity):
        pass

    def del_entity(self, group, entity):
        pass

    def query(self, query_obj):
        idx = random.randint(1, len(self.solr))
        retry = 0
        while(True):
            try:
                docs = self.solr[idx].search(**query_obj)
                result = [doc for doc in docs]
            except Exception as e:
                print(str(e))
                retry += 1
                self.solr[idx] = pysolr.Solr(self.solr_url[idx], timeout=20)
                if retry <= 3:
                    continue
                raise e
            return result

    def info(self):
        url = self.solr.url
        return url

    def __reduce__(self):
        return (self.__class__, (self.args_index_host, self.args_index_name, ))
