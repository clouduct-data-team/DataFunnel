from abc import ABC, abstractmethod

#from input_type.group import Group
from group import Group

class Recommend(ABC):
    @abstractmethod
    def recommend_group(self, group: Group) -> None:
        pass
