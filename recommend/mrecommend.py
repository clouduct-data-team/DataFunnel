import re
import math
from typing import Iterable, Dict, List
from collections import defaultdict
from pprintpp import pprint as pp
from .recommend import Recommend
#from input_type.mgroup import MGroup
#from input_type.mentity import MEntity
from group import Group as MGroup
from entity import Entity as MEntity
import json

REGEX_BRACKET = re.compile('\(|\（')
REGEX_CJK = re.compile('[\u1100-\u11ff\u2e80-\ua4cf\ua840-\ua87f\uac00-\ud7af\uf900-\ufaff\ufe30-\ufe4f\uff65-\uffdc\U00020000-\U0002ffff]', re.UNICODE)
REGEX_SPECIAL_CHAR = re.compile(r'[※*…﹡%％@＠〖〗【】☆★○●◇◆□■△▲￥丶、<>!！\[\]+=＝〈〉；;。\\.、/：:,，?\-—－―()（）?？>；，:;,：？*。．。，、；：？！ˉˇ¨`~々～‖∶＂＇｀｜·…—〃‘’“”〝〞〔〕〈〉《》「」『』〖〗【】（）［］·:;\'"|]')
SPLIT_SPACE = re.compile(r'\s')
ENG_CHAR = re.compile(r'[A-Za-z]')
CHAR_TYPE_CJK = 1
CHAR_TYPE_NON_CJK = 2



def getBranchName(country, name):
    """
    ex: input: "我家牛排 (忠孝店)"
        output: 忠孝店, (5, 10)
                # 5 is the index of (, 10 is the index of )
    samples = [
            "我家牛排 (忠孝店)",
            "我家牛排（忠孝店）",
            "我家牛排【 忠孝店】",
            "我家牛排［忠孝店］",
            "我家牛排 - 忠孝店",
            "我家牛排 忠孝店",
            "我家牛排忠孝店",
            " XXX Hotel - Taipei"
            " XXX Hotel (Taipei)"
        ]
    ! India
    name: "Canara Bank"

    for sample in samples:
        print(sample, getBranchName(sample))

    """
    start_idx = end_idx = 0
    branch_pair_list = ["()", "【】", "[]", "（）", "〖〗", "『』", "〔〕","〈〉", "《》", "「」",
                        "［］"]
    for branch_pair in branch_pair_list:
        if branch_pair[0] in name and branch_pair[1] in name:
            start_idx = name.index(branch_pair[0])
            end_idx = name.index(branch_pair[1])
            break

    if start_idx == end_idx == 0:
        sperator_list = "-—－―"
        if country == "CHINA":
            sperator_list = sperator_list + " "
        for sperator in sperator_list:
            if name.count(sperator) == 1:
                start_idx = name.index(sperator)
                end_idx = len(name) + 1
                break

    if start_idx == end_idx == 0:
        return '', (0,0)
    return name[start_idx+1: end_idx].strip(), (start_idx, end_idx)

def escape_rmd_name(rmd_name):
    # remove branch within () （）
    match = next(REGEX_BRACKET.finditer(rmd_name), None)
    return rmd_name[:match.start()] if match else rmd_name


def is_eng_char(s: str) -> bool:
    return ENG_CHAR.match(s)


def char_type(c: str) -> str:
    return (CHAR_TYPE_CJK
        if REGEX_SPECIAL_CHAR.match(c) or REGEX_CJK.match(c)
        else CHAR_TYPE_NON_CJK)


def tokenize(s: str) -> List[str]:
    tokens = []
    for tt in SPLIT_SPACE.split(s):
        p, p_type  = '', None
        for c in tt:
            c_type = char_type(c)
            if c_type == CHAR_TYPE_CJK or c_type != p_type:
                if p:
                    tokens.append(p)
                p = c
            else:
                p += c
            p_type = c_type
        if p:
            tokens.append(p)
    return tokens


def mlcs(strlist: List[str], threshold: float):
    '''
    1. Find all major_tokens appear in strlist > threshold.
    2. Count number of all major_tokens in each str as score.
    3. return max score of strlist
    '''
    norm_strlist = list(map(str.lower, strlist))
    token_strlist = list(map(tokenize, norm_strlist))

    tokens = set(
            token.lower()
            for tokens in token_strlist
            for token in tokens
        )
    token_in_str = {
            t: [t in s for s in token_strlist]
            for t in tokens
        }
    threshold = math.ceil(len(token_strlist)*threshold)
    major_tokens = [
            t for t, in_str in token_in_str.items()
            if sum(in_str) >= threshold
        ]
    if not major_tokens:
        return max(strlist, key=len)
    scores = map(sum, zip(*[token_in_str[t] for t in major_tokens]))
    scores = list(scores)

    mx = 0
    for i, score in enumerate(scores):
        if (score > scores[mx] or (
                score == scores[mx] and
                len(strlist[i]) < len(strlist[mx]))):
            mx = i
    return strlist[mx]


class MRecommend(object):
    def __init__(self, country: str,
                 source_ranking_map: Dict[str, int], 
                 source_ch: Dict[str, str],
                 catkey_prefix_tree):
        global g_source_ranking_map
        g_source_ranking_map = source_ranking_map
        self.country = country.upper()
        self.source_ch = source_ch
        self.catkey_prefix_tree = catkey_prefix_tree

    def merge_entity_group(self, group: MGroup, entities: Iterable[MEntity]) -> MGroup:
        '''
        Returns:
            merged group for merging group and entities
        '''
        group['inputs'].extend(entities)
        '''
        logger.debug(
                "\t[ADD Entity TO Group]  sid:[{gid}]"
                "\teid:[{eid}] "
                "\t[Finish]".format(
                    gid=group['gid'], eid=list(
                        map(lamdba e: e['tempid'], entities)))
            )
        '''
        return group

    def recommend_group(self, group: MGroup) -> None:
        assert hasattr(group, 'obj')
        obj = group.obj
        obj.update(self.get_recommend(obj))

    def get_recommend(self, group):
        self.cache_name = self.name(group)
        self.branch, self.branch_idx = getBranchName(self.country, self.cache_name)
        for entity in group['inputs']:
            for tel in entity['tel']:
                tel.update({'tel_branch': self.branch})
        # args['source_ranking_map'] = orm.get_source_ranking_map()
        recommend_fields = (
            ('recommend_name', self.cache_name),
            ('recommend_mainname', self.mainname),
            ('recommend_head', self.head),
            ('recommend_branch', self.branch),
            ('recommend_catkey', self.cat_key),
            ('recommend_address', self.address),
            ('recommend_lat', self.lat),
            ('recommend_lng', self.lng),
            ('recommend_category_name', self.get_recommend_category_name),
            ('recommend_category_id', self.get_recommend_category_id),
            ('region', self.get_region),
        )
        recommend_result = {
                field_name: field_value(group)
                if callable(field_value) else field_value
                for field_name, field_value in recommend_fields
            }
        for field_name, field_value in recommend_result.items():
            if type(field_value) is list:
                recommend_result[field_name] = sorted(field_value)
        return recommend_result

    def cat_key(self, group):
        if self.country == 'INDIA':
            return ''
        prefix = ''
        for token in self.head(group):
            prefix += token
            prefix_candidates = self.catkey_prefix_tree.keys(prefix)
            if len(prefix_candidates) == 0:
                prefix = ''
                continue
            for candidate in prefix_candidates:
                if candidate == prefix:
                    return candidate
        return ''
                

    def get_region(self, obj):
        '''
        Voting on value of region_name, excluding empty str or -1
        '''
        region = dict(
            country_code=None,
            country_name=None,
            province_name=None,
            province_id=None,
            city_id=None,
            city_name=None,
            district_name=None,
            district_id=None,
        )
        for rname in region:
            statis = defaultdict(int)
            for e in obj['inputs']:
                statis[e['region'][rname]] += 1
            # exclude invalid value
            statis.pop('', 0)
            statis.pop(-1, 0)
            if statis:
                region[rname] = max(statis.items(), key=lambda p: p[1])[0]
            else:
                # all value invalid, use arbitrary
                region[rname] = e['region'][rname]
        return region

    def name(self, group):
        """
        default return mlcs, if empty return longest name instead
        """
        return mlcs([e['name'] for e in group['inputs']], 0.6) or \
            max(group['inputs'], key=lambda e: len(e['name']))['name']

    def mainname(self, group):
        if len(group['inputs']) == 1:
            source = group['inputs'][0]['source_name']
            if (source in g_source_ranking_map and
                    200 <= g_source_ranking_map[source] <= 400):
                if source != 'yulore_special':
                    return self.source_ch[source]
                else:
                    #return group['inputs'][0]['mainname']
                    if 'extend' in group['inputs'][0]:
                        extend = json.loads(group['inputs'][0]['extend'])
                        if 'mainname' in extend:
                            return extend['mainname']
                #return "MAINNAME"
                #return group['inputs'][0]['mainname']

        return self.head(group)


    def head(self, group):
        if self.branch != "":
            return escape_rmd_name(self.cache_name[:self.branch_idx[0]])
        return ""

    def address(self, group):
        return max(
                [entity['address']['street'] for entity in group['inputs']],
                key=len,
            )

    def lat(self, group):
        return self.latlng(group)[0]

    def lng(self, group):
        return self.latlng(group)[1]

    def latlng(self, group):
        return max(
                [(entity['lat'], entity['lng']) for entity in group['inputs']],
                key=lambda p: p[0]+p[1],
            )

    def get_recommend_category_name(self, group):
        cat_names = set()
        for entity in group['inputs']:
            cat_names.update(entity['category_name'])
        return list(cat_names)

    def get_recommend_category_id(self, group):
        cat_ids = set()
        for entity in group['inputs']:
            cat_ids.update(entity['category_id'])
        return list(cat_ids)
